<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['user_id', 'name', 'status'];

    public function phones()
    {
        return $this->belongsToMany('App\Phone', 'phone_group',
            'group_id', 'phone_id');
    }
}
