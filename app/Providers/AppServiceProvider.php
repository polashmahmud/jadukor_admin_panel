<?php

namespace App\Providers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        view()->composer(['layouts.sidebar', 'dashboard', 'layouts.header', 'admin.index', 'admin.show', 'masterreseller.index', 'masterreseller.show', 'reseller.index', 'reseller.show', 'user.index', 'user.show'], function ($view) {
            $user_roles = Auth::user()->roles;
            $user_roles_array = $user_roles->pluck('id')->toArray();

            $user_permissions_all = DB::table('permission_role')->whereIn('role_id', $user_roles_array)->get();
            $user_permissions_array_all_value = $user_permissions_all->pluck('permission_id')->toArray();
            $user_permissions = array_unique($user_permissions_array_all_value);

            $view->with('user_permissions', $user_permissions);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        if ($this->app->environment() !== 'production') {
            $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
        }
    }
}
