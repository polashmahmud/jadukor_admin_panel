<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Template extends Model
{
    protected $fillable=['name', 'content'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
