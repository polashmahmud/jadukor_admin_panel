<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SmsCost extends Model
{
    protected $fillable = ['preview_sms_id', 'operator_id', 'recipient', 'sms', 'rate', 'cost'];

    public function previewsms()
    {
        return $this->belongsTo('App\PreviewSms');
    }

    public function operator()
    {
        return $this->belongsTo('App\Operator');
    }
}
