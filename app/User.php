<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\MediaLibrary\Models\Media;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Spatie\MediaLibrary\HasMedia\HasMedia;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasMediaTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'username', 'email', 'member_id', 'phone', 'address', 'price_type', 'average_masking_price', 'average_non_masking_price', 'status', 'role', 'api', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function roles()
    {
        return $this->belongsToMany('App\Role');
    }

    public function maskings()
    {
        return $this->belongsToMany('App\Masking');
    }

    public function sms()
    {
        return $this->hasMany('App\Sms');
    }

    public function template()
    {
        return $this->hasMany('App\Template');

    }

    public function registerMediaCollections()
    {
        $this
            ->addMediaCollection('avatar')
            ->registerMediaConversions(function (Media $media = null) {

                $this->addMediaConversion('small')
                    ->width(50)
                    ->height(50);

                $this->addMediaConversion('thumb')
                    ->width(150)
                    ->height(150);

                $this->addMediaConversion('medium')
                    ->width(300)
                    ->height(300);
            });
    }
}
