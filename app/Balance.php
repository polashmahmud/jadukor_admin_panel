<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    protected $fillable = ['user_id', 'state', 'payment_method', 'description', 'amount', 'masking_sms', 'non_masking_sms'];
}
