<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class Sms extends Model
{
    protected $fillable = ['user_id', 'sender', 'contents', 'send_type', 'group_id', 'sms_type', 'masking', 'ttl_number', 'ttl_sms', 'ttl_cost', 'schedule', 'schedule_time', 'status'];

    public function scopeChartdata($query, $id)
    {
        return $query->where([
            ['masking', $id],
            ['user_id', Auth::id()],
            ["created_at",">", Carbon::now()->subMonths(12)]
        ])
            ->selectRaw('
                year(created_at) year,
                monthname(created_at) month,
                sum(ttl_number) total
            ')
            ->groupBy('year','month')
            ->get();
    }

    public function smscost()
    {
        return $this->hasMany('App\SmsCost', 'preview_sms_id', 'id');
    }

    public function numbers()
    {
        return $this->hasMany('App\Number', 'preview_sms_id', 'id');
    }

    public function sendroute()
    {
        return $this->hasMany('App\Sendroute', 'sms_id', 'id');
    }
}
