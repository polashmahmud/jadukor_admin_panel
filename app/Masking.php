<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Masking extends Model
{
    protected $fillable = ['user_id', 'display_name', 'name', 'status', 'approved_at'];

    public function users()
    {
        return $this->belongsToMany('App\User', 'masking_user', 'user_id', 'masking_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
