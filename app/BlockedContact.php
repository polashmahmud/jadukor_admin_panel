<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockedContact extends Model
{
    protected $fillable = ['name', 'number'];
}
