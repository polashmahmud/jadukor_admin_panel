<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Number extends Model
{
    protected $fillable=['preview_sms_id', 'content', 'number', 'sender', 'sms', 'rate', 'cost'];

    public function sms()
    {
        return $this->belongsTo('App\Sms');
    }
}
