<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ['user_id', 'number', 'name', 'email'];

    public function groups()
    {
        return $this->belongsToMany('App\Group', 'phone_group',
            'phone_id', 'group_id');
    }
}
