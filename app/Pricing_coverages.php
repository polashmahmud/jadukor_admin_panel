<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pricing_coverages extends Model
{
    protected $fillable = ['user_id', 'country_id', 'operator_id', 'sms_gateway_non_masking_id', 'sms_gateway_masking_id', 'masking_price', 'non_masking_price', 'sell_msk_price', 'sell_non_msk_price', 'status'];
    protected $hidden = ['created_at', 'updated_at'];

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function operator()
    {
        return $this->belongsTo('App\Operator');
    }

    public function masking_gateway()
    {
        return $this->belongsTo('App\Smsgateway', 'sms_gateway_masking_id', 'id');
    }

    public function non_masking_gateway()
    {
        return $this->belongsTo('App\Smsgateway', 'sms_gateway_non_masking_id', 'id');
    }
}
