<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sendroute extends Model
{
    protected $fillable=['sms_id', 'route_id', 'ttl_number', 'ttl_sms'];

    public function sms()
    {
        return $this->belongsTo('App\Sms');
    }
}
