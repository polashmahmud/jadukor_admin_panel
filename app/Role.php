<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['name', 'display_name', 'category'];

    public function permissions()
    {
        return $this->belongsToMany('App\Permission');
    }
}
