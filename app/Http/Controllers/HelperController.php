<?php

namespace App\Http\Controllers;

use App\Http\Requests\StatusChangeRequest;
use App\Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class HelperController extends Controller
{
    public function statusChange(StatusChangeRequest $request)
    {
        $value = $request->value == 0 ? 1 : 0;

        DB::table($request->table)
            ->where('id', $request->id)
            ->update([$request->column => $value]);

        $message = $request->message;
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    public function switchUser(Request $request)
    {
        $user = User::find($request->user_id);
        if (Auth::user()->role == 1 && Auth::id() == $user->member_id || Auth::user()->role == 2 && Auth::id() == $user->member_id|| Auth::user()->role == 3 && Auth::id() == $user->member_id) {

            Auth::login($user);
            return redirect('/');
        } else {
            Session::flash('warning', 'Permission Denied');
            return back();
        }
    }
}
