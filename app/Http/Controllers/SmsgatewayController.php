<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Http\Requests\SmsGetewayRequest;
use App\Log;
use App\Smsgateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class SmsgatewayController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $smsgeteways = Smsgateway::all();
        $col_data=array();
        $smsgateway = new Smsgateway;
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Display Name'),
            trans('messages.User Display Name'),
            trans('messages.URL'),
            trans('messages.Type'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($smsgeteways as $value) {
            $form_url = route('sms-gateway.destroy', $value->id);
            $edit_url = route('sms-gateway.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->display_name,
                $value->user_display_name,
                $value->url,
                $value->type ? "Masking" : "Non Masking",
                $value->status ? "Active" : "Deactivate",
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.smsgateway.index', compact('col_heads', 'col_data', 'smsgateway'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SmsGetewayRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        $smsGateway = new Smsgateway;
        $smsGateway->fill($data);
        $smsGateway->save();

        $message = "Add New SMS Gateway";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Smsgateway  $smsgateway
     * @return \Illuminate\Http\Response
     */
    public function show(Smsgateway $smsgateway)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Smsgateway  $smsgateway
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $smsgeteways = Smsgateway::all();
        $col_data=array();
        $smsgateway = Smsgateway::find($id);
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Display Name'),
            trans('messages.User Display Name'),
            trans('messages.URL'),
            trans('messages.Type'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($smsgeteways as $value) {
            $form_url = route('sms-gateway.destroy', $value->id);
            $edit_url = route('sms-gateway.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->display_name,
                $value->user_display_name,
                $value->url,
                $value->type ? "Masking" : "Non Masking",
                $value->status ? "Active" : "Deactivate",
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.smsgateway.index', compact('col_heads', 'col_data', 'smsgateway'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Smsgateway  $smsgateway
     * @return \Illuminate\Http\Response
     */
    public function update(SmsGetewayRequest $request, $id)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        $smsGateway = Smsgateway::find($id);
        $smsGateway->fill($data);
        $smsGateway->save();

        $message = "Edit SMS Gateway";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Smsgateway  $smsgateway
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $smsGateway = Smsgateway::find($id);
        $smsGateway->delete();

        $message = "Delete SMS Gateway";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
