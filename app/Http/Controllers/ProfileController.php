<?php

namespace App\Http\Controllers;

use App\Log;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ProfileController extends Controller
{
    public function index($name)
    {
        $user = User::where('username', $name)->first();

        // Last Transaction
        $transaction = Transaction::where([['user_id', $user->id],['state', 1]])->latest()->first();


        if (Auth::id() !== $user->id) {
            Session::flash('error', 'You Can\'t access this profile');
            return back();
        }


        return view('profile.index', compact('user', 'transaction'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->address = $request->address;

        if ($request->hasFile('avatar')) {
            if ( ! empty($user->getFirstMediaUrl('avatar', 'small'))) {
                $user->clearMediaCollection('avatar');
            }
            $user->addMedia($request->avatar)->toMediaCollection('avatar');
        }

        $user->save();

        $message = "Profile Change";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    public function passwordChange(Request $request, $id)
    {
        $request->validate([
            'password' => 'required'
        ]);

        $user = User::find($id);

        if (isset($request->password)) {
            $user->password = Hash::make($request->password);
        }

        $user->save();

        $message = "Change Password";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
