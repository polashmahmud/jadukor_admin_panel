<?php

namespace App\Http\Controllers;

use App\BlockedContact;
use App\Classes\Helper;
use App\Default_pricing;
use App\Log;
use App\Number;
use App\Operator;
use App\PreviewSms;
use App\Pricing_coverages;
use App\SmsCost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use phpDocumentor\Reflection\Types\String_;
use App\Sms;
use App\Group;

class PreviewSmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPreviewSms = Sms::where([['user_id', Auth::id()], ['status', 0]])->get();

        $col_data=array();
        $col_heads = array(
            trans('messages.Sender'),
            trans('messages.Recipients'),
            trans('messages.Contents'),
            trans('messages.Type'),
            trans('messages.Schedule'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allPreviewSms as $value) {
            $form_url = route('preview-sms.destroy', $value->id);
            $edit_url = route('preview-sms.edit', $value->id);
            $show_url = route('preview-sms.show', $value->id);
            $col_data[] = array(
                $value->sender,
                $value->recipients,
                $value->contents,
                $value->sms_type ? "Unicode" : "Text",
                $value->schedule_time,
                $value->status ? "Send" : "Save",
                "<a href=\"$show_url\"><i class=\"ti-user color-dark\"></i></a> " .
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('preview_sms.index', compact('col_heads', 'col_data'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PreviewSms  $previewSms
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $previewSms = Sms::with('numbers')->find($id);
        $numbers = $previewSms->numbers->pluck('number')->implode(',');
        $phoneGroups = Group::where([['user_id', Auth::id()], ['status', 1]])->get();
        return view('preview_sms.show', compact('previewSms', 'phoneGroups', 'numbers'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PreviewSms  $previewSms
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $previewSms = Sms::with('numbers')->find($id);
        $numbers = $previewSms->numbers->pluck('number')->implode(',');
        $phoneGroups = Group::where([['user_id', Auth::id()], ['status', 1]])->get();
        return view('preview_sms.edit', compact('previewSms', 'phoneGroups', 'numbers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PreviewSms  $previewSms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->group_id)) {
            $group_id = Group::find($request->group_id);
            $Numbers = $group_id->phones->pluck('number');
        } else {
            if (strpos($request->recipients, ',') !== false) {
                $Numbers = explode(',', $request->input('recipients'));
            } else {
                $Numbers = explode("\n", $request->input('recipients'));
            }
        } 

        $allNumbers=[];
        foreach ($Numbers as $number) {
            $fullNumber = '880' . substr(preg_replace('/\D/', '', $number), -10);
            if (strlen(trim($fullNumber)) == 13) {
                $allNumbers[] = trim($fullNumber);
            }
        }

        // Blocked Contact
        $blockContact = BlockedContact::get();
        $blockContactAll = json_decode(json_encode($blockContact->pluck('number')));

        // remove blocker contact
        $phoneNumbers = array_diff($allNumbers,$blockContactAll);

        $data = [];
        foreach ($phoneNumbers as $value) {
            $data[] = substr($value,0,5);
        }

        if (strlen($request->contents) != strlen(utf8_decode($request->contents)))
        {
            $smsType = 'unicode';
        } else {
            $smsType = 'text';
        }

        // Special characters
        if (preg_match('/[\'\^{}\\\\\\[~\\]|€]/', $request->contents))
        {
            $specila_text = "yes";
        } else {
            $specila_text = "no";
        }

        // SMS Text Count
        if (strlen($request->contents) != strlen(utf8_decode($request->contents)))
        {
            $smsTextCount = mb_strlen($request->contents, 'UTF-8');
        } else {
            $smsTextCount = strlen($request->contents);
        }

        $count = Helper::SmsCount($smsType, $smsTextCount, $specila_text);

        if ($count == 0) {
            Session::flash('error', 'Message text is too long');
            return back();
        }


        // User Pricing
        $user_pricing = Pricing_coverages::where('user_id', Auth::id())->get();
        if (empty($user_pricing)) {
            $default_pricing = Pricing_coverages::where('user_id', Auth::id())->get(['id', 'operator_id']);
            $default_pricing_prefix_id = $default_pricing->pluck('operator_id');
        } else {
            $default_pricing = Default_pricing::get(['id', 'operator_id']);
            $default_pricing_prefix_id = $default_pricing->pluck('operator_id');
        }

        // Prefix
        $prefix = Operator::whereIn('id',$default_pricing_prefix_id)->get();
        $prefixList = json_decode(json_encode($prefix->pluck('prefix')));

        // Count how many sms each prefix value
        $overlap = array_intersect($data, $prefixList);
        $counts  = array_count_values($overlap);

        $previewSms = Sms::find($id);
        $previewSms->contents = $request->contents;
        $previewSms->sms_type = $smsType == 'unicode' ? 1 : 0;
        if(isset($request->group_id)) {
            $previewSms->group_id = $request->group_id;
        }
        $previewSms->save();

        $numbers = [];
        foreach ($phoneNumbers as $number) {
            $numbers[] = array(
                'preview_sms_id' => $previewSms->id,
                'content' => $request->message,
                'number' => $number,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }
        Number::where('preview_sms_id', $previewSms->id)->delete();
        Number::insert($numbers);

        SmsCost::where('preview_sms_id', $id)->delete();

        foreach ($counts as $key=>$value) {
            $smsCost = new SmsCost;
            $smsCost->preview_sms_id = $previewSms->id;
            $smsCost->operator_id = Helper::operatorId($key);
            $smsCost->recipient = $value;
            $smsCost->sms = $count;
            $smsCost->rate = Auth::user()->price_type == 0 ? Helper::smsPrice($key, $request->sender_id) : 1;
            $smsCost->cost = Auth::user()->price_type == 0 ? ($value * $count) * Helper::smsPrice($key, $request->sender_id) : $value * $count;
            $smsCost->save();
        }


        // $message = "Edit Successfully";
        // Session::flash('success', $message);
        // Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return redirect()->route('preview-sms.show', $id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PreviewSms  $previewSms
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $previewSms = Sms::find($id);
        $previewSms->delete();

        $message = "Delete Draft SMS";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
