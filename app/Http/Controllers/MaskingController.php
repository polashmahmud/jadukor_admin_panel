<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Http\Requests\MaskingRequest;
use App\Log;
use App\Masking;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class MaskingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $maskings = Masking::where('user_id', Auth::id())->latest()->get();
        $masking = new Masking;
        $col_data=array();
        $col_heads = array(
            trans('messages.Added At'),
            trans('messages.Masking / Sender ID'),
            trans('messages.Approved At'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($maskings as $value) {
            $form_url = route('masking.destroy', $value->id);
            $edit_url = route('masking.edit', $value->id);
            $col_data[] = array(
                $value->created_at,
                $value->display_name,
                $value->approved_at,
                $value->status ? 'Active' : 'Pending',
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('masking.index', compact('col_heads', 'col_data', 'masking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MaskingRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();
        if (isset($request->name)) {
            $data['name'] = $request->name;
        } else {
            $data['name'] = $request->display_name;
        }
        if (isset($request->status)) {
            if ($request->status==1) {
                $data['status'] = 1;
                $data['approved_at'] = Carbon::now();
            }
        }

        Masking::create($data);

        $message = "Add New Masking";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Masking  $masking
     * @return \Illuminate\Http\Response
     */
    public function show(Masking $masking)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Masking  $masking
     * @return \Illuminate\Http\Response
     */
    public function edit(Masking $masking)
    {
        $maskings = Masking::where('user_id', Auth::id())->get();
        $col_data=array();
        $col_heads = array(
            trans('messages.Added At'),
            trans('messages.Masking / Sender ID'),
            trans('messages.Approved At'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($maskings as $value) {
            $form_url = route('masking.destroy', $value->id);
            $edit_url = route('masking.edit', $value->id);
            $col_data[] = array(
                $value->created_at,
                $value->display_name,
                $value->approved_at,
                $value->status ? 'Active' : 'Pending',
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('masking.index', compact('col_heads', 'col_data', 'masking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Masking  $masking
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Masking $masking)
    {
        $data = $request->all();
        if ($request->status==1) {
            $data['approved_at'] = Carbon::now();
        } else {
            $data['approved_at'] = null;
        }

        $masking->update($data);

        $message = "Edit Masking";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Masking  $masking
     * @return \Illuminate\Http\Response
     */
    public function destroy(Masking $masking)
    {
        $masking->delete();

        $message = "Delete Masking";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
