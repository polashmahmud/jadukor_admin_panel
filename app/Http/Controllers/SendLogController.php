<?php

namespace App\Http\Controllers;

use App\SendLog;
use Illuminate\Http\Request;

class SendLogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SendLog  $sendLog
     * @return \Illuminate\Http\Response
     */
    public function show(SendLog $sendLog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SendLog  $sendLog
     * @return \Illuminate\Http\Response
     */
    public function edit(SendLog $sendLog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SendLog  $sendLog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SendLog $sendLog)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SendLog  $sendLog
     * @return \Illuminate\Http\Response
     */
    public function destroy(SendLog $sendLog)
    {
        //
    }
}
