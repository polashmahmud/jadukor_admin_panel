<?php

namespace App\Http\Controllers;

use App\Log;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class ApiController extends Controller
{
    public function index()
    {
        return view('api_settings.index');
    }

    public function generate(Request $request)
    {
        // Only For User Transfer Amount
        if ($request->user_id != Auth::id()) {
            Session::flash('warning', 'Permission Denied');
            return back();
        }

        $hashed_random_password = Hash::make(str_random(8));
        $user = User::find($request->user_id);

        $user->api = $hashed_random_password;
        $user->save();

        $message = "Api Key Generate";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
