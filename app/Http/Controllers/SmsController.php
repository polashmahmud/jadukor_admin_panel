<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Classes\Helper;
use App\Log;
use App\Number;
use App\Operator;
use App\PreviewSms;
use App\Pricing_coverages;
use App\SendLog;
use App\Sms;
use App\Smsgateway;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;
use App\Sendroute;
use PHPUnit\Framework\Constraint\Count;

class SmsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $numbers = Number::where('preview_sms_id', $request->preview_sms_id)->pluck('number');

        // User and reseller Balance Check
        if (Auth::user()->price_type==0) {
            $userBalance = Helper::Balance(Auth::id());
            $resellerBalance = Helper::Balance(Auth::user()->member_id);
            $cost = $request->cost;
            if ($cost > $userBalance) {
                $message = "There is not enough balance";
                Session::flash('error', $message);
                return back();
            }
            if ($cost > $resellerBalance) {
                $message = "Contact your Administrator";
                Session::flash('error', $message);
                return back();
            }
        } else {
            if ($request->sender=='non_masking') {
                $userBalance = Helper::BalanceNonMaskingSMS(Auth::id());
                $resellerBalance = Helper::BalanceNonMaskingSMS(Auth::user()->member_id);
                $cost = $request->cost;
                if ($cost > $userBalance) {
                    $message = "There is not enough balance";
                    Session::flash('error', $message);
                    return back();
                }
                if ($cost > $resellerBalance) {
                    $message = "Contact your Administrator";
                    Session::flash('error', $message);
                    return back();
                }
            } else {
                $userBalance = Helper::BalanceMaskingSMS(Auth::id());
                $resellerBalance = Helper::BalanceMaskingSMS(Auth::user()->member_id);
                $cost = $request->cost;
                if ($cost > $userBalance) {
                    $message = "There is not enough balance";
                    Session::flash('error', $message);
                    return back();
                }
                if ($cost > $resellerBalance) {
                    $message = "Contact your Administrator";
                    Session::flash('error', $message);
                    return back();
                }
            }
        }

        $allTeleTalkNumber=[];
        $allAirtelNumber=[];
        $allGPNumber=[];
        $allRobiNumber=[];
        $allBanglalinkNumber=[];

        foreach ($numbers as $number) {
            $fullNumber = '880' . substr(preg_replace('/\D/', '', $number), -10);
            if (strlen(trim($fullNumber)) == 13) {
                if (substr($fullNumber, 0, 5)=="88013") {
                    $allGPNumber[] = $fullNumber;
                } elseif (substr($fullNumber, 0, 5) == "88015") {
                    $allTeleTalkNumber[] = $fullNumber;
                } elseif (substr($fullNumber, 0, 5) == "88016") {
                    $allAirtelNumber[] = $fullNumber;
                } elseif (substr($fullNumber, 0, 5) == "88017") {
                    $allGPNumber[] = $fullNumber;
                } elseif (substr($fullNumber, 0, 5) == "88018") {
                    $allRobiNumber[] = $fullNumber;
                } elseif (substr($fullNumber, 0, 5) == "88019") {
                    $allBanglalinkNumber[] = $fullNumber;
                } else {
                    $message = "Please check your number";
                    Session::flash('error', $message);
                    return back();
                }

            }
        }

        $smsType = $request->smsType==0 ? 'text' : 'unicode';
        $smsContent = rawurlencode($request->contents);
        $masking_name = $request->sender;

        // Special characters
        if (preg_match('/[\'\^{}\\\\\\[~\\]|€]/', $request->contents))
        {
            $specila_text = "yes";
        } else {
            $specila_text = "no";
        }

        // SMS Text Count
        if (strlen($request->contents) != strlen(utf8_decode($request->contents)))
        {
            $smsTextCount = mb_strlen($request->contents, 'UTF-8');
        } else {
            $smsTextCount = strlen($request->contents);
        }

        $count = Helper::SmsCount($smsType, $smsTextCount, $specila_text);

        if ($count == 0) {
            Session::flash('error', 'Message text is too long');
            return back();
        }

        $sendRoute = [];
        $sendLog = [];

        if (!empty($allTeleTalkNumber)) {
            $allNumbers = json_encode($allTeleTalkNumber);
            if ($masking_name=="non_masking") {
                if ($smsType=="text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88015, "non_masking");
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88015, $masking_name),
                    'ttl_number' => count($allTeleTalkNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            } else {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88015, $masking_name);
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88015, $masking_name),
                    'ttl_number' => count($allTeleTalkNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }

        if (!empty($allAirtelNumber)) {
            $allNumbers = json_encode($allAirtelNumber);
            if ($masking_name=="non_masking") {
                if ($smsType=="text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88016, "non_masking");
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88016, $masking_name),
                    'ttl_number' => count($allAirtelNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            } else {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88016, $masking_name);
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88016, $masking_name),
                    'ttl_number' => count($allAirtelNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }

        if (!empty($allGPNumber)) {
            $allNumbers = json_encode($allGPNumber);
            if ($masking_name == "non_masking") {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88017, "non_masking");
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88017, $masking_name),
                    'ttl_number' => count($allGPNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            } else {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88017, $masking_name);
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88017, $masking_name),
                    'ttl_number' => count($allGPNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }

        if (!empty($allRobiNumber)) {
            $allNumbers = json_encode($allRobiNumber);
            if ($masking_name=="non_masking") {
                if ($smsType=="text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88018, "non_masking");
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88018, $masking_name),
                    'ttl_number' => count($allRobiNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            } else {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88018, $masking_name);
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88018, $masking_name),
                    'ttl_number' => count($allRobiNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }

        if (!empty($allBanglalinkNumber)) {
            $allNumbers = json_encode($allBanglalinkNumber);
            if ($masking_name == "non_masking") {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88019, "non_masking");
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88019, $masking_name),
                    'ttl_number' => count($allBanglalinkNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            } else {
                if ($smsType == "text") {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name];
                } else {
                    $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                    $replaceText = [$smsType, $allNumbers, $smsContent, $masking_name, "datacoding=8"];
                }
                $url = Helper::sendUrl(88019, $masking_name);
                $curl_url = str_replace($originalText, $replaceText, $url);
                $send = Curl::to($curl_url)->get();
                $sendLog[] = [
                    'user_id' => Auth::id(),
                    'preview_sms_id' => $request->preview_sms_id,
                    'report' => $send
                ];
                $sendRoute[] = [
                    'sms_id' => $request->preview_sms_id,
                    'route_id' => Helper::sendID(88019, $masking_name),
                    'ttl_number' => count($allBanglalinkNumber),
                    'ttl_sms' => $count,
                    'created_at' => date('Y-m-d H:i:s'),
                    'updated_at' => date('Y-m-d H:i:s')
                ];
            }
        }

        $totalSendNumbers = Count($allTeleTalkNumber) + count($allAirtelNumber) + count($allGPNumber) + count($allRobiNumber) + count($allBanglalinkNumber);

        // Save Database
        $updateData = Sms::find($request->preview_sms_id);
        $updateData->sender = $masking_name;
        $updateData->contents = $request->contents;
        $updateData->sms_type = $request->smsType;
        $updateData->masking = $masking_name == 'non_masking' ? 0 : 1;
        $updateData->ttl_number = $totalSendNumbers;
        $updateData->ttl_sms = $count;
        $updateData->ttl_cost = $request->cost;
        $updateData->status = 1;
        $updateData->save();

        Sendroute::insert($sendRoute);
        SendLog::insert($sendLog);

        // Transaction
        $totalCost = $updateData->smscost->sum('cost');

        // SMS Send Count Save in Transaction Table
        $url = url('report/all-sms/view', $updateData->id);
        $message_transcation[] = [
            'user_id' => Auth::id(),
            'state' => 0,
            'payment_method' => 'Send SMS',
            'description' => "Send SMS <a class=\"btn btn-link\" href=\"$url\">Details</a> ",
            'amount' => $totalCost * - 1,
            'created_at' => Carbon::now()
        ];

        $message_balance[] = [
            'user_id' => Auth::id(),
            'state' => 0,
            'payment_method' => 'Send SMS',
            'description' => "Send SMS <a class=\"btn btn-link\" href=\"$url\">Details</a> ",
            'amount' => $totalCost * - 1,
            'masking_sms' => $masking_name=='non_masking' ? 0 : $count *-1,
            'non_masking_sms' => $masking_name=='non_masking' ? $count * -1 : 0,
            'created_at' => Carbon::now()
        ];

        // Parents A/C reduse sms balance without admin
        if (Auth::id() != 1) {
            $message_transcation[] = [
                'user_id' => Auth::user()->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send SMS <a class=\"btn btn-link\" href=\"$url\">Details</a> ",
                'amount' => $totalCost * -1,
                'created_at' => Carbon::now()
            ];

            $message_balance[] = [
                'user_id' => Auth::user()->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send SMS <a class=\"btn btn-link\" href=\"$url\">Details</a> ",
                'amount' => $totalCost * - 1,
                'masking_sms' => $masking_name=='non_masking' ? 0 : $count *-1,
                'non_masking_sms' => $masking_name=='non_masking' ? $count * -1 : 0,
                'created_at' => Carbon::now()
            ];
        }

        $reseller = User::find(Auth::user()->member_id);

        if ($reseller->member_id != 1) {
            $message_transcation[] = [
                'user_id'       => $reseller->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send SMS <a class=\"btn btn-link\" href=\"$url\">Details</a> ",
                'amount' => $totalCost * -1,
                'created_at' => Carbon::now()
            ];

            $message_balance[] = [
                'user_id' => $reseller->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send SMS <a class=\"btn btn-link\" href=\"$url\">Details</a> ",
                'amount' => $totalCost * - 1,
                'masking_sms' => $masking_name=='non_masking' ? 0 : $count *-1,
                'non_masking_sms' => $masking_name=='non_masking' ? $count * -1 : 0,
                'created_at' => Carbon::now()
            ];
        }

        // Sms::insert($message_send_info);
        Transaction::insert($message_transcation);
        Balance::insert($message_balance);


        $message = "Message Send Successfully";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return redirect()->route('send-sms.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function show(Sms $sms)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function edit(Sms $sms)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sms $sms)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sms  $sms
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sms $sms)
    {
        //
    }
}
