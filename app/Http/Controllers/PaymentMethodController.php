<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Http\Requests\PaymentMethodRequest;
use App\Log;
use App\PaymentMethod;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PaymentMethodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allPaymentMethod = PaymentMethod::all();
        $paymentMethod = new PaymentMethod;
        $col_data=array();
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allPaymentMethod as $value) {
            $form_url = route('payment-method.destroy', $value->id);
            $edit_url = route('payment-method.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->name,
                Helper::status_change($status_change_url, 'payment_methods', $value->id, 'status', $value->status, 'Active', 'Pending', 'Payment Method Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.payment_method.index', compact('col_heads', 'col_data', 'paymentMethod'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PaymentMethodRequest $request)
    {
        PaymentMethod::create($request->all());

        $message = "Payment Method Add";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function show(PaymentMethod $paymentMethod)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function edit(PaymentMethod $paymentMethod)
    {
        $allPaymentMethod = PaymentMethod::all();
        $col_data=array();
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allPaymentMethod as $value) {
            $form_url = route('payment-method.destroy', $value->id);
            $edit_url = route('payment-method.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->name,
                Helper::status_change($status_change_url, 'payment_methods', $value->id, 'status', $value->status, 'Active', 'Pending', 'Payment Method Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.payment_method.index', compact('col_heads', 'col_data', 'paymentMethod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function update(PaymentMethodRequest $request, PaymentMethod $paymentMethod)
    {
        $paymentMethod->fill($request->all());
        $paymentMethod->save();

        $message = "Payment Method Edit";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PaymentMethod  $paymentMethod
     * @return \Illuminate\Http\Response
     */
    public function destroy(PaymentMethod $paymentMethod)
    {
        $paymentMethod->delete();

        $message = "Payment Method Delete";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
