<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Country;
use App\Http\Requests\PricingCoverageRequest;
use App\Log;
use App\Operator;
use App\Pricing_coverages;
use App\Smsgateway;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PricingCoveragesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('username', $request->name)->first();
        $pricing_coverage = new Pricing_coverages;
        $operators = Operator::where('status', 1)->get();
        $countries = Country::all();
        $smsGateways = Smsgateway::where('status', 1)->get();
        $col_data=array();
        $allDefaultPricing = Pricing_coverages::with(['country:id,name','operator:id,name,prefix'])->where('user_id', $user->id)->get();
        $col_heads = array(
            trans('messages.Country'),
            trans('messages.Operator'),
            trans('messages.SMS Gateway'),
            trans('messages.Prefix'),
            trans('messages.Masking Price'),
            trans('messages.Non Masking Price'),
            trans('messages.Sell Masking Price'),
            trans('messages.Sell Non Masking Price'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allDefaultPricing as $value) {
            $form_url = route('pricing-coverages.destroy', $value->id);
            $edit_url = route('pricing-coverages.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->country->name,
                $value->operator->name,
                $value->type ? "Masking" : "Non Masking",
                $value->operator->prefix,
                $value->masking_price,
                $value->non_masking_price,
                $value->sell_msk_price,
                $value->sell_non_msk_price,
                Helper::status_change($status_change_url, 'pricing_coverages', $value->id, 'status', $value->status, 'Active', 'Pending', 'Pricing Coverage Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('pricing_coverage.index', compact('col_heads', 'col_data', 'operators', 'countries', 'pricing_coverage', 'smsGateways', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PricingCoverageRequest $request)
    {
        $alreadyExist = Pricing_coverages::where([
            ['country_id', $request->country_id],
            ['operator_id',$request->operator_id],
            ['user_id',$request->user_id],
        ])->get();

        if (count($alreadyExist)) {
            Session::flash('warning', 'Operator Already Exist');
            return back();
        }

        $data = $request->all();

        $pricingCoverage = new Pricing_coverages;
        $pricingCoverage->fill($data);
        $pricingCoverage->save();

        $message = "Add Pricing Coverage";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Pricing_coverages  $pricing_coverages
     * @return \Illuminate\Http\Response
     */
    public function show(Pricing_coverages $pricing_coverages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Pricing_coverages  $pricing_coverages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pricing_coverage = Pricing_coverages::with('user')->where('id', $id)->first();
        $user = User::where('id', $pricing_coverage->user_id)->first();
        $operators = Operator::where('status', 1)->get();
        $countries = Country::all();
        $smsGateways = Smsgateway::where('status', 1)->get();
        $col_data=array();
        $allDefaultPricing = Pricing_coverages::with(['country:id,name','operator:id,name,prefix'])->where('user_id', $pricing_coverage->user_id)->get();
        $col_heads = array(
            trans('messages.Country'),
            trans('messages.Operator'),
            trans('messages.SMS Gateway'),
            trans('messages.Prefix'),
            trans('messages.Masking Price'),
            trans('messages.Non Masking Price'),
            trans('messages.Sell Masking Price'),
            trans('messages.Sell Non Masking Price'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allDefaultPricing as $value) {
            $form_url = route('pricing-coverages.destroy', $value->id);
            $edit_url = route('pricing-coverages.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->country->name,
                $value->operator->name,
                $value->type ? "Masking" : "Non Masking",
                $value->operator->prefix,
                $value->masking_price,
                $value->non_masking_price,
                $value->sell_msk_price,
                $value->sell_non_msk_price,
                Helper::status_change($status_change_url, 'pricing_coverages', $value->id, 'status', $value->status, 'Active', 'Pending', 'Pricing Coverage Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('pricing_coverage.index', compact('col_heads', 'col_data', 'operators', 'countries', 'pricing_coverage', 'smsGateways', 'user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Pricing_coverages  $pricing_coverages
     * @return \Illuminate\Http\Response
     */
    public function update(PricingCoverageRequest $request, $id)
    {
        $pricingCoverage = Pricing_coverages::find($id);

        $data = $request->all();

        $pricingCoverage->fill($data);
        $pricingCoverage->save();

        $message = "Edit Pricing Coverage";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Pricing_coverages  $pricing_coverages
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pricingCoverage = Pricing_coverages::find($id);
        $pricingCoverage->delete();

        $message = "Delete Pricing Coverage";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
