<?php

namespace App\Http\Controllers;

use App\Default_pricing;
use App\Http\Requests\CustomerRequest;
use App\Log;
use App\Pricing_coverages;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customer = new User;
        return view('customer.create', compact('customer'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {
        // Add User
        $data = $request->all();
        $data['password'] = Hash::make($request->password);
        $data['member_id'] = Auth::id();
        $customer = new User;
        $customer->fill($data);
        $customer->save();

        // Add Role
        $customer->roles()->attach($request->role);

        // Add Pricing
        if (1 == Auth::user()->role) {
            $defaultPricing = Default_pricing::where('status', 1)->get([
                'id',
                'country_id',
                'operator_id',
                'sms_gateway_masking_id',
                'sms_gateway_non_masking_id',
                'masking_price',
                'non_masking_price',
                'sell_msk_price',
                'sell_non_msk_price',
                'status'
            ]);
        } else {
            $defaultPricing = Pricing_coverages::where([['status', 1], ['user_id', Auth::id()]])->get([
                'id',
                'country_id',
                'operator_id',
                'sms_gateway_masking_id',
                'sms_gateway_non_masking_id',
                'masking_price',
                'non_masking_price',
                'sell_msk_price',
                'sell_non_msk_price',
                'status'
            ]);
        }

        $user_id = $customer->id;
        foreach ($defaultPricing as $value) {
            $col_data[] = [
                'user_id' => $user_id,
                'country_id' => $value->country_id,
                'operator_id' => $value->operator_id,
                'sms_gateway_masking_id' => $value->sms_gateway_masking_id,
                'sms_gateway_non_masking_id' => $value->sms_gateway_non_masking_id,
                'masking_price' => $value->sell_msk_price,
                'non_masking_price' => $value->sell_non_msk_price,
                'sell_msk_price' => $value->sell_msk_price + .10,
                'sell_non_msk_price' => $value->sell_non_msk_price + .10,
                'status' => $value->status
            ];
        }

        Pricing_coverages::insert($col_data);

        // Add Profile


        $message = "Add Customer";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
