<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Template;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\TemplateRequest;
use Illuminate\Support\Facades\Session;
use App\Log;
use App\Classes\Helper;
use App\Number;

class TemplateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = Template::where('user_id', Auth::id())->get();
        $col_data=array();
        $template = new Template;
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Content'),
            trans('messages.Created At'),
            trans('messages.Option')
        );

        foreach ($templates as $value) {
            $form_url = route('template.destroy', $value->id);
            $edit_url = route('template.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->content,
                $value->created_at,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }
        
        return view('template.index', compact('col_data','col_heads', 'template'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TemplateRequest $request)
    {
        $template = new Template;
        $template->user_id = Auth::id();
        $template->name = $request->name;
        $template->content = $request->content;
        $template->save();

        $message = "New Template Create";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\template  $template
     * @return \Illuminate\Http\Response
     */
    public function show(template $template)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\template  $template
     * @return \Illuminate\Http\Response
     */
    public function edit(template $template)
    {
        $templates = Template::where('user_id', Auth::id())->get();
        $col_data=array();
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Content'),
            trans('messages.Created At'),
            trans('messages.Option')
        );

        foreach ($templates as $value) {
            $form_url = route('template.destroy', $value->id);
            $edit_url = route('template.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->content,
                $value->created_at,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }
        
        return view('template.index', compact('col_data', 'col_heads', 'template'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\template  $template
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, template $template)
    {
        $template->name = $request->name;
        $template->content = $request->content;
        $template->save();

        $message = "Edit Template Create";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\template  $template
     * @return \Illuminate\Http\Response
     */
    public function destroy(template $template)
    {
        $template->delete();

        $message = "Delete Template Create";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();

    }
}
