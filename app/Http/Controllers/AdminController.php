<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Classes\Helper;
use App\Default_pricing;
use App\Log;
use App\Pricing_coverages;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $admins = User::where('role', 1)->get();

        return view('admin.index', compact('admins'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $user = User::where('username', $name)->first();
        $pricing = Pricing_coverages::with(['country:id,iso', 'operator:id,name,prefix', 'masking_gateway:id,user_display_name', 'non_masking_gateway:id,user_display_name'])->where('user_id', $user->id)->get();
        $balance = Balance::where([['user_id', $user->id], ['state', 1]])->latest()->take(25)->get();

        return view('admin.show', compact('user', 'balance', 'pricing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $admin = User::find($id);
        return view('admin.create', compact('admin'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // Update User
        $customer = User::find($id);
        $customer->first_name =$request->first_name;
        $customer->last_name =$request->last_name;
        $customer->address =$request->address;
        $customer->role =$request->role;
        if (isset($request->password)) {
            $customer->password = Hash::make($request->password);;
        }
        if (isset($request->average_sms_price)) {
            $customer->average_sms_price = $request->average_sms_price;
        }
        $customer->status =$request->status;

        $customer->save();

        // Update Role
        $customer->roles()->sync($request->role);


        $message = "Edit Admin";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $admin = User::find($id);
        $admin->delete();

        $message = "Delete Customer";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }


}
