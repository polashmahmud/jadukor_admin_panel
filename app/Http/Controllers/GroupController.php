<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Group;
use App\Http\Requests\GroupRequest;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allGroup = Group::where('user_id', Auth::id())->get();
        $col_data=array();
        $group = new Group;
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allGroup as $value) {
            $form_url = route('group.destroy', $value->id);
            $edit_url = route('group.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->name,
                Helper::status_change($status_change_url, 'groups', $value->id, 'status', $value->status, 'Active', 'Pending', 'Phone Group Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('group.index', compact('col_heads', 'col_data', 'group'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(GroupRequest $request)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        Group::create($data);

        $message = "Phone Group Add";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\PhoneGroup  $phoneGroup
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\PhoneGroup  $phoneGroup
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $allGroup = Group::where('user_id', Auth::id())->get();
        $group = Group::find($id);
        $col_data=array();
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allGroup as $value) {
            $form_url = route('group.destroy', $value->id);
            $edit_url = route('group.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->name,
                Helper::status_change($status_change_url, 'groups', $value->id, 'status', $value->status, 'Active', 'Pending', 'Phone Group Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('group.index', compact('col_heads', 'col_data', 'group'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\PhoneGroup  $phoneGroup
     * @return \Illuminate\Http\Response
     */
    public function update(GroupRequest $request, $id)
    {
        $data = $request->all();
        $group = Group::find($id);
        $group->fill($data);
        $group->save();

        $message = "Phone Group Edit";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\PhoneGroup  $phoneGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $group = Group::find($id);
        $group->delete();

        $message = "Phone Group Delete";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
