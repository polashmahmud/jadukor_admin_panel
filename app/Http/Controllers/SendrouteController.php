<?php

namespace App\Http\Controllers;

use App\Sendroute;
use Illuminate\Http\Request;

class SendrouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sendroute  $sendroute
     * @return \Illuminate\Http\Response
     */
    public function show(Sendroute $sendroute)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sendroute  $sendroute
     * @return \Illuminate\Http\Response
     */
    public function edit(Sendroute $sendroute)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sendroute  $sendroute
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sendroute $sendroute)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sendroute  $sendroute
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sendroute $sendroute)
    {
        //
    }
}
