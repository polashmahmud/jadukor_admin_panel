<?php

namespace App\Http\Controllers;

use App\SmsCost;
use Illuminate\Http\Request;

class SmsCostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\SmsCost  $smsCost
     * @return \Illuminate\Http\Response
     */
    public function show(SmsCost $smsCost)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\SmsCost  $smsCost
     * @return \Illuminate\Http\Response
     */
    public function edit(SmsCost $smsCost)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\SmsCost  $smsCost
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, SmsCost $smsCost)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\SmsCost  $smsCost
     * @return \Illuminate\Http\Response
     */
    public function destroy(SmsCost $smsCost)
    {
        //
    }
}
