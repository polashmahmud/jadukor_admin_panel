<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Sms;
use App\Smsgateway;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Number;
use Maatwebsite\Excel\Facades\Excel;
use App\Sendroute;

class ReportController extends Controller
{
    public function allSms(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($request->date == 'today') {
            $allData = Sms::Where([
                ['user_id', Auth::id()],
                ['status', 1],
                ['created_at', $today]
            ])->latest()->paginate(25);
            $value = 'today';
        } elseif ($request->date == 'week') {
            $allData = Sms::where([
                ['user_id', Auth::id()],
                ['status', 1],
                ['created_at', '>=', $week],
                ['created_at', '<=', $today]
            ])->latest()->paginate(25);
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            $allData = Sms::where([
                ['user_id', Auth::id()],
                ['status', 1],
                [DB::raw('Month(created_at)'), '=', $this_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            $allData = Sms::where([
                ['user_id', Auth::id()],
                ['status', 1],
                [DB::raw('Month(created_at)'), '=', $last_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            $allData = Sms::where([
                ['user_id', Auth::id()],
                ['status', 1],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            $allData = Sms::where([
                ['user_id', Auth::id()],
                ['status', 1],
                [DB::raw('Year(created_at)'), '=', $last_year]
            ])->latest()->paginate(25);
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            $allData = Sms::where([
                ['user_id', Auth::id()],
                ['status', 1],
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $end_date]
            ])->latest()->paginate(25);
        } else {
            $allData = Sms::Where([
                ['user_id', Auth::id()],
                ['status', 1],
                ['created_at', $today]
            ])->latest()->paginate(25);
            $value = 'today';
        }

        return view('report.all_sms', compact('allData', 'value'));
    }

    public function allSmsView($id)
    {
        $allData = Number::where('preview_sms_id', $id)->get();

        return view('report.all_sms_view', compact('allData'));
    }

    public function download($id) 
    {
        $data = Number::where('preview_sms_id', $id)
            ->select('number as mobile', 'created_at as send_time', 'sender', 'cost as charge')
            ->get();
        
        return Excel::create('sms_details', function ($excel) use ($data) {
            $excel->sheet('SMS', function ($sheet) use ($data) {
                $sheet->fromArray($data);
            });
        })->download('xls');
    }

    public function routingSmsTotal(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($request->date == 'today') {
            $allData = Sendroute::Where([
                ['created_at', $today]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'today';
        } elseif ($request->date == 'week') {
            $allData = Sendroute::Where([
                ['created_at', '>=', $week],
                ['created_at', '<=', $today]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            $allData = Sendroute::Where([
                [DB::raw('Month(created_at)'), '=', $this_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            $allData = Sendroute::Where([
                [DB::raw('Month(created_at)'), '=', $last_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            $allData = Sendroute::Where([
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            $allData = Sendroute::Where([
                [DB::raw('Year(created_at)'), '=', $last_year]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            $allData = Sendroute::Where([
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $end_date]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
        } else {
            $allData = Sendroute::Where([
                ['created_at', $today]
            ])->selectRaw('route_id, sum(ttl_sms) as total_sms')->groupBy('route_id')->get();
            $value = 'today';
        }

        return view('report.routing_total_sms', compact('allData', 'value'));
    }

    public function routingSms(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $send_route = $request->route_id;


        if ($request->date == 'today') {
            $allData = Sendroute::Where([
                ['route_id', $send_route],
                ['created_at', $today]
            ])->latest()->paginate(25);
            $value = 'today';
        } elseif ($request->date == 'week') {
            $allData = Sendroute::where([
                ['route_id', $send_route],
                ['created_at', '>=', $week],
                ['created_at', '<=', $today]
            ])->latest()->paginate(25);
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            $allData = Sendroute::where([
                ['route_id', $send_route],
                [DB::raw('Month(created_at)'), '=', $this_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            $allData = Sendroute::where([
                ['route_id', $send_route],
                [DB::raw('Month(created_at)'), '=', $last_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            $allData = Sendroute::where([
                ['route_id', $send_route],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            $allData = Sendroute::where([
                ['route_id', $send_route],
                [DB::raw('Year(created_at)'), '=', $last_year]
            ])->latest()->paginate(25);
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            $allData = Sendroute::where([
                ['route_id', $send_route],
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $end_date]
            ])->latest()->paginate(25);
        } else {
            $allData = Sendroute::Where([
                ['created_at', $today]
            ])->latest()->paginate(25);
            $value = 'today';
        }

        $smsGateways = Smsgateway::where('status', 1)->get();

        return view('report.routing_wise_sms_details', compact('allData', 'value', 'smsGateways', 'send_route'));
    }

    public function userUsage(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($request->date == 'today') {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    ['created_at', $today]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    ['created_at', $today]
                ])->get();
            }
            $value = 'today';
        } elseif ($request->date == 'week') {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    ['created_at', '>=', $week],
                    ['created_at', '<=', $today]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    ['created_at', '>=', $week],
                    ['created_at', '<=', $today]
                ])->get();
            }
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    [DB::raw('Month(created_at)'), '=', $this_month],
                    [DB::raw('Year(created_at)'), '=', $this_year]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    [DB::raw('Month(created_at)'), '=', $this_month],
                    [DB::raw('Year(created_at)'), '=', $this_year]
                ])->get();
            }
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    [DB::raw('Month(created_at)'), '=', $last_month],
                    [DB::raw('Year(created_at)'), '=', $this_year]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    [DB::raw('Month(created_at)'), '=', $last_month],
                    [DB::raw('Year(created_at)'), '=', $this_year]
                ])->get();
            }
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    [DB::raw('Year(created_at)'), '=', $this_year]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    [DB::raw('Year(created_at)'), '=', $this_year]
                ])->get();
            }
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    [DB::raw('Year(created_at)'), '=', $last_year]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    [DB::raw('Year(created_at)'), '=', $last_year]
                ])->get();
            }
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    ['created_at', '>=', $start_date],
                    ['created_at', '<=', $end_date]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    ['created_at', '>=', $start_date],
                    ['created_at', '<=', $end_date]
                ])->get();
            }
        } else {
            if (Auth::user()->role == 1) {
                $allData = User::Where([
                    ['created_at', $today]
                ])->get();
            } else {
                $allData = User::Where([
                    ['member_id', Auth::id()],
                    ['created_at', $today]
                ])->get();
            }
            $value = 'today';
        }

        return view('report.user_usage', compact('allData', 'value'));
    }

    public function userUsageDetails(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;
        $user_id = $request->user_id;


        if ($request->date == 'today') {
            $allData = Sms::Where([
                ['user_id', $user_id],
                ['status', 1],
                ['created_at', $today]
            ])->latest()->paginate(25);
            $value = 'today';
        } elseif ($request->date == 'week') {
            $allData = Sms::where([
                ['user_id', $user_id],
                ['status', 1],
                ['created_at', '>=', $week],
                ['created_at', '<=', $today]
            ])->latest()->paginate(25);
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            $allData = Sms::where([
                ['user_id', $user_id],
                ['status', 1],
                [DB::raw('Month(created_at)'), '=', $this_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            $allData = Sms::where([
                ['user_id', $user_id],
                ['status', 1],
                [DB::raw('Month(created_at)'), '=', $last_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            $allData = Sms::where([
                ['user_id', $user_id],
                ['status', 1],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->latest()->paginate(25);
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            $allData = Sms::where([
                ['user_id', $user_id],
                ['status', 1],
                [DB::raw('Year(created_at)'), '=', $last_year]
            ])->latest()->paginate(25);
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            $allData = Sms::where([
                ['user_id', $user_id],
                ['status', 1],
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $end_date]
            ])->latest()->paginate(25);
        } else {
            $allData = Sms::Where([
                ['user_id', Auth::id()],
                ['status', 1],
                ['created_at', $today]
            ])->latest()->paginate(25);
            $value = 'today';
        }

        if (Auth::user()->role == 1) {
            $users = User::all();
        } else {
            $users = User::where('member_id', Auth::id())->get();
        }

        return view('report.user_sms_details', compact('allData', 'value', 'users', 'user_id'));
    }

    public function userBalance(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($request->date == 'today') {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                ['created_at', $today]
            ])->get();
            $value = 'today';
        } elseif ($request->date == 'week') {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                ['created_at', '>=', $week],
                ['created_at', '<=', $today]
            ])->get();
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                [DB::raw('Month(created_at)'), '=', $this_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->get();
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                [DB::raw('Month(created_at)'), '=', $last_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->get();
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->get();
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                [DB::raw('Year(created_at)'), '=', $last_year]
            ])->get();
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $end_date]
            ])->get();
        } else {
            $allData = Transaction::Where([
                ['user_id', Auth::id()],
                ['created_at', $today]
            ])->get();
            $value = 'today';
        }

        return view('report.user_balance', compact('allData', 'value'));
    }

    public function transaction(Request $request)
    {
        $today = date("Y/m/d");
        $week = date("Y/m/d", strtotime("-7 days"));
        $this_month = date("m");
        $last_month = date("m", strtotime("-1 month"));
        $this_year = date("Y");
        $last_year = date("Y", strtotime("-1 Year"));
        $start_date = $request->start_date;
        $end_date = $request->end_date;

        if ($request->date == 'today') {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                ['created_at', $today]
            ])->get();
            $value = 'today';
        } elseif ($request->date == 'week') {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                ['created_at', '>=', $week],
                ['created_at', '<=', $today]
            ])->get();
            $value = 'week';
        } elseif ($request->date == 'this_month') {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                [DB::raw('Month(created_at)'), '=', $this_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->get();
            $value = 'this_month';
        } elseif ($request->date == 'last_month') {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                [DB::raw('Month(created_at)'), '=', $last_month],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->get();
            $value = 'last_month';
        } elseif ($request->date == 'this_year') {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                [DB::raw('Year(created_at)'), '=', $this_year]
            ])->get();
            $value = 'this_year';
        } elseif ($request->date == 'last_year') {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                [DB::raw('Year(created_at)'), '=', $last_year]
            ])->get();
            $value = 'last_year';
        } elseif ($request->start_date && $request->end_date) {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                ['created_at', '>=', $start_date],
                ['created_at', '<=', $end_date]
            ])->get();
        } else {
            $allData = Balance::Where([
                ['user_id', Auth::id()],
                ['created_at', $today]
            ])->get();
            $value = 'today';
        }

        return view('report.transaction', compact('allData', 'value'));
    }




}
