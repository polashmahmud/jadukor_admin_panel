<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Group;
use App\Http\Requests\PhoneRequest;
use App\Log;
use App\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PhoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $groups = Group::where('user_id', Auth::id())->get();
        $phones = Phone::where('user_id', Auth::id())->get();
        $col_data=array();
        $phone = new Phone;

        return view('phone.index', compact('col_heads', 'col_data', 'groups', 'phone', 'phones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PhoneRequest $request)
    {
        $phone = new Phone;
        $phone->name = $request->name;
        $phone->number = $request->number;
        $phone->email = $request->email;
        $phone->user_id = Auth::id();
        $phone->save();

        $phone->groups()->attach($request->group_id);

        $message = "Contact Add";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function show(Phone $phone)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $groups = Group::where('user_id', Auth::id())->get();
        $phones = Phone::where('user_id', Auth::id())->get();
        $col_data=array();
        $phone = Phone::find($id);

        return view('phone.index', compact('col_heads', 'col_data', 'groups', 'phone', 'phones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $phone = Phone::find($id);
        $phone->number = $request->number;
        $phone->name = $request->name;
        $phone->email = $request->email;
        $phone->save();

        $phone->groups()->sync($request->group_id);

        $message = "Contact Edit";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Phone  $phone
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $phone = Phone::find($id);
        $phone->delete();

        $message = "Contact Delete";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
