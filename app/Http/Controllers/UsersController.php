<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Classes\Helper;
use App\Default_pricing;
use App\Http\Requests\CustomerRequest;
use App\Http\Requests\CustomerUpdateRequest;
use App\Log;
use App\Pricing_coverages;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::user()->role == 1) {
            $users = User::where('role', 4)->get();
        } else {
            $users = User::where([['role', 4], ['member_id', Auth::id()]])->get();
        }

        return view('user.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CustomerRequest $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $user = User::where('username', $name)->first();
        $pricing = Pricing_coverages::with(['country:id,iso', 'operator:id,name,prefix', 'masking_gateway:id,user_display_name', 'non_masking_gateway:id,user_display_name'])->where('user_id', $user->id)->get();
        $balance = Balance::where([['user_id', $user->id], ['state', 1]])->latest()->get();

        return view('user.show', compact('user', 'balance', 'pricing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('user.create', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CustomerUpdateRequest $request, $id)
    {
        // Update User
        $user = User::find($id);
        $user->first_name =$request->first_name;
        $user->last_name =$request->last_name;
        $user->address =$request->address;
        $user->role =$request->role;
        if (isset($request->password)) {
            $user->password = Hash::make($request->password);;
        }
        if (isset($request->average_sms_price)) {
            $user->average_sms_price = $request->average_sms_price;
        }
        $user->status =$request->status;
        $user->save();

        // Update Role
        $user->roles()->sync($request->role);

        $message = "Edit User";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        $message = "Delete Customer";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
