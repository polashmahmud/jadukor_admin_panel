<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Classes\Helper;
use App\Log;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BalanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->user_id);

        // Only For User Transfer Amount
//        if ($user->id != Auth::id()) {
//            Session::flash('warning', 'Permission Denied');
//            return back();
//        }

        $transactionBalance = Helper::transactionBalance($request->user_id);

        if ($user->price_type == 1) {
            if ($request->type==1) {
                $amount = $user->average_masking_price * $request->sms;
            }
            if ($request->type==0) {
                $amount = $user->average_non_masking_price * $request->sms;
            }
        } else {
            $amount = $request->amount;
        }

        if ($amount > $transactionBalance) {
            Session::flash('warning', 'There is not enough balance to transfer');
            return back();
        }

        $data = $request->all();
        $data['state'] = 1;
        $data['payment_method'] = 'Amount Transfer';

        if ($user->price_type == 1) {
            $data['amount'] = 0;
            if ($request->type==1) {
                $data['masking_sms'] = $request->sms;
                $data['non_masking_sms'] = 0;
            }
            if ($request->type==0) {
                $data['masking_sms'] = 0;
                $data['non_masking_sms'] = $request->sms;
            }
        } else {
            $data['masking_sms'] = 0;
            $data['non_masking_sms'] = 0;
        }

        Balance::create($data);

        $transaction = new Transaction;
        $transaction->user_id = $request->user_id;
        $transaction->state = 0;
        $transaction->payment_method = 'Transfer';
        $transaction->description = 'Transfer Amount to SMS Balance';
        $transaction->amount = $amount * -1;
        $transaction->save();

        $message = "Transfer Amount";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function show(Balance $balance)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function edit(Balance $balance)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Balance $balance)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Balance  $balance
     * @return \Illuminate\Http\Response
     */
    public function destroy(Balance $balance)
    {
        //
    }
}
