<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Classes\Helper;
use App\CustomSms;
use App\Log;
use App\Transaction;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Ixudra\Curl\Facades\Curl;
use Maatwebsite\Excel\Facades\Excel;


class CustomSmsController extends Controller
{
    public function index()
    {
        return view('send_message.custom');
    }

    public function draft()
    {
        $allPreviewSms = DB::table('preview_custom_sms')->where([['user_id', Auth::id()], ['status', 0]])->get();

        $col_data=array();
        $col_heads = array(
            trans('messages.Created At'),
            trans('messages.Description'),
            trans('messages.Schedule'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allPreviewSms as $value) {
//            $form_url = route('preview-sms.destroy', $value->id);
//            $edit_url = route('preview-sms.edit', $value->id);
            $show_url = route('custom-sms.show', $value->id);
            $col_data[] = array(
                $value->created_at,
                $value->description,
                $value->schedule_time,
                $value->status ? "Send" : "Save",
                "<a href=\"$show_url\"><i class=\"ti-user color-dark\"></i></a> "
//                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('custom_sms.draft', compact('col_heads', 'col_data'));
    }


    public function store(Request $request)
    {
        if ($request->hasFile('file')) {
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv" || $extension == "txt") {
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function ($reader) {
                    $reader->select(['number', 'sms']);
                })->get()->toArray();
            }
        } else {
            Session::flash('error', "No file found");
            return back();
        }

        $preview_id = DB::table('preview_custom_sms')->insertGetId([
           'user_id' => Auth::id(),
            'schedule' => $request->schedul_sms,
            'schedule_time' => $request->time,
            'status' => 0,
            'created_at'=> date('Y-m-d H:i:s'),
            'updated_at'=> date('Y-m-d H:i:s'),
        ]);
        $allData = [];
        foreach ($data as $value) {
            $fullNumber = '880' . substr(preg_replace('/\D/', '', $value['number']), -10);
            if (strlen(trim($fullNumber)) == 13) {
                $prefix = substr($fullNumber, 0, 5);
                $content = $value['sms'];
                // Special characters
                if (preg_match('/[\'\^{}\\\\\\[~\\]|€]/', $content)) {
                    $specila_text = "yes";
                } else {
                    $specila_text = "no";
                }

                // SMS Text Count
                if (strlen($content) != strlen(utf8_decode($content))) {
                    $smsTextCount = mb_strlen($content, 'UTF-8');
                    $smsType = 'unicode';
                } else {
                    $smsTextCount = strlen($content);
                    $smsType = 'text';
                }

                $count = Helper::SmsCount($smsType, $smsTextCount, $specila_text);

                $allData[] = [
                    'user_id' => Auth::id(),
                    'preview_custom_sms_id' => $preview_id,
                    'number' => trim($fullNumber),
                    'content' =>  $content,
                    'sender' =>  $request->sender_id,
                    'prefix' => $prefix,
                    'sms_type' => $specila_text=='no' ? 0 : 1,
                    'masking' => $request->sender_id == 'non_masking' ? 0 : 1,
                    'ttl_sms' => $count,
                    'sms_cost' => Auth::user()->price_type == 0 ? Helper::smsPrice($prefix, $request->sender_id) : 1,
                    'ttl_cost' => (Auth::user()->price_type == 0 ? Helper::smsPrice($prefix, $request->sender_id) : 1) * $count,
                    'schedule' => $request->schedul_sms,
                    'schedule_time' => $request->time,
                    'status' => 0,
                    'created_at'=> date('Y-m-d H:i:s'),
                    'updated_at'=> date('Y-m-d H:i:s'),
                ];
            }
        }

        CustomSms::insert($allData);

        return redirect()->route('custom-sms.show', $preview_id);

    }

    public function show($id)
    {
        $allSms = CustomSms::where('preview_custom_sms_id', $id)->get();

        $grouped = CustomSms::where('preview_custom_sms_id', $id)
            ->select(DB::raw('prefix, sum(ttl_sms) as ttl_sms, sum(ttl_cost) as ttl_cost'))
            ->groupBy('prefix')
            ->get();

        return view('custom_sms.show', compact('allSms', 'grouped', 'id'));
    }

    public function destroy($id)
    {
        CustomSms::find($id)->delete();

        return back();
    }

    public function sendSMS(Request $request)
    {
        // User and reseller Balance Check
        if (Auth::user()->price_type==0) {
            $userBalance = Helper::Balance(Auth::id());
            $resellerBalance = Helper::Balance(Auth::user()->member_id);
            $cost = $request->cost;
            if ($cost > $userBalance) {
                $message = "There is not enough balance";
                Session::flash('error', $message);
                return back();
            }
            if ($cost > $resellerBalance) {
                $message = "Contact your Administrator";
                Session::flash('error', $message);
                return back();
            }
        } else {
            if ($request->sender=='non_masking') {
                $userBalance = Helper::BalanceNonMaskingSMS(Auth::id());
                $resellerBalance = Helper::BalanceNonMaskingSMS(Auth::user()->member_id);
                $cost = $request->cost;
                if ($cost > $userBalance) {
                    $message = "There is not enough balance";
                    Session::flash('error', $message);
                    return back();
                }
                if ($cost > $resellerBalance) {
                    $message = "Contact your Administrator";
                    Session::flash('error', $message);
                    return back();
                }
            } else {
                $userBalance = Helper::BalanceMaskingSMS(Auth::id());
                $resellerBalance = Helper::BalanceMaskingSMS(Auth::user()->member_id);
                $cost = $request->cost;
                if ($cost > $userBalance) {
                    $message = "There is not enough balance";
                    Session::flash('error', $message);
                    return back();
                }
                if ($cost > $resellerBalance) {
                    $message = "Contact your Administrator";
                    Session::flash('error', $message);
                    return back();
                }
            }
        }

        $numbers = CustomSms::Where([['preview_custom_sms_id', $request->preview_id], ['ttl_cost', '>', 0]])->get();
        $wrongNumbers = CustomSms::Where([['preview_custom_sms_id', $request->preview_id], ['ttl_cost', '<=', 0]])->get();

        foreach ($numbers as $number) {
            $prefix = $number->prefix;
            $masking_name = $number->sender;
            $smsType = $number->sms_type ? "unicode" : "text";

            if (88013 == $prefix) {
                if ("non_masking" == $masking_name) {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88013, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                } else {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88013, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                }
            }

            if (88015 == $prefix) {
                if ("non_masking" == $masking_name) {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88015, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                } else {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88015, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                }
            }

            if (88016 == $prefix) {
                if ("non_masking" == $masking_name) {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88016, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                } else {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88016, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                }
            }

            if (88017 == $prefix) {
                if ("non_masking" == $masking_name) {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88017, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                } else {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88017, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                }
            }

            if (88018 == $prefix) {
                if ("non_masking" == $masking_name) {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88018, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                } else {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88018, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                }
            }

            if (88019 == $prefix) {
                if ("non_masking" == $masking_name) {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88019, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                } else {
                    if ("text" == $smsType) {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name];
                    } else {
                        $originalText = ["%text%", "%number%", "%MessageContent%", "%maskingID%", "datacoding=0"];
                        $replaceText = [$smsType, $number->number, rawurlencode($number->content), $masking_name, "datacoding=8"];
                    }
                    $url = Helper::sendUrl(88019, $masking_name);
                    $curl_url = str_replace($originalText, $replaceText, $url);
                    $send = Curl::to($curl_url)->get();
                }
            }

            $number->status = 1;
            $number->log = $send;
            $number->save();
        }

        DB::table('preview_custom_sms')->where('id', $request->preview_id)->update(['status' => 1]);

        // Transaction
        $totalCost = $cost;

        // SMS Send Count Save in Transaction Table
        $message_transcation[] = [
            'user_id' => Auth::id(),
            'state' => 0,
            'payment_method' => 'Send SMS',
            'description' => "Send Custom SMS",
            'amount' => $totalCost * - 1,
            'created_at' => Carbon::now()
        ];

        $message_balance[] = [
            'user_id' => Auth::id(),
            'state' => 0,
            'payment_method' => 'Send SMS',
            'description' => "Send Custom SMS",
            'amount' => $totalCost * - 1,
            'masking_sms' => $masking_name=='non_masking' ? 0 : -1,
            'non_masking_sms' => $masking_name=='non_masking' ? -1 : 0,
            'created_at' => Carbon::now()
        ];

        // Parents A/C reduse sms balance without admin
        if (Auth::id() != 1) {
            $message_transcation[] = [
                'user_id' => Auth::user()->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send Custom SMS",
                'amount' => $totalCost * -1,
                'created_at' => Carbon::now()
            ];

            $message_balance[] = [
                'user_id' => Auth::user()->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send Custom SMS",
                'amount' => $totalCost * - 1,
                'masking_sms' => $masking_name=='non_masking' ? 0 : -1,
                'non_masking_sms' => $masking_name=='non_masking' ? -1 : 0,
                'created_at' => Carbon::now()
            ];
        }

        $reseller = User::find(Auth::user()->member_id);

        if ($reseller->member_id != 1) {
            $message_transcation[] = [
                'user_id'       => $reseller->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send Custom SMS",
                'amount' => $totalCost * -1,
                'created_at' => Carbon::now()
            ];

            $message_balance[] = [
                'user_id' => $reseller->member_id,
                'state' => 0,
                'payment_method' => 'Send SMS',
                'description' => "Send Custom SMS",
                'amount' => $totalCost * - 1,
                'masking_sms' => $masking_name=='non_masking' ? 0 : -1,
                'non_masking_sms' => $masking_name=='non_masking' ? -1 : 0,
                'created_at' => Carbon::now()
            ];
        }

        // Sms::insert($message_send_info);
        Transaction::insert($message_transcation);
        Balance::insert($message_balance);


        $message = "Custom Message Send Successfully";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return redirect()->route('custom-sms.index');


    }
}
