<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Country;
use App\Default_pricing;
use App\Http\Requests\DefaultPricingRequest;
use App\Log;
use App\Operator;
use App\Smsgateway;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class DefaultPricingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = Operator::where('status', 1)->get();
        $countries = Country::all();
        // Non-Masking Gateways
        $smsGateways = Smsgateway::where([['status', 1],['type',0]])->get();
        // Masking Gateways
        $smsGatewaysMasking = Smsgateway::where([['status', 1],['type',1]])->get();
        $col_data=array();
        $default_pricing = new Default_pricing;
        $allDefaultPricing = Default_pricing::with([
            'country:id,name',
            'operator:id,name,prefix',
            'masking_gateway:id,user_display_name',
            'non_masking_gateway:id,user_display_name'
        ])->get();

        $col_heads = array(
            trans('messages.Country'),
            trans('messages.Operator'),
            trans('messages.SMS Gateway Masking'),
            trans('messages.SMS Gateway Non-Masking'),
            trans('messages.Prefix'),
            trans('messages.Masking Price'),
            trans('messages.Non Masking Price'),
            trans('messages.Sell Masking Price'),
            trans('messages.Sell Non Masking Price'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allDefaultPricing as $value) {
            $form_url = route('default-pricing.destroy', $value->id);
            $edit_url = route('default-pricing.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->country->name,
                $value->operator->name,
                $value->masking_gateway->user_display_name,
                $value->non_masking_gateway->user_display_name,
                $value->operator->prefix,
                $value->masking_price,
                $value->non_masking_price,
                $value->sell_msk_price,
                $value->sell_non_msk_price,
                Helper::status_change($status_change_url, 'default_pricings', $value->id, 'status', $value->status, 'Active', 'Pending', 'Default Pricing Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('default_pricing.index', compact('col_heads', 'col_data', 'operators', 'countries', 'default_pricing', 'smsGateways', 'smsGatewaysMasking'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(DefaultPricingRequest $request)
    {
        $alreadyExist = Default_pricing::where([
            ['country_id', $request->country_id],
            ['operator_id',$request->operator_id],
        ])->get();

        if (count($alreadyExist)) {
            Session::flash('warning', 'Operator Already Exist');
            return back();
        }

        $data = $request->all();
        $data['user_id'] = Auth::id();

        $defaultPricing = new Default_pricing;
        $defaultPricing->fill($data);
        $defaultPricing->save();

        $message = "Add Default Pricing";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Default_pricing  $default_pricing
     * @return \Illuminate\Http\Response
     */
    public function show(Default_pricing $default_pricing)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Default_pricing  $default_pricing
     * @return \Illuminate\Http\Response
     */
    public function edit(Default_pricing $default_pricing)
    {
        $operators = Operator::where('status', 1)->get();
        $countries = Country::all();
        $smsGateways = Smsgateway::where('status', 1)->get();
        $smsGatewaysMasking = Smsgateway::where([['status', 1],['type',1]])->get();
        $col_data=array();
        $allDefaultPricing = Default_pricing::with(['country:id,name','operator:id,name,prefix'])->get();
        $col_heads = array(
            trans('messages.Country'),
            trans('messages.Operator'),
            trans('messages.SMS Gateway'),
            trans('messages.Prefix'),
            trans('messages.Masking Price'),
            trans('messages.Non Masking Price'),
            trans('messages.Sell Masking Price'),
            trans('messages.Sell Non Masking Price'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($allDefaultPricing as $value) {
            $form_url = route('default-pricing.destroy', $value->id);
            $edit_url = route('default-pricing.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->country->name,
                $value->operator->name,
                $value->type ? "Masking" : "Non Masking",
                $value->operator->prefix,
                $value->masking_price,
                $value->non_masking_price,
                $value->sell_msk_price,
                $value->sell_non_msk_price,
                Helper::status_change($status_change_url, 'default_pricings', $value->id, 'status', $value->status, 'Active', 'Pending', 'Default Pricing Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('default_pricing.index', compact('col_heads', 'col_data', 'operators', 'countries', 'default_pricing', 'smsGateways', 'smsGatewaysMasking'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Default_pricing  $default_pricing
     * @return \Illuminate\Http\Response
     */
    public function update(DefaultPricingRequest $request, Default_pricing $default_pricing)
    {
        $data = $request->all();
        $data['user_id'] = Auth::id();

        $default_pricing->fill($data);
        $default_pricing->save();

        $message = "Edit Default Pricing";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Default_pricing  $default_pricing
     * @return \Illuminate\Http\Response
     */
    public function destroy(Default_pricing $default_pricing)
    {
        $default_pricing->delete();

        $message = "Default Pricing Delete";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
