<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Http\Requests\TransactionRequest;
use App\Log;
use App\PaymentMethod;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = User::where('username', $request->name)->first();
        $transactions = Transaction::where('user_id', $user->id)->get();
        $transaction = new Transaction;
        $paymentMethod = PaymentMethod::where('status', 1)->get();

        $col_data=array();
        $col_heads = array(
            trans('messages.State'),
            trans('messages.Payment Method'),
            trans('messages.Description'),
            trans('messages.Amount'),
            trans('messages.Created At'),
            trans('messages.Option')
        );

        foreach ($transactions as $value) {
            $form_url = route('transaction.destroy', $value->id);
            $edit_url = route('transaction.edit', $value->id);
            $col_data[] = array(
                $value->state ? "Credit" : "Debit",
                $value->payment_method,
                $value->description,
                $value->state ? "<span class=\"label label-primary\">$value->amount</span>" : "<span class=\"label label-danger\">$value->amount</span>",
                $value->created_at,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('transaction.index', compact('transaction', 'col_heads', 'col_data', 'user', 'paymentMethod'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TransactionRequest $request)
    {
        $data = $request->all();
        Transaction::create($data);

        $message = "Add Transaction";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $transaction = Transaction::find($id);
        $transactions = Transaction::where('user_id', $transaction->user_id)->get();
        $user = User::where('id', $transaction->user_id)->first();
        $paymentMethod = PaymentMethod::where('status', 1)->get();

        $col_data=array();
        $col_heads = array(
            trans('messages.State'),
            trans('messages.Payment Method'),
            trans('messages.Description'),
            trans('messages.Amount'),
            trans('messages.Created At'),
            trans('messages.Option')
        );

        foreach ($transactions as $value) {
            $form_url = route('transaction.destroy', $value->id);
            $edit_url = route('transaction.edit', $value->id);
            $col_data[] = array(
                $value->state ? "Credit" : "Debit",
                $value->payment_method,
                $value->description,
                $value->state ? "<span class=\"label label-primary\">$value->amount</span>" : "<span class=\"label label-danger\">$value->amount</span>",
                $value->created_at,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('transaction.index', compact('transaction', 'col_heads', 'col_data', 'user', 'paymentMethod'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        $transaction = Transaction::find($id);
        $transaction->fill($data);
        $transaction->save();

        $message = "Edit Transaction";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $transaction = Transaction::find($id);
        $transaction->delete();

        $message = "Delete Transaction";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
