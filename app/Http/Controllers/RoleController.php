<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Permission;
use App\Role;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $permissions = Permission::all();
        $permissions_test = DB::table('permissions')->orderBy('category')->get();
        $permission_role = DB::table('permission_role')
            ->select(DB::raw('CONCAT(role_id,"-",permission_id) AS detail'))
            ->pluck('detail')->toArray();

        $roles = Role::all();


        return view('setting.role.index', compact('permissions', 'roles', 'permissions_test', 'permission_role'));

    }

    public function savePermission(Request $request)
    {
        $input = $request->all();
        $permissions = array_get($input, 'permission');

        DB::table('permission_role')->truncate();

        if($permissions != '')
            foreach($permissions as $r_key => $permission){
                foreach($permission as $p_key => $per){
                    $values[] = $p_key;
                }

                $role = Role::find($r_key);

                if(count($values))
                    $role->permissions()->attach($values);
                unset($values);
            }

        $message = "Role and Permission Save";
        Session::flash('success', $message);
        return back();
    }
}
