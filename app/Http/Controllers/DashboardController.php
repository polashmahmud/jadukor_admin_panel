<?php

namespace App\Http\Controllers;

use App\Masking;
use App\Sms;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class DashboardController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $allAdmin = User::where([['status', 1], ['role', 1]])->count();
        $allMasterReseller = User::where([['status', 1], ['role', 2]])->count();
        $allReseller = User::where([['status', 1], ['role', 3]])->count();
        $allUser = User::where([['status', 1], ['role', 4]])->count();

        if (Auth::user()->role > 1) {
            $maskingPending = Masking::with('user')->where([['status', 0], ['member_id', Auth::id()]])->get();
        } else {
            $maskingPending = Masking::with('user')->where('status', 0)->get();
        }

        // Chart
        $base =  ['January' => 0, 'February' => 0, 'March' => 0, 'April' => 0, 'May' => 0, 'June' => 0, 'July' => 0, 'August' => 0, 'September' => 0, 'October' => 0, 'November' => 0, 'December' => 0];

        $maskingChart = Sms::chartData(1);
        $non_maskingChart = Sms::chartData(0);

        $maskingData = $maskingChart->mapWithKeys(function ($item) {
            return [$item['month'] => $item['total']];
        });
        $allMaskingData = $maskingData->toArray();
        $nonMaskingData = $non_maskingChart->mapWithKeys(function ($item) {
            return [$item['month'] => $item['total']];
        });
        $allNonMaskingData = $nonMaskingData->toArray();

        $masking_chart_data = array_merge($base, $allMaskingData);
        $masking_chart_value = json_encode(array_values($masking_chart_data));

        $non_masking_chart_data = array_merge($base, $allNonMaskingData);
        $non_masking_chart_value = json_encode(array_values($non_masking_chart_data));


        return view('dashboard', compact('allAdmin', 'allMasterReseller', 'allReseller', 'allUser', 'maskingPending', 'masking_chart_value', 'non_masking_chart_value'));
    }
}
