<?php

namespace App\Http\Controllers;

use App\Phone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;

class FileInputContact extends Controller
{
    public function store(Request $request)
    {
        if($request->hasFile('file')){
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv") {

                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                    $reader->select(['name', 'email', 'number']);
                })->get();

                if(!empty($data) && $data->count()){

                    foreach ($data as $key => $value) {
                        if ($value->count() == 3) {
                            $phone = new Phone;
                            $phone->user_id = Auth::id();
                            $phone->name = $value->name;
                            $phone->email = $value->email;
                            $phone->number = $value->number;
                            $phone->save();

                            $phone->groups()->attach($request->group_id);
                        } else {
                            Session::flash('error','Somthing is wrong in your file');
                            return back();
                        }
                    }
                }
                Session::flash('success','Your Data has successfully imported');
                return back();

            }else {
                Session::flash('error','File is a '.$extension.' file.!! Please upload a valid xls/csv file..!!');
                return back();
            }
        }
    }
}
