<?php

namespace App\Http\Controllers;

use App\BlockedContact;
use App\Classes\Helper;
use App\Default_pricing;
use App\Group;
use App\Number;
use App\Operator;
use App\PreviewSms;
use App\Pricing_coverages;
use App\SendMessage;
use App\SmsCost;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Session;
use Maatwebsite\Excel\Facades\Excel;
use App\Sms;
use App\Template;


class SendMessageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $templates = Template::where('user_id', Auth::id())->get();
        return view('send_message.index', compact('templates'));
    }

    public function group()
    {
        $phoneGroups = Group::where([['user_id', Auth::id()],['status', 1]])->get();
        $templates = Template::where('user_id', Auth::id())->get();

        return view('send_message.group', compact('phoneGroups', 'templates'));
    }

    public function file()
    {
        $templates = Template::where('user_id', Auth::id())->get();
        return view('send_message.file', compact('templates'));
    }

    public function store(Request $request)
    {
        if (isset($request->template)) {
            $smsContent = $request->template;
        } else {
            $smsContent = $request->message;
        }

        if (isset($request->group_id)){
            $group_id = Group::find($request->group_id);
            $Numbers = $group_id->phones->pluck('number');
            $sendType = 1;
        } elseif (isset($request->msisdn)) {
            if (strpos($request->msisdn, ',') !== false) {
                $Numbers = explode(',',$request->input('msisdn'));
            } else {
                $Numbers = explode("\n",$request->input('msisdn'));
            }
            $sendType = 0;
        } elseif ($request->hasFile('file')) {
            $extension = File::extension($request->file->getClientOriginalName());
            if ($extension == "xlsx" || $extension == "xls" || $extension == "csv" || $extension == "txt") {
                $path = $request->file->getRealPath();
                $data = Excel::load($path, function($reader) {
                    $reader->select(['number']);
                })->get();
            }
            $Numbers = $data->pluck('number');
            $sendType = 2;
        }

        $allNumbers=[];
        foreach ($Numbers as $number) {
            $fullNumber = '880' . substr(preg_replace('/\D/', '', $number), -10);
            if (strlen(trim($fullNumber)) == 13) {
                $allNumbers[] = trim($fullNumber);
            }
        }

        // Blocked Contact
        $blockContact = BlockedContact::get();
        $blockContactAll = json_decode(json_encode($blockContact->pluck('number')));

        // remove blocker contact
        $phoneNumbers = array_diff($allNumbers,$blockContactAll);

        $data = [];
        foreach ($phoneNumbers as $value) {
            $data[] = substr($value,0,5);
        }

        $smsType = $request->recipientsmsRadios;

        // Special characters
        if (preg_match('/[\'\^{}\\\\\\[~\\]|€]/', $smsContent))
        {
            $specila_text = "yes";
        } else {
            $specila_text = "no";
        }

        // SMS Text Count
        if (strlen($smsContent) != strlen(utf8_decode($smsContent)))
        {
            $smsTextCount = mb_strlen($smsContent, 'UTF-8');
        } else {
            $smsTextCount = strlen($smsContent);
        }

        $count = \App\Classes\Helper::SmsCount($smsType, $smsTextCount, $specila_text);

        if ($count == 0) {
            Session::flash('error', 'Message text is too long');
            return back();
        }

        // User Pricing
        $user_pricing = Pricing_coverages::where('user_id', Auth::id())->get();
        if (empty($user_pricing)) {
            $default_pricing = Pricing_coverages::where('user_id', Auth::id())->get(['id', 'operator_id']);
            $default_pricing_prefix_id = $default_pricing->pluck('operator_id');
        } else {
            $default_pricing = Default_pricing::get(['id', 'operator_id']);
            $default_pricing_prefix_id = $default_pricing->pluck('operator_id');
        }

        // Prefix
        $prefix = Operator::whereIn('id',$default_pricing_prefix_id)->get();
        $prefixList = json_decode(json_encode($prefix->pluck('prefix')));

        // Count how many sms each prefix value
        $overlap = array_intersect($data, $prefixList);
        $counts  = array_count_values($overlap);

        $previewSms = new Sms;
        $previewSms->user_id = Auth::id();
        $previewSms->sender = $request->sender_id;
        $previewSms->contents = $smsContent;
        $previewSms->send_type = $sendType;
        if (isset($request->group_id)) {
            $previewSms->group_id = $request->group_id;
        }
        $previewSms->sms_type = $smsType=='unicode' ? 1 : 0;
        $previewSms->masking = $request->sender_id == 'non_masking' ? 0 : 1;
        $previewSms->schedule = $request->schedul_sms;
        $previewSms->schedule_time = $request->time;
        $previewSms->status = 0;
        $previewSms->save();


        foreach ($counts as $key=>$value) {
            $smsCost = new SmsCost;
            $smsCost->preview_sms_id = $previewSms->id;
            $smsCost->operator_id = Helper::operatorId($key);
            $smsCost->recipient = $value;
            $smsCost->sms = $count;
            $smsCost->rate = Auth::user()->price_type == 0 ? Helper::smsPrice($key, $request->sender_id) : 1;
            $smsCost->cost = Auth::user()->price_type == 0 ? ($value * $count) * Helper::smsPrice($key, $request->sender_id) : $value * $count;
            $smsCost->save();
        }

        $numbers = [];
        foreach ($phoneNumbers as $number) {
            $nPrefix = substr($number, 0, 5);
            $numbers[] = array(
                'preview_sms_id' => $previewSms->id,
                'content' => $smsContent,
                'number' => $number,
                'sender' => $request->sender_id,
                'sms' => $count,
                'rate' => Auth::user()->price_type == 0 ? Helper::smsPrice($nPrefix, $request->sender_id) : 1,
                'cost' => Auth::user()->price_type == 0 ? $count * Helper::smsPrice($nPrefix, $request->sender_id) : $count,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );
        }

        Number::insert($numbers);

        return redirect()->route('preview-sms.show', $previewSms->id);

    }


}
