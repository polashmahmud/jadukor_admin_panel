<?php

namespace App\Http\Controllers;

use App\Log;
use App\Masking;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class AddMasking extends Controller
{
    public function index($name)
    {
        $user = User::where('username', $name)->first();
        $approvedMasking = DB::table('masking_user')->where('user_id', $user->id)->pluck('masking_id')->toArray();
        $maskings = Masking::where('status', 1)->get();
        return view('masking.add', compact('user', 'maskings', 'approvedMasking'));
    }

    public function store(Request $request)
    {
        $user = User::find($request->user_id);
        $user->maskings()->sync($request->masking_id);

        $message = "Apply Masking To User";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
