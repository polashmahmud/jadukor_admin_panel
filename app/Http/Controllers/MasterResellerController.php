<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Classes\Helper;
use App\Pricing_coverages;
use App\User;
use Illuminate\Http\Request;

class MasterResellerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $masterreseller = User::where('role', 2)->get();

        return view('masterreseller.index', compact('masterreseller'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($name)
    {
        $user = User::where('username', $name)->first();
        $pricing = Pricing_coverages::with(['country:id,iso', 'operator:id,name,prefix', 'masking_gateway:id,user_display_name', 'non_masking_gateway:id,user_display_name'])->where('user_id', $user->id)->get();
        $balance = Balance::where([['user_id', $user->id], ['state', 1]])->latest()->take(25)->get();

        return view('masterreseller.show', compact('user', 'balance', 'pricing'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $mastereseller = User::find($id);
        return view('masterreseller.create', compact('mastereseller'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
