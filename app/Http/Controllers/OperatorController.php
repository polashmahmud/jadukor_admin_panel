<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Country;
use App\Http\Requests\OperatorRequest;
use App\Log;
use App\Operator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OperatorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $operators = Operator::with('country:id,name')->get();
        $countries = Country::all();
        $col_data=array();
        $operator = new Operator;
        $col_heads = array(
            trans('messages.Operator Name'),
            trans('messages.Country Name'),
            trans('messages.Prefix'),
            trans('messages.MNC'),
            trans('messages.TADIG'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($operators as $value) {
            $form_url = route('operators.destroy', $value->id);
            $edit_url = route('operators.edit', $value->id);
            $status_change_url = route('status-change', $value->id);
            $col_data[] = array(
                $value->name,
                $value->country->name,
                $value->prefix,
                $value->mnc,
                $value->tadig,
                Helper::status_change($status_change_url, 'operators', $value->id, 'status', $value->status, 'Active', 'Pending', 'Status Change'),
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.operators.index', compact('col_heads', 'col_data', 'operator', 'countries'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OperatorRequest $request)
    {
        $alreadyExist = Operator::where([['country_id', $request->country_id], ['prefix',$request->prefix]])->get();

        if (count($alreadyExist)) {
            Session::flash('warning', 'Operator Already Exist');
            return back();
        }

        Operator::create($request->all());
        $message = "Add New Operator";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function show(Operator $operator)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function edit(Operator $operator)
    {
        $operators = Operator::with('country:id,name')->get();
        $countries = Country::all();
        $col_data=array();
        $col_heads = array(
            trans('messages.Operator Name'),
            trans('messages.Country Name'),
            trans('messages.Prefix'),
            trans('messages.MNC'),
            trans('messages.TADIG'),
            trans('messages.Status'),
            trans('messages.Option')
        );

        foreach ($operators as $value) {
            $form_url = route('operators.destroy', $value->id);
            $edit_url = route('operators.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->country->name,
                $value->prefix,
                $value->mnc,
                $value->tadig,
                $value->status ? "Active" : "Deactivate",
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.operators.index', compact('col_heads', 'col_data', 'operator', 'countries'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function update(OperatorRequest $request, Operator $operator)
    {
        $operator->country_id = $request->country_id;
        $operator->name = $request->name;
        $operator->prefix = $request->prefix;
        $operator->mnc = $request->mnc;
        $operator->tadig = $request->tadig;
        $operator->status = $request->status;
        $operator->save();
        $message = "Edit Operator";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Operator  $operator
     * @return \Illuminate\Http\Response
     */
    public function destroy(Operator $operator)
    {
        $operator->delete();
        $message = "Operator Delete";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
