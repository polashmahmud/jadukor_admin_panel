<?php

namespace App\Http\Controllers;

use App\BlockedContact;
use App\Classes\Helper;
use App\Http\Requests\BlockedContactRequest;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BlockedContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blockedContacts = BlockedContact::all();
        $blockedContact = new BlockedContact;

        $col_data=array();
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Number'),
            trans('messages.Option')
        );

        foreach ($blockedContacts as $value) {
            $form_url = route('blocked-contacts.destroy', $value->id);
            $edit_url = route('blocked-contacts.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->number,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('blocked_contacts.index', compact('col_heads', 'col_data', 'blockedContact'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(BlockedContactRequest $request)
    {
        BlockedContact::create($request->all());

        $message = "Add Block Contact";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\BlockedContact  $blockedContact
     * @return \Illuminate\Http\Response
     */
    public function show(BlockedContact $blockedContact)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\BlockedContact  $blockedContact
     * @return \Illuminate\Http\Response
     */
    public function edit(BlockedContact $blockedContact)
    {
        $blockedContacts = BlockedContact::all();

        $col_data=array();
        $col_heads = array(
            trans('messages.Name'),
            trans('messages.Number'),
            trans('messages.Option')
        );

        foreach ($blockedContacts as $value) {
            $form_url = route('blocked-contacts.destroy', $value->id);
            $edit_url = route('blocked-contacts.edit', $value->id);
            $col_data[] = array(
                $value->name,
                $value->number,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('blocked_contacts.index', compact('col_heads', 'col_data', 'blockedContact'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\BlockedContact  $blockedContact
     * @return \Illuminate\Http\Response
     */
    public function update(BlockedContactRequest $request, BlockedContact $blockedContact)
    {
        $blockedContact->fill($request->all());
        $blockedContact->save();

        $message = "Edit Block Contact";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\BlockedContact  $blockedContact
     * @return \Illuminate\Http\Response
     */
    public function destroy(BlockedContact $blockedContact)
    {
        $blockedContact->delete();

        $message = "Delete Block Contact";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
