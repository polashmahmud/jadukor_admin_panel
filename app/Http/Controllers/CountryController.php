<?php

namespace App\Http\Controllers;

use App\Classes\Helper;
use App\Country;
use App\Http\Requests\CountryRequest;
use App\Log;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::all();
        $col_data=array();
        $country = new Country;
        $col_heads = array(
            trans('messages.iso'),
            trans('messages.Name'),
            trans('messages.Nick Name'),
            trans('messages.iso3'),
            trans('messages.Num Code'),
            trans('messages.Phone Code'),
            trans('messages.Option')
        );

        foreach ($countries as $value) {
            $form_url = route('countries.destroy', $value->id);
            $edit_url = route('countries.edit', $value->id);
            $col_data[] = array(
                $value->iso,
                $value->name,
                $value->nickname,
                $value->iso3,
                $value->numcode,
                $value->phonecode,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.country.index', compact('col_heads', 'col_data', 'country'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CountryRequest $request)
    {
        Country::create($request->all());
        $message = "Add New Country";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function show(Country $country)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function edit(Country $country)
    {
        $countries = Country::all();
        $col_data=array();
        $col_heads = array(
            trans('messages.iso'),
            trans('messages.Name'),
            trans('messages.Nick Name'),
            trans('messages.iso3'),
            trans('messages.Num Code'),
            trans('messages.Phone Code'),
            trans('messages.Option')
        );

        foreach ($countries as $value) {
            $form_url = route('countries.destroy', $value->id);
            $edit_url = route('countries.edit', $value->id);
            $col_data[] = array(
                $value->iso,
                $value->name,
                $value->nickname,
                $value->iso3,
                $value->numcode,
                $value->phonecode,
                "<a href=\"$edit_url\"><i class=\"ti-pencil-alt color-success\"></i></a> " .
                Helper::delete_form($form_url, $value->id)
            );
        }

        return view('setting.country.index', compact('col_heads', 'col_data', 'country'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function update(CountryRequest $request, Country $country)
    {
        $country->iso = $request->iso;
        $country->name = $request->name;
        $country->nickname = $request->nickname;
        $country->iso3 = $request->iso3;
        $country->numcode = $request->numcode;
        $country->phonecode = $request->phonecode;
        $country->save();

        $message = "Edit Country";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function destroy(Country $country)
    {
        $country->delete();
        $message = "Country Delete";
        Session::flash('success', $message);
        Log::create(['user_id' => Auth::id(), 'description' => $message]);
        return back();
    }
}
