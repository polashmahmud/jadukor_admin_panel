<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PricingCoverageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country_id' => 'required',
            'operator_id' => 'required',
            'sms_gateway_masking_id' => 'required',
            'sms_gateway_non_masking_id' => 'required',
            'masking_price' => 'required',
            'non_masking_price' => 'required',
            'sell_msk_price' => 'required',
            'sell_non_msk_price' => 'required',
            'status' => 'required',
        ];
    }
}
