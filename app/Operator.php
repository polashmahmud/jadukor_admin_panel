<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operator extends Model
{
    protected $fillable=['country_id', 'name', 'prefix', 'mnc', 'tadig', 'status'];

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id');
    }
}
