<?php

namespace App\Classes;

use App\Balance;
use App\Default_pricing;
use App\Operator;
use App\Pricing_coverages;
use App\Smsgateway;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class Helper {

    public static function delete_form($form_url, $id)
    {
        $token = @csrf_token();
        $form = "<form id=\"delete-form-$id\" method=\"post\" action=\"$form_url\" style=\"display: none\">
                    <input type=\"hidden\" name=\"_token\" value=\"$token\">
                    <input type=\"hidden\" name=\"_method\" value=\"DELETE\">
                  </form>
                  <span><a href=\"\" onclick=\"
                  if(confirm('Are you sure, You Want to delete this?'))
                      {
                        event.preventDefault();
                        document.getElementById('delete-form-$id').submit();
                      }
                      else{
                        event.preventDefault();
                      }\" ><i class=\"ti-trash color-danger\"></i></a>";

        return $form;
    }

    public static function status_change($url, $table, $id, $column, $value, $btn_text_one, $btn_text_two, $message)
    {
        $btn_class = $value ? 'btn btn-info btn-xs' : 'btn btn-warning btn-xs';
        $btn_text = $value ? $btn_text_one : $btn_text_two;
        $token = @csrf_token();
        $form = "<form id=\"status-change-$id\" method=\"post\" action=\"$url\" style=\"display: none\">
                    <input type=\"hidden\" name=\"_token\" value=\"$token\">
                    <input type=\"hidden\" name=\"table\" value=\"$table\">
                    <input type=\"hidden\" name=\"id\" value=\"$id\">
                    <input type=\"hidden\" name=\"column\" value=\"$column\">
                    <input type=\"hidden\" name=\"value\" value=\"$value\">
                    <input type=\"hidden\" name=\"message\" value=\"$message\">
                  </form>
                  <span><a class='$btn_class' href=\"\" onclick=\"
                  if(confirm('Are you sure, You Want to Change this?'))
                      {
                        event.preventDefault();
                        document.getElementById('status-change-$id').submit();
                      }
                      else{
                        event.preventDefault();
                      }\" >$btn_text</a>";

        return $form;

    }

    public static function fullName($firstName=null,$lastName=null)
    {
        return $firstName .' '. $lastName;
    }

    public static function transactionBalance($user_id)
    {
        $amount = Transaction::where('user_id', $user_id)
            ->sum('amount');
        return $amount;
    }

    public static function sendSoFar($user_id)
    {
        $amount = Transaction::Where([['user_id', $user_id], ['payment_method', 'Send SMS']])
            ->sum('amount');
        return abs($amount);
    }

    public static function Balance($user_id)
    {
        $amount = Balance::where('user_id', $user_id)
            ->sum('amount');
        return $amount;
    }

    public static function BalanceMaskingSMS($user_id)
    {
        $sms = Balance::where('user_id', $user_id)
            ->sum('masking_sms');
        return $sms;
    }

    public static function BalanceNonMaskingSMS($user_id)
    {
        $sms = Balance::where('user_id', $user_id)
            ->sum('non_masking_sms');
        return $sms;
    }

    public static function SmsCount($smsType, $smsTextCount, $specila_text)
    {
        if ($smsType == 'text')
        {
            if ($specila_text == "yes") {
                if ($smsTextCount <= 140) {
                    return 1;
                } elseif ($smsTextCount <= 280) {
                    return 2;
                } elseif ($smsTextCount <= 420) {
                    return 3;
                } elseif ($smsTextCount <= 560) {
                    return 4;
                } elseif ($smsTextCount <= 700) {
                    return 5;
                } else {
                    return 0;
                }
            } else {
                if ($smsTextCount <= 160) {
                    return 1;
                } elseif ($smsTextCount <= 306) {
                    return 2;
                } elseif ($smsTextCount <= 459) {
                    return 3;
                } elseif ($smsTextCount <= 612) {
                    return 4;
                } elseif ($smsTextCount <= 772) {
                    return 5;
                } else {
                    return 0;
                }
            }
        }

        if($smsType == 'unicode') {
            if ($smsTextCount <= 70) {
                return 1;
            } elseif ($smsTextCount <= 134) {
                return 2;
            } elseif ($smsTextCount <= 201) {
                return 3;
            } elseif ($smsTextCount <= 268) {
                return 4;
            } elseif ($smsTextCount <= 338) {
                return 5;
            } else {
                return 0;
            }
        }
    }

    public static function smsPrice($prefix, $sender)
    {
        $operator = Operator::where('prefix', $prefix)->first();

        if (! isset($operator)) {
            return 0;
        }

        $user_pricing = Pricing_coverages::where('user_id', Auth::id())->get();
        if (empty($user_pricing)) {
            $pricing = Pricing_coverages::where([['user_id', Auth::id()], ['operator_id', $operator->id]])->first();

            return $sender=='non_masking' ? $pricing->non_masking_price : $pricing->masking_price;
        } else {
            $pricing = Default_pricing::where([['user_id', Auth::id()], ['operator_id', $operator->id]])->first();
            return $sender=='non_masking' ? $pricing->non_masking_price : $pricing->masking_price;
        }
    }

    public static function operatorId($prefix)
    {
        $operator = Operator::where('prefix', $prefix)->first();
        return $operator->id;
    }

    public static function routeName($id)
    {
        $routeName = Smsgateway::find($id);

        return $routeName->display_name;
    }

    public static function sendUrl($prefix, $senderID)
    {
        $operatorID = Operator::where('prefix', $prefix)->first();
        $url = Pricing_coverages::where([['user_id', Auth::id()], ['operator_id', $operatorID->id]])->first();
        if ($senderID=="non_masking") {
            $nonMasking = Smsgateway::find($url->sms_gateway_non_masking_id);
            return $nonMasking->url;
        } else {
            $masking = Smsgateway::find($url->sms_gateway_masking_id);
            return $masking->url;
        }
    }

    public static function sendID($prefix, $senderID)
    {
        $operatorID = Operator::where('prefix', $prefix)->first();
        $url = Pricing_coverages::where([['user_id', Auth::id()], ['operator_id', $operatorID->id]])->first();
        if ($senderID == "non_masking") {
            $nonMasking = $url->sms_gateway_non_masking_id;
            return $nonMasking;
        } else {
            $masking = $url->sms_gateway_masking_id;
            return $masking;
        }
    }

    public static function operatorName($number)
    {
        $prefix = substr($number, 0, 5);

        switch ($prefix) {
            case '88013':
                return "Grameenphone";
                break;

            case '88017':
                return "Grameenphone";
                break;

            case '88015':
                return "Teletalk";
                break;

            case '88016':
                return "Airtal";
                break;

            case '88018':
                return "Robi";
                break;

            case '88019':
                return "Banglink";
                break;

            default:
                return "Unknown";
                break;
        }
    }
}
