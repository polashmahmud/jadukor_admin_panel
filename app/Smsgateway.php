<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Smsgateway extends Model
{
    protected $fillable = ['user_id', 'name', 'display_name', 'user_display_name', 'url', 'type', 'status'];
}
