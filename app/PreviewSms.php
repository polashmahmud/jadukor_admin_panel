<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PreviewSms extends Model
{
    protected $fillable = ['user_id', 'sender', 'contents', 'send_type', 'group_id', 'sms_type', 'masking', 'sms', 'send_route', 'schedule', 'schedule_time', 'status'];

    public function smscost()
    {
        return $this->hasMany('App\SmsCost', 'preview_sms_id', 'id');
    }

    public function numbers()
    {
        return $this->hasMany('App\Number', 'preview_sms_id', 'id');
    }
}
