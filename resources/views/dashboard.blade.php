@extends('layouts.app')

@section('title', 'Dashboard')

@section('breadcrumb')

@endsection

@push('header-scripts')

@endpush

@push('footer-scripts')
    <script src={{ asset("assets/js/lib/chart-js/Chart.min.js") }}></script>
    <script src={{ asset("assets/js/lib/circle-progress/circle-progress.min.js") }}></script>
    <script src={{ asset("assets/js/lib/circle-progress/circle-progress-init.js") }}></script>
    <script src={{ asset("assets/js/lib/sparklinechart/jquery.sparkline.min.js") }}></script>
    <script src={{ asset("assets/js/lib/sparklinechart/sparkline.init.js") }}></script>
    <script>


        new Chart(document.getElementById("bar-chart-grouped"), {
            type: 'bar',
            data: {
                labels: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'],
                datasets: [
                    {
                        label: "Masking",
                        backgroundColor: "#3e95cd",
                        data: {!! $masking_chart_value !!}
                    }, {
                        label: "Non Masking",
                        backgroundColor: "#8e5ea2",
                        data: {!! $non_masking_chart_value !!}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Population growth (millions)'
                }
            }
        });


        new Chart(document.getElementById("pie-chart"), {
            type: 'pie',
            data: {
                labels: ["Delivered", "Pending", "Rejected", "Failed"],
                datasets: [{
                    label: "SMS (millions)",
                    backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9"],
                    data: [2478,5267,734,784]
                }]
            },
            options: {
                title: {
                    display: true,
                    text: 'SMS Status in 6 months'
                }
            }
        });


    </script>
@endpush


@section('content')
    @if(in_array(2, $user_permissions))
    <div class="row">
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Admin</div>
                        <div class="stat-digit">{{ $allAdmin }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Master Reseller</div>
                        <div class="stat-digit">{{ $allMasterReseller }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">Reseller</div>
                        <div class="stat-digit">{{ $allReseller }}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-3">
            <div class="card">
                <div class="stat-widget-one">
                    <div class="stat-icon dib"><i class="ti-user color-primary border-primary"></i></div>
                    <div class="stat-content dib">
                        <div class="stat-text">User</div>
                        <div class="stat-digit">{{ $allUser }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(in_array(3, $user_permissions))
    <div class="row">
        <div class="col-lg-12">
            <div class="card alert">
                <div class="card-header">
                    <h4>Pending Masking </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>User Name</th>
                                <th>Masking</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($maskingPending as $value)
                            <tr>
                                <th scope="row">{{ $loop->index + 1 }}</th>
                                <td>{{ $value->created_at }}</td>
                                <td>{{ $value->user->first_name }} {{ $value->user->last_name }}</td>
                                <td>{{ $value->display_name }}</td>
                                <td class="color-danger">Pending</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if(in_array(4, $user_permissions))
    <div class="row">
        <div class="col-lg-6">
            <div class="card alert">
                <canvas id="bar-chart-grouped" width="800" height="450"></canvas>
            </div>
        </div>
        <div class="col-lg-6">
            <div class="card alert">
                <canvas id="pie-chart" width="800" height="450"></canvas>
            </div>
        </div>
    </div>
    @endif
@endsection