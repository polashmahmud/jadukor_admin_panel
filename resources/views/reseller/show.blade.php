@extends('layouts.app')

@section('title', 'Details')

@section('breadcrumb')
    <li class="active">Details</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-3">
            <div class="card alert">
                <div class="card-header">
                    <h4>{{ $user->first_name }} {{ $user->last_name }}</h4>
                </div>
                <div class="card-body">
                    <div class="user-profile m-t-15">
                        <div class="user-photo m-b-30">
                            @if( ! \Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar', 'small'))
                                <img class="img-responsive" src="{{ asset("assets/images/user-profile.jpg") }}" alt="" />
                            @else
                                <img class="img-responsive" src="{{ asset(\Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar', 'medium')) }}" alt=""/>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="ti-money f-s-48 color-dark"></i></span>
                    </div>
                    <div class="media-body media-text-right">
                        <h4>{{ \App\Classes\Helper::transactionBalance($user->id) }} TK</h4>
                        <h6>Total Amount</h6>
                    </div>
                </div>
            </div>
            @if($user->price_type==1)
                <div class="card">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="ti-email f-s-48 color-dark"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h4>{{ \App\Classes\Helper::BalanceMaskingSMS($user->id) }}</h4>
                            <h6>Total Balance Masking SMS</h6>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="ti-email f-s-48 color-dark"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h4>{{ \App\Classes\Helper::BalanceNonMaskingSMS($user->id) }}</h4>
                            <h6>Total Balance Non-Masking SMS</h6>
                        </div>
                    </div>
                </div>
            @else
                <div class="card">
                    <div class="media">
                        <div class="media-left meida media-middle">
                            <span><i class="ti-email f-s-48 color-dark"></i></span>
                        </div>
                        <div class="media-body media-text-right">
                            <h4>{{ \App\Classes\Helper::balance($user->id) }}</h4>
                            <h6>SMS Balance</h6>
                        </div>
                    </div>
                </div>
            @endif
            @if(in_array(66, $user_permissions))
                <form id="switch_user" method="post" action="{{ route('switch-user') }}" style="display: none;">
                    @csrf
                    <input type="hidden" value="{{ $user->id }}" name="user_id">
                </form>
                <a href="" class="btn btn-default btn-block m-b-10" onclick="
                     if(confirm('Are you sure?'))
                      {
                        event.preventDefault();
                        document.getElementById('switch_user').submit();
                      }
                      else{
                        event.preventDefault();
                      }" >Switch Account</a>
            @endif
            <div class="card alert">
                <div class="card-header">
                    <h4>Approved Masking</h4>
                    @if(\Illuminate\Support\Facades\Auth::user()->role==1)
                        <a style="float: right;" class="btn btn-link btn-flat m-b-10 m-l-5" href="{{ route('add-masking', $user->username) }}">Add</a>
                    @endif
                </div>
                <div class="card-body">
                    @foreach($user->maskings as $masking)
                        <span class="label label-primary">{{ $masking->name }}</span>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            @if($user->price_type==1)
                <div class="card alert">
                    <div class="card-header">
                        <h4>Average SMS Price </h4>
                    </div>
                    <div class="card-body">
                        Masking: {{ $user->average_masking_price }} TK | Non-Masking: {{ $user->average_non_masking_price }}
                    </div>
                </div>
            @endif
            <div class="card alert">
                <div class="card-header">
                    <h4>SMS Price </h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Prefix</th>
                                <th>Gateway Masking</th>
                                <th>Gateway Non-Masking</th>
                                <th>Masking Price</th>
                                <th>Non Masking Price</th>
                                <th>Sell Masking Price</th>
                                <th>Sell Non Masking Price</th>
                                @if(in_array(63, $user_permissions))
                                    <th>Status</th>
                                @endif
                                <th style="width:120px;">Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($pricing as $value)
                                <tr>
                                    <td>{{ $value->operator->name .'-'.$value->operator->prefix }}</td>
                                    <td>{{ $value->masking_gateway->user_display_name }}</td>
                                    <td>{{ $value->non_masking_gateway->user_display_name }}</td>
                                    <td>{{ $value->masking_price }}</td>
                                    <td>{{ $value->non_masking_price }}</td>
                                    <td>{{ $value->sell_msk_price }}</td>
                                    <td>{{ $value->sell_non_msk_price }}</td>
                                    @if(in_array(63, $user_permissions))
                                        <td>
                                            <form id="status-change-{{ $value->id }}" method="post" action="{{ route('status-change', $value->id) }}" style="display: none;">
                                                @csrf
                                                <input type="hidden" name="table" value="pricing_coverages">
                                                <input type="hidden" name="id" value="{{ $value->id }}">
                                                <input type="hidden" name="column" value="status">
                                                <input type="hidden" name="value" value="{{ $value->status }}">
                                                <input type="hidden" name="message" value="Pricing Status Change">
                                            </form>
                                            <a class='{{ $value->status ? 'btn btn-info btn-xs' : 'btn btn-warning btn-xs' }}' href="" onclick="
                                                    if(confirm('Are you sure, You Want to Change this?'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('status-change-{{ $value->id }}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }" >{{ $value->status ? 'Active' : 'Pending' }}</a>
                                        </td>
                                    @endif
                                    <td>

                                        @if(in_array(64, $user_permissions))
                                            <a href="{{ route('pricing-coverages.edit', $value->id) }}"><i class="ti-pencil-alt color-success"></i></a>
                                        @endif
                                        @if(in_array(65, $user_permissions))
                                            <form id="delete-form-{{ $value->id }}" method="post" action="{{ route('pricing-coverages.destroy', $value->id) }}" style="display: none">
                                                @csrf
                                                @method('Delete')
                                            </form>
                                            <a href="" onclick="
                                                    if(confirm('Are you sure, You Want to delete this?'))
                                                    {
                                                    event.preventDefault();
                                                    document.getElementById('delete-form-{{ $value->id }}').submit();
                                                    }
                                                    else{
                                                    event.preventDefault();
                                                    }" ><i class="ti-trash color-danger"></i>
                                            </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            @if($user->price_type==1)
                <div class="card alert">
                    <div class="card-body">
                        <div class="card-header m-b-20">
                            <h4>Buy SMS</h4>
                        </div>
                    </div>
                    <div class="row">
                        <form action="{{ route('balance.store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <div class="col-md-3">
                                <div class="row" style="margin-top: 40px;">
                                    <div class="col-lg-6">
                                        <input type="radio" value="1" name="type"> Masking
                                    </div>
                                    <div class="col-lg-6">
                                        <input type="radio" value="0" name="type" checked> Non-Masking
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="basic-form">
                                    <div class="form-group">
                                        <label>SMS</label>
                                        <input type="number" class="form-control border-none input-flat bg-ash" placeholder="SMS" name="sms" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="basic-form">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input type="text" class="form-control border-none input-flat bg-ash" placeholder="Description" name="description" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" style="margin-top: 27px;">
                                <div class="basic-form">
                                    <div class="form-group">
                                        <label></label>
                                        <button class="btn btn-default btn-lg" type="submit">Buy SMS</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @else
                <div class="card alert">
                    <div class="card-body">
                        <div class="card-header m-b-20">
                            <h4>Add SMS Amount</h4>
                        </div>
                    </div>
                    <div class="row">
                        <form action="{{ route('balance.store') }}" method="POST">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <div class="col-md-3">
                                <div class="basic-form">
                                    <div class="form-group">
                                        <label>Amount</label>
                                        <input type="number" class="form-control border-none input-flat bg-ash" placeholder="Amount" name="amount" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="basic-form">
                                    <div class="form-group">
                                        <label>Description</label>
                                        <input type="text" class="form-control border-none input-flat bg-ash" placeholder="Description" name="description" required>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3" style="margin-top: 27px;">
                                <div class="basic-form">
                                    <div class="form-group">
                                        <label></label>
                                        <button class="btn btn-default btn-lg" type="submit">Add Amount</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            @endif

            @if($user->price_type==1)
                <div class="card alert">
                    <div class="card-header">
                        <h4>Buy SMS </h4>
                    </div>
                    <div class="card-body">
                        <div class="bootstrap-data-table-panel">
                            <div class="table-responsive">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Buy SMS</th>
                                        <th>Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($balance as $value)
                                        <tr>
                                            <td>{{ $value->created_at }}</td>
                                            <td>Masking: {{ $value->masking_sms }} | Non-Masking: {{ $value->non_masking_sms }}</td>
                                            <td>{{ $value->description }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @else
                <div class="card alert">
                    <div class="card-header">
                        <h4>Amount transfer </h4>
                    </div>
                    <div class="card-body">
                        <div class="bootstrap-data-table-panel">
                            <div class="table-responsive">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Amount</th>
                                        <th>Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($balance as $value)
                                        <tr>
                                            <td>{{ $value->created_at }}</td>
                                            <td>{{ $value->amount }}</td>
                                            <td>{{ $value->description }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
@endsection
