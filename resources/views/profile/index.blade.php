@extends('layouts.app')

@section('title', 'Profile')

@section('breadcrumb')
    <li class="active">Profile</li>
@endsection

@push('header-scripts')

@endpush

@push('footer-scripts')

@endpush


@section('content')
    <div class="row">
        <div class="col-lg-7">
            <div class="card alert">
                <div class="card-header">
                    <h4>Profile</h4>
                </div>
                <form action="{{ route('profile.update', $user->id) }}" method="POST" enctype="multipart/form-data" class="form-horizontal">
                    @csrf
                    @method('PUT')
                <div class="card-body">
                    <div class="user-profile m-t-15">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="user-photo m-b-30">
                                    @if( ! \Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar', 'small'))
                                        <img class="img-responsive" src="{{ asset("assets/images/user-profile.jpg") }}" alt="" />
                                    @else
                                        <img class="img-responsive" src="{{ asset(\Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar', 'medium')) }}" alt=""/>
                                    @endif
                                </div>
                                <hr>
                                <input type="file" name="avatar">
                            </div>
                            <div class="col-lg-8">
                                <div class="user-profile-name">{{ $user->first_name }} {{ $user->last_name }}</div>
                                <div class="custom-tab user-profile-tab">
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#1" aria-controls="1" role="tab" data-toggle="tab">About</a></li>
                                    </ul>
                                    <br>
                                    <div class="tab-content">
                                        <div class="horizontal-form">
                                            <form class="form-horizontal">
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">First Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Last Name</label>
                                                    <div class="col-sm-10">
                                                        <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label class="col-sm-2 control-label">Last Name</label>
                                                    <div class="col-sm-10">
                                                        <textarea class="form-control" rows="3" name="address">{{ $user->address }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-sm-offset-2 col-sm-10">
                                                        <button type="submit" class="btn btn-default">Update</button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>
        <div class="col-lg-5">
            <div class="card alert">
                <div class="card-header pr">
                    <h4>Change Password</h4>
                </div>
                <form action="{{ route('profile.password.change', $user->id) }}" method="POST" class="form-horizontal">
                    @csrf
                    @method('PUT')
                    <div class="basic-form m-t-20">
                        <div class="form-group">
                            <label>New Password</label>
                            <input type="password" name="password" class="form-control" required placeholder="Type new password">
                        </div>
                    </div>
                <button class="btn btn-default btn-lg m-b-10 bg-default border-none m-r-5 sbmt-btn" type="submit">Change password</button>
                </form>
            </div>

            <div class="card alert">
                <div class="card-header">
                    <h4>Last Activity </h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Last Transaction <span class="badge">{{ $transaction->amount }}</span></li>
                        <li class="list-group-item">Balance <span class="badge">{{ \App\Classes\Helper::transactionBalance(\Illuminate\Support\Facades\Auth::id()) }}</span></li>
                        {{--<li class="list-group-item">Last Login <span class="badge">Sat, 18th Aug 2018</span></li>--}}
                        {{--<li class="list-group-item">Last Login IP <span class="badge">210.1.246.22</span></li>--}}
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection