@if ($errors->any())
    <div class="alert fade in alert-dismissable">
        <ul class="list-group">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            @foreach ($errors->all() as $error)
                <li class="list-group-item list-group-item-danger"><strong>{{ __('messages.Error!') }}</strong> {{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif