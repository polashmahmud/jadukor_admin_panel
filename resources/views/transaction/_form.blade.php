<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Payment Method</label>
        <select class="form-control bg-ash border-none js-example-basic-single" name="payment_method">
            <option></option>
            @if($transaction->exists)
                @foreach($paymentMethod as $value)
                    <option value="{{ $value->name }}" @if($value->name == $transaction->payment_method) selected @endif>{{ $value->name }}</option>
                @endforeach
            @else
                @foreach($paymentMethod as $value)
                    <option value="{{ $value->name }}">{{ $value->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Description</label>
        <textarea class="form-control border-none input-flat bg-ash" rows="3" name="description" placeholder="Description" >{{ old('description',$transaction->description) }}</textarea>
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Amount</label>
        <input type="number" class="form-control border-none input-flat bg-ash" name="amount" placeholder="Amount" value="{{ old('amount',$transaction->amount) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Add or Deduct Balance</label>
        @if($transaction->exists)
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="state" value="0" @if($transaction->state ==0) checked @endif >
                                <label class="form-check-label" for="type">
                                    Deduct
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="state" value="1" @if($transaction->state ==1) checked @endif>
                                <label class="form-check-label" for="type">
                                    Add
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="state" value="0">
                                <label class="form-check-label" for="type">
                                    Deduct
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="state" value="1" checked>
                                <label class="form-check-label" for="type">
                                    Add
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>