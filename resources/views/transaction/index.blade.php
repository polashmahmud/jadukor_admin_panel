@extends('layouts.app')

@section('title', 'Transaction')

@section('breadcrumb')
    <li class="active">Transaction</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
    <link href={{ asset("css/select2.min.css") }} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
    <script src={{ asset("js/select2.min.js") }}></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                allowClear: true,
                placeholder: 'Select an option'
            });
        });
    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card alert">
                <div class="card-header pr">
                    @if($transaction->exists)
                        <h4>Edit Transaction</h4>
                    @else
                        <h4>Add New Transaction</h4>
                    @endif
                </div>

                @if($transaction->exists)
                    <form method="POST" action="{{ route('transaction.update', $transaction->id) }}">
                        @method('PATCH')
                        @else
                            <form action="{{ route('transaction.store') }}" method="POST">
                                @endif
                                @csrf
                                <input type="hidden" name="user_id" value="{{ $user->id }}">

                                @include('transaction._form')

                                @if($transaction->exists)
                                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                                    <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('transaction.index') }}?name={{$user->username}}">Back</a>
                                @else
                                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                                @endif
                            </form>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card alert">
                <div class="card-header">
                    <h4>All Transaction </h4>
                    <span class="pull-right label label-default">TK: {{ \App\Classes\Helper::transactionBalance($user->id) }}</span>
                </div>
                <hr>
                @include('common.datatable')
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection
