@extends('layouts.app')

@section('title', 'Master Reseller')

@section('breadcrumb')
    <li class="active">Master Reseller</li>
@endsection

@push('header-scripts')

@endpush

@push('footer-scripts')
<script>
    function yesnoCheck() {
        if (document.getElementById('yesCheck').checked) {
            document.getElementById('ifYes').style.display = '';
        }
        else document.getElementById('ifYes').style.display = 'none';
    }
</script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card alert">
                <div class="card-header">
                    @if($mastereseller->exists)
                        <h4>Edit Master Reseller</h4>
                    @else
                        <h4>Add New Master Reseller</h4>
                    @endif
                    <h4></h4>
                    <hr>
                </div>
                <div class="card-body">
                    @if($mastereseller->exists)
                    <form method="POST" action="{{ route('master-reseller.update', $mastereseller->id) }}">
                    @method('PATCH')
                    @else
                    <form action="{{ route('master-reseller.store') }}" method="POST">
                    @endif
                    @csrf
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="form-horizontal">
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">First Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="first_name" placeholder="Enter First Name" value="{{ old('first_name',$mastereseller->first_name) }}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Last Name</label>
                                    <div class="col-sm-10">
                                        <input type="text" class="form-control" name="last_name" placeholder="Enter Last Name" value="{{ old('last_name',$mastereseller->last_name) }}" required>
                                    </div>
                                </div>
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="{{ old('username',$mastereseller->username) }}" disabled>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Username</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="username" placeholder="Enter Username" value="{{ old('username',$mastereseller->username) }}" required>
                                        </div>
                                    </div>
                                @endif
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" value="{{ old('phone',$mastereseller->phone) }}" disabled>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="phone" placeholder="Enter Phone" value="{{ old('phone',$mastereseller->phone) }}" required>
                                        </div>
                                    </div>
                                @endif
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" value="{{ old('email',$mastereseller->email) }}" disabled>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" name="email" placeholder="Enter Email" value="{{ old('email',$mastereseller->email) }}" required>
                                        </div>
                                    </div>
                                @endif
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="password" placeholder="Enter Password">
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" name="password" placeholder="Enter Password" required>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Address</label>
                                    <div class="col-sm-10">
                                        <textarea class="form-control" rows="3" name="address" placeholder="Address" >{{ old('address',$mastereseller->address) }}</textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-7">
                            <div class="form-horizontal">
                                <div class="card alert">
                                    <div class="card-header">
                                        <h4>Setup Prices </h4>
                                        <span class="pull-right">
                                            <a class="btn btn-link" href="{{ route('default-pricing.index') }}">Default Prices</a>
                                        </span>
                                    </div>
                                </div>
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Pricing Type</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                @if($mastereseller->price_type ==0)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="price_type" value="0"  checked> Money Type
                                                </div>
                                                @endif
                                                @if($mastereseller->price_type ==1)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="price_type" value="1"  checked> SMS Type
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Pricing Type</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                @if(\Illuminate\Support\Facades\Auth::user()->role ==1 || \Illuminate\Support\Facades\Auth::user()->price_type ==0)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="price_type" value="0" checked onclick="javascript:yesnoCheck();" id="noCheck"> Money Type
                                                </div>
                                                @endif
                                                    @if(\Illuminate\Support\Facades\Auth::user()->role ==1 || \Illuminate\Support\Facades\Auth::user()->price_type ==1)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="price_type" value="1" onclick="javascript:yesnoCheck();" id="yesCheck"> SMS Type
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                @if($mastereseller->exists)
                                    @if($mastereseller->price_type ==1)
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Average Masking Price</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="average_masking_price" placeholder="Average Masking SMS Price" value="{{ old('average_masking_price',$mastereseller->average_masking_price) }}" step="any">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Average Non Masking Price</label>
                                            <div class="col-sm-10">
                                                <input type="number" class="form-control" name="average_non_masking_price" placeholder="Average Non Masking SMS Price" value="{{ old('average_non_masking_price',$mastereseller->average_non_masking_price) }}" step="any">
                                            </div>
                                        </div>
                                    @endif
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Average Masking Price</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" name="average_masking_price" placeholder="Average Masking SMS Price" value="{{ old('average_masking_price',$mastereseller->average_masking_price) }}" step="any">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Average Non Masking Price</label>
                                        <div class="col-sm-10">
                                            <input type="number" class="form-control" name="average_non_masking_price" placeholder="Average Non Masking SMS Price" value="{{ old('average_non_masking_price',$mastereseller->average_non_masking_price) }}" step="any">
                                        </div>
                                    </div>
                                @endif
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Role</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="4" @if($mastereseller->role == 4) checked @endif > User
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="3" @if($mastereseller->role == 3) checked @endif > Reseller
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="2" @if($mastereseller->role == 2) checked @endif > Master Reseller
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="1" @if($mastereseller->role == 1) checked @endif > Admin
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Role</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="4" checked> User
                                                </div>
                                                @if(\Illuminate\Support\Facades\Auth::user()->role == 1 || \Illuminate\Support\Facades\Auth::user()->role == 2 || \Illuminate\Support\Facades\Auth::user()->role == 3)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="3"> Reseller
                                                </div>
                                                @endif
                                                @if(\Illuminate\Support\Facades\Auth::user()->role == 1 || \Illuminate\Support\Facades\Auth::user()->role == 2)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="2"> Master Reseller
                                                </div>
                                                @endif
                                                @if(\Illuminate\Support\Facades\Auth::user()->role == 1)
                                                <div class="col-lg-3">
                                                    <input type="radio" name="role" value="1"> Admin
                                                </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if($mastereseller->exists)
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="radio" name="status" value="1" @if($mastereseller->status == 1) checked @endif > Active
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="radio" name="status" value="0" @if($mastereseller->status == 0) checked @endif> Inactive
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @else
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Status</label>
                                        <div class="col-sm-10">
                                            <div class="row">
                                                <div class="col-lg-3">
                                                    <input type="radio" name="status" value="1" checked> Active
                                                </div>
                                                <div class="col-lg-3">
                                                    <input type="radio" name="status" value="0"> Inactive
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif

                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        @if($mastereseller->exists)
                                            <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                                        @else
                                            <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection