<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Template Name" value="{{ old('name',$template->name) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Content</label>
        <textarea class="form-control border-none input-flat bg-ash" name="content" cols="30" rows="10">{{ old('content',$template->content) }}</textarea>
    </div>
</div>
