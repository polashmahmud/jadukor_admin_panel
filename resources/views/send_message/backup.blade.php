@extends('layouts.app')

@section('title', 'Send SMS')

@section('breadcrumb')
    <li class="active">Send SMS</li>
@endsection

@push('header-scripts')
    <link href={{asset("css/jquery.datetimepicker.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{ asset("js/jquery.datetimepicker.full.min.js") }} type="text/javascript"></script>
    <script src={{ asset("assets/js/jquery.textareaCounter.plugin.js") }} type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            var arabicPattern = /[\u0600-\u06FF]/;
            $('#recipient #message').bind('input propertychange', function(ev) {
                var text = ev.target.value;
                if (arabicPattern.test(text)) {
                    // arabic;
                    $('#recipient #message').css('direction', 'rtl')
                }else{
                    $('#recipient #message').css('direction', 'ltr')
                }
            });

            <!-- Sart Recipient-->

            $('#recipient .count_me').textareaCount({
                'maxCharacterSize': 765,
                'textAlign': 'right',
                'warningColor': '#CC3300',
                'warningNumber': 160,
                'isCharacterCount': true,
                'isWordCount': false,
                'displayFormat': '#input Characters | #left Characters Left',
                'originalStyle': 'contacts-count',
                'counterCssClass':'#recipient .charleft',

            }, function (data) {
                var parts = 1;
                var isUnicode = isDoubleByte($('#recipient .count_me').val());
                var typeRadio = $('input:radio[name=recipientsmsRadios]:checked').val();
                var charPerSMS = 160;
                if(isUnicode)
                {  charPerSMS = 70;
                    if (data.input > 70) {
                        parts = Math.ceil(data.input / 67);
                        charPerSMS = 67;
                    }
                    if(typeRadio=="text")
                    {
                        $("#recipientsmsRadiosUnicode").prop('checked', true);
                    }else if(typeRadio=="flash")
                    {
                        $("#recipientsmsRadiosUnicodeFlash").prop('checked', true);
                    }

                }
                else
                {
                    var isUnicodeNormal = isDoubleByteNormal($('#recipient .count_me').val());
                    if(isUnicodeNormal)
                    {   charPerSMS = 140;
                        if (data.input > 140) {
                            parts = Math.ceil(data.input / 134);
                            charPerSMS = 134;
                        }
                    }else{
                        charPerSMS = 160;
                        if (data.input > 160) {
                            parts = Math.ceil(data.input / 153);
                            charPerSMS = 153;
                        }
                    }

                    if(typeRadio=="unicode")
                    {
                        $("#recipientsmsRadiosText").prop('checked', true);
                    }else if(typeRadio=="flashunicode")
                    {
                        $("#recipientsmsRadiosFlash").prop('checked', true);
                    }
                }

                $('#recipient .parts-count').text('| ' + parts + ' SMS ('+charPerSMS+' Char./SMS)');
            });
            <!-- End Recipient-->

            <!-- Sart Group SMS-->

            $('#group .count_me').textareaCount({
                'maxCharacterSize': 765,
                'textAlign': 'right',
                'warningColor': '#CC3300',
                'warningNumber': 160,
                'isCharacterCount': true,
                'isWordCount': false,
                'displayFormat': '#input Characters | #left Characters Left',
                'originalStyle': 'contacts-count',
                'counterCssClass':'#group .charleft',

            }, function (data) {
                var parts = 1;
                var isUnicode = isDoubleByte($('#group .count_me').val());
                var typeRadio = $('input:radio[name=groupssmsRadios]:checked').val();
                var charPerSMS = 160;
                if(isUnicode)
                {  charPerSMS = 70;
                    if (data.input > 70) {
                        parts = Math.ceil(data.input / 67);
                        charPerSMS = 67;
                    }
                    if(typeRadio=="text")
                    {
                        $("#groupssmsRadiosUnicode").prop('checked', true);
                    }else if(typeRadio=="flash")
                    {
                        $("#groupssmsRadiosUnicodeFlash").prop('checked', true);
                    }
                }else
                {
                    var isUnicodeNormal = isDoubleByteNormal($('#group .count_me').val());
                    if(isUnicodeNormal)
                    {   charPerSMS = 140;
                        if (data.input > 140) {
                            parts = Math.ceil(data.input / 134);
                            charPerSMS = 134;
                        }
                    }else{
                        charPerSMS = 160;
                        if (data.input > 160) {
                            parts = Math.ceil(data.input / 153);
                            charPerSMS = 153;
                        }
                    }
                    if(typeRadio=="unicode")
                    {
                        $("#groupssmsRadiosText").prop('checked', true);
                    }else if(typeRadio=="flashunicode")
                    {
                        $("#groupssmsRadiosFlash").prop('checked', true);
                    }
                }

                $('#group .parts-count').text('| ' + parts + ' SMS ('+charPerSMS+' Char./SMS)');
            });
            <!-- End Group SMS-->

            <!-- Sart Upload SMS-->

            $('#upload .count_me').textareaCount({
                'maxCharacterSize': 765,
                'textAlign': 'right',
                'warningColor': '#CC3300',
                'warningNumber': 160,
                'isCharacterCount': true,
                'isWordCount': false,
                'displayFormat': '#input Characters | #left Characters Left',
                'originalStyle': 'contacts-count',
                'counterCssClass':'#upload .charleft',

            }, function (data) {
                var parts = 1;
                var isUnicode = isDoubleByte($('#upload .count_me').val());
                var typeRadio = $('input:radio[name=uploadsmsRadios]:checked').val();
                var charPerSMS = 160;
                if(isUnicode)
                {     charPerSMS = 70;
                    if (data.input > 70) {
                        parts = Math.ceil(data.input / 67);
                        charPerSMS = 67;
                    }
                    if(typeRadio=="text")
                    {
                        $("#uploadsmsRadiosUnicode").prop('checked', true);
                    }else if(typeRadio=="flash")
                    {
                        $("#uploadsmsRadiosUnicodeFlash").prop('checked', true);
                    }
                }else
                {
                    var isUnicodeNormal = isDoubleByteNormal($('#upload .count_me').val());
                    if(isUnicodeNormal)
                    {   charPerSMS = 140;
                        if (data.input > 140) {
                            parts = Math.ceil(data.input / 134);
                            charPerSMS = 134;
                        }
                    }else{
                        charPerSMS = 160;
                        if (data.input > 160) {
                            parts = Math.ceil(data.input / 153);
                            charPerSMS = 153;
                        }
                    }

                    if(typeRadio=="unicode")
                    {
                        $("#uploadsmsRadiosText").prop('checked', true);
                    }else if(typeRadio=="flashunicode")
                    {
                        $("#uploadsmsRadiosFlash").prop('checked', true);
                    }
                }

                $('#upload .parts-count').text('| ' + parts + ' SMS ('+charPerSMS+' Char./SMS)');
            });
            <!-- End Upload SMS-->
            function isDoubleByte(str) {
                for (var i = 0, n = str.length; i < n; i++) {
                    //if (str.charCodeAt( i ) > 255 && str.charCodeAt( i )!== 8364 )
                    if (str.charCodeAt( i ) > 255)
                    { return true; }
                }
                return false;
            }

            function isDoubleByteNormal(str) {
                for (var i = 0, n = str.length; i < n; i++) {
                    if (str.charCodeAt( i ) ==91
                        || str.charCodeAt( i ) ==92
                        || str.charCodeAt( i ) ==93
                        || str.charCodeAt( i ) ==94
                        || str.charCodeAt( i ) ==123
                        || str.charCodeAt( i ) ==124
                        || str.charCodeAt( i ) ==125
                        || str.charCodeAt( i ) ==126
                    ) { return true; }
                }
                return false;
            }



        });

    </script>
    <script type="text/javascript">

        function yesnoCheck() {
            if (document.getElementById('yesCheck').checked) {
                document.getElementById('ifYes').style.display = 'block';
            }
            else document.getElementById('ifYes').style.display = 'none';

        }
        jQuery('#datetimepicker').datetimepicker();

    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card alert">
                <div class="card-header">
                    <h4>Send SMS</h4>
                </div>
                <div class="card-body">
                    <div class="portlet light ">
                        <div class="portlet-body">
                            <ul class="nav nav-tabs margin-bottom-20">
                                <li class="active">
                                    <a href="#recipient" data-toggle="tab">Fast SMS </a>
                                </li>
                                <li>
                                    <a href="#group" data-toggle="tab">Group Contact </a>
                                </li>
                                <li>
                                    <a href="#upload" data-toggle="tab">Upload File </a>
                                </li>
                            </ul>

                            <div class="tab-content">
                                <div class="tab-pane fade active in" id="recipient">
                                    <form data-toggle="validator" id="send-form" role="form" method="POST" action="{{ route('send-sms.store') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="control-label">Select Sender ID
                                                <span class="required"> * </span>
                                            </label>
                                            <select class="form-control select2" name="sender_id" id="sender_id" required>
                                                <option value="non_masking">Non-Masking</option>
                                                @foreach(\Illuminate\Support\Facades\Auth::user()->maskings as $masking)
                                                <option value="{{ $masking->name }}" >{{ $masking->display_name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block"><a href="#">Request New Sender ID</a></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter Mobile Numbers
                                                <span class="required"> * </span>
                                            </label>
                                            <textarea class="form-control" rows="2" name="msisdn" id="msisdn"  placeholder="Please start with country code" required></textarea>
                                            <span class="help-block">New line separated</span>
                                        </div>
                                        <div class="form-group">
                                            <label>Select SMS Type
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="recipientsmsRadios" id="recipientsmsRadiosText" value="text" checked> Text
                                                    <span></span>
                                                </label>

                                                <label class="mt-radio">
                                                    <input type="radio" name="recipientsmsRadios" id="recipientsmsRadiosUnicode" value="unicode"> Unicode
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter SMS Content
                                                <span class="required"> * </span>
                                            </label>
                                            <textarea class="count_me form-control" style="direction:ltr;" name="message" id="message" required></textarea>

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <span class="help-block">CHECK YOUR SMS COUNT </span>
                                                </div>
                                                <div class="col-md-7">
                                                    <div style="float: right"> <span  class='charleft'>&nbsp;</span><span class='parts-count'></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Schedule SMS
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" onclick="javascript:yesnoCheck();" name="schedul_sms" value="0" id="noCheck" checked> Send Now
                                                </label>

                                                <label class="mt-radio">
                                                    <input type="radio" onclick="javascript:yesnoCheck();" name="schedul_sms" value="1" id="yesCheck"> Send Later
                                                </label>
                                            </div>
                                            <div id="ifYes" style="display:none" class="form-group">
                                                <input id="datetimepicker" type="text" name="time">
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn yellow-gold">Send SMS</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="group">
                                    <form data-toggle="validator" id="send-form" role="form">
                                        <div class="form-group">
                                            <label class="control-label">Select Sender ID
                                                <span class="required"> * </span>
                                            </label>
                                            <select class="form-control select2" name="sender_id" id="sender_id" required>
                                                <option value="non_masking">Non-Masking</option>
                                                @foreach(\Illuminate\Support\Facades\Auth::user()->maskings as $masking)
                                                    <option value="{{ $masking->name }}" >{{ $masking->display_name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block"><a href="#">Request New Sender ID</a></span>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">Select Contact Group
                                                <span class="required"> * </span>
                                            </label>
                                            <select class="form-control select2" name="groups_id[]" id="groups_id" required>
                                                <option></option>
                                                @foreach($phoneGroups as $group)
                                                    <option value="{{ $group->id }}" >{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>

                                        <div class="form-group">
                                            <label>Select SMS Type
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="groupssmsRadios" id="groupssmsRadiosText" value="text" checked> Text
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="groupssmsRadios" id="groupssmsRadiosUnicode" value="unicode"> Unicode
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter SMS Content
                                                <span class="required"> * </span>
                                            </label>
                                            <textarea class="count_me form-control" name="message" id="message" required></textarea>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <span class="help-block">CHECK YOUR SMS COUNT </span>
                                                </div>
                                                <div class="col-md-7">
                                                    <div style="float: right"> <span  class='charleft'>&nbsp;</span><span class='parts-count'></span></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Schedule SMS
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="scheduleRecipientsRadios"   id="sendnow" value="now" checked> Send Now
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="scheduleRecipientsRadios"   id="sendlater" value="later"> Send Later
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>

                                        <div class="row" id="sendoption" style="display:none!important">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="input-group date form_meridian_datetime">
                                                        <input type="text" name="scheduledRecipientsDateTime" id="scheduledRecipientsDateTime" size="16" class="form-control" >
                                                        <span class="input-group-btn">
                                                                            <button class="btn default date-set" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <input type="hidden" name="isDeliveryReportRequest" value="1">
                                        <div class="form-actions">
                                            <button type="submit" class="btn yellow-gold">Send SMS</button>
                                        </div>
                                    </form>
                                </div>
                                <div class="tab-pane fade" id="upload">
                                    <form data-toggle="validator" id="send-form" method="post"  role="form" enctype="multipart/form-data" name="send-form">
                                        <div class="form-group">
                                            <label class="control-label">Select Sender ID
                                                <span class="required"> * </span>
                                            </label>
                                            <select class="form-control select2" name="sender_id" id="sender_id" required>
                                                <option value="1">Non-Masking</option>
                                                <option value="2295" >JADUKOR</option>
                                                <option value="2597" >CyberAware</option>
                                            </select>
                                            <span class="help-block"><a href="">Request New Sender ID</a></span>
                                        </div>
                                        <div class="fileinput fileinput-new margin-bottom-10" data-provides="fileinput">
                                            <label>Select File (xls, xlsx, ods, txt, csv)</label>
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                                    <span class="fileinput-new"> Select file </span>
                                                                    <span class="fileinput-exists"> Change </span>
                                                                    <input type="file" id="contactsFile" name="contactsFile"> </span>
                                                <a href="#" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                            <span class="help-block">
                                                                <a href="#filedata" data-toggle="modal"> File data instructions </a>
                                                            </span>
                                        </div>
                                        <div class="form-group">
                                            <label>Select SMS Type
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="uploadsmsRadios" id="uploadsmsRadiosText" value="text" checked> Text
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="uploadsmsRadios" id="uploadsmsRadiosUnicode" value="unicode"> Unicode
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="uploadsmsRadios" id="uploadsmsRadiosDynamic" value="dynamic"> Dynamic
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter SMS Content
                                                <span class="required"> * </span>
                                            </label>
                                            <textarea class="count_me form-control" name="message" id="message" required></textarea>
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <span class="help-block">CHECK YOUR SMS COUNT </span>
                                                </div>
                                                <div class="col-md-7">
                                                    <div style="float: right"> <span  class='charleft'>&nbsp;</span><span class='parts-count'></span></div>
                                                </div>
                                            </div>
                                            <div class="row">  <span class="help-block">template </span>
                                                <select>
                                                    <option value="0"> &nbsp;</option>
                                                    <option value="1">fgfdgfd</option>
                                                    <option value="2295" >fdgfdgfdgfdgfd</option>
                                                    <option value="2597" >Cfgfdgon</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Schedule SMS
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" name="scheduleRecipientsRadios"   id="sendnow" value="now" checked> Send Now
                                                    <span></span>
                                                </label>
                                                <label class="mt-radio">
                                                    <input type="radio" name="scheduleRecipientsRadios"   id="sendlater" value="later"> Send Later
                                                    <span></span>
                                                </label>
                                            </div>
                                        </div>
                                        <div class="row" id="sendoption" style="display:none!important">
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <div class="input-group date form_meridian_datetime">
                                                        <input type="text" name="scheduledRecipientsDateTime" id="scheduledRecipientsDateTime" size="16" class="form-control" >
                                                        <span class="input-group-btn">
                                                                            <button class="btn default date-set" type="button">
                                                                                <i class="fa fa-calendar"></i>
                                                                            </button>
                                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-actions">
                                            <button type="submit" class="btn yellow-gold">Send SMS</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            <div class="row">
                <div class="col-md-4">
                    <div class="card bg-primary">
                        <div class="media">
                            <div class="media-left meida media-middle">
                                <span><i class="ti-pencil f-s-48 color-white"></i></span>
                            </div>
                            <div class="media-body media-text-right">
                                <h4>278</h4>
                                <h6>SMS Balance</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-primary">
                        <div class="media">
                            <div class="media-left meida media-middle">
                                <span><i class="ti-pencil f-s-48 color-white"></i></span>
                            </div>
                            <div class="media-body media-text-right">
                                <h4>278</h4>
                                <h6>Send so far</h6>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="card bg-primary">
                        <div class="media">
                            <div class="media-left meida media-middle">
                                <span><i class="ti-pencil f-s-48 color-white"></i></span>
                            </div>
                            <div class="media-body media-text-right">
                                <h4>278</h4>
                                <h6>Send Today</h6>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12">
                    <div class="card alert">
                        <div class="card-header">
                            <h4>SMS Content</h4>
                        </div>
                        <div class="card-body">
                            <ul class="list-group">
                                <li class="list-group-item">160 Characters are counted as 1 SMS in case of English language &amp; 70 in other language.</li>
                                <li class="list-group-item">One simple text message containing extended GSM character set (~^{}[]\|\n) is of 140 characters long. Check your SMS count before pushing SMS.</li>
                                <li class="list-group-item">Check your balance before througing sms</li>
                                <li class="list-group-item">Number format must be start with 88, for example 8801727000000</li>
                                <li class="list-group-item">You may send up to 3 sms size in a single try.</li>
                                <li class="list-group-item">Type one number in one line, for example <br> 8801727000000 <br> 8801727000001</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection