@if(\Illuminate\Support\Facades\Auth::user()->price_type==1)
<div class="row">
    <div class="col-md-4">
        <div class="card bg-primary">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="ti-pencil f-s-48 color-white"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h4>{{ \App\Classes\Helper::BalanceMaskingSMS(\Illuminate\Support\Facades\Auth::id()) }}</h4>
                    <h6>Balance Masking</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card bg-primary">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="ti-pencil f-s-48 color-white"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h4>{{ \App\Classes\Helper::BalanceNonMaskingSMS(\Illuminate\Support\Facades\Auth::id()) }}</h4>
                    <h6>Balance Non-Masking</h6>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="card bg-primary">
            <div class="media">
                <div class="media-left meida media-middle">
                    <span><i class="ti-pencil f-s-48 color-white"></i></span>
                </div>
                <div class="media-body media-text-right">
                    <h4>{{ \App\Classes\Helper::sendSoFar(\Illuminate\Support\Facades\Auth::id()) }}</h4>
                    <h6>Send so far</h6>
                </div>
            </div>
        </div>
    </div>
</div>
@else
    <div class="row">
        <div class="col-md-6">
            <div class="card bg-primary">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="ti-pencil f-s-48 color-white"></i></span>
                    </div>
                    <div class="media-body media-text-right">
                        <h4>{{ \App\Classes\Helper::Balance(\Illuminate\Support\Facades\Auth::id()) }}</h4>
                        <h6>SMS Balance</h6>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card bg-primary">
                <div class="media">
                    <div class="media-left meida media-middle">
                        <span><i class="ti-pencil f-s-48 color-white"></i></span>
                    </div>
                    <div class="media-body media-text-right">
                        <h4>{{ \App\Classes\Helper::sendSoFar(\Illuminate\Support\Facades\Auth::id()) }}</h4>
                        <h6>Send so far</h6>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
