@extends('layouts.app')

@section('title', 'Custom SMS')

@section('breadcrumb')
    <li class="active">Custom SMS</li>
@endsection

@push('header-scripts')
    <link href={{asset("css/jquery.datetimepicker.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{ asset("js/jquery.datetimepicker.full.min.js") }} type="text/javascript"></script>
    <script src={{ asset("assets/js/jquery.textareaCounter.plugin.js") }} type="text/javascript"></script>
    <script>
        $(document).ready(function(){
            var arabicPattern = /[\u0600-\u06FF]/;
            $('#recipient #message').bind('input propertychange', function(ev) {
                var text = ev.target.value;
                if (arabicPattern.test(text)) {
                    // arabic;
                    $('#recipient #message').css('direction', 'rtl')
                }else{
                    $('#recipient #message').css('direction', 'ltr')
                }
            });

            <!-- Sart Recipient-->

            $('#recipient .count_me').textareaCount({
                'maxCharacterSize': 765,
                'textAlign': 'right',
                'warningColor': '#CC3300',
                'warningNumber': 160,
                'isCharacterCount': true,
                'isWordCount': false,
                'displayFormat': '#input Characters | #left Characters Left',
                'originalStyle': 'contacts-count',
                'counterCssClass':'#recipient .charleft',

            }, function (data) {
                var parts = 1;
                var isUnicode = isDoubleByte($('#recipient .count_me').val());
                var typeRadio = $('input:radio[name=recipientsmsRadios]:checked').val();
                var charPerSMS = 160;
                if(isUnicode)
                {  charPerSMS = 70;
                    if (data.input > 70) {
                        parts = Math.ceil(data.input / 67);
                        charPerSMS = 67;
                    }
                    if(typeRadio=="text")
                    {
                        $("#recipientsmsRadiosUnicode").prop('checked', true);
                    }else if(typeRadio=="flash")
                    {
                        $("#recipientsmsRadiosUnicodeFlash").prop('checked', true);
                    }

                }
                else
                {
                    var isUnicodeNormal = isDoubleByteNormal($('#recipient .count_me').val());
                    if(isUnicodeNormal)
                    {   charPerSMS = 140;
                        if (data.input > 140) {
                            parts = Math.ceil(data.input / 134);
                            charPerSMS = 134;
                        }
                    }else{
                        charPerSMS = 160;
                        if (data.input > 160) {
                            parts = Math.ceil(data.input / 153);
                            charPerSMS = 153;
                        }
                    }

                    if(typeRadio=="unicode")
                    {
                        $("#recipientsmsRadiosText").prop('checked', true);
                    }else if(typeRadio=="flashunicode")
                    {
                        $("#recipientsmsRadiosFlash").prop('checked', true);
                    }
                }

                $('#recipient .parts-count').text('| ' + parts + ' SMS ('+charPerSMS+' Char./SMS)');
            });
            <!-- End Recipient-->

            <!-- Sart Group SMS-->

            $('#group .count_me').textareaCount({
                'maxCharacterSize': 765,
                'textAlign': 'right',
                'warningColor': '#CC3300',
                'warningNumber': 160,
                'isCharacterCount': true,
                'isWordCount': false,
                'displayFormat': '#input Characters | #left Characters Left',
                'originalStyle': 'contacts-count',
                'counterCssClass':'#group .charleft',

            }, function (data) {
                var parts = 1;
                var isUnicode = isDoubleByte($('#group .count_me').val());
                var typeRadio = $('input:radio[name=groupssmsRadios]:checked').val();
                var charPerSMS = 160;
                if(isUnicode)
                {  charPerSMS = 70;
                    if (data.input > 70) {
                        parts = Math.ceil(data.input / 67);
                        charPerSMS = 67;
                    }
                    if(typeRadio=="text")
                    {
                        $("#groupssmsRadiosUnicode").prop('checked', true);
                    }else if(typeRadio=="flash")
                    {
                        $("#groupssmsRadiosUnicodeFlash").prop('checked', true);
                    }
                }else
                {
                    var isUnicodeNormal = isDoubleByteNormal($('#group .count_me').val());
                    if(isUnicodeNormal)
                    {   charPerSMS = 140;
                        if (data.input > 140) {
                            parts = Math.ceil(data.input / 134);
                            charPerSMS = 134;
                        }
                    }else{
                        charPerSMS = 160;
                        if (data.input > 160) {
                            parts = Math.ceil(data.input / 153);
                            charPerSMS = 153;
                        }
                    }
                    if(typeRadio=="unicode")
                    {
                        $("#groupssmsRadiosText").prop('checked', true);
                    }else if(typeRadio=="flashunicode")
                    {
                        $("#groupssmsRadiosFlash").prop('checked', true);
                    }
                }

                $('#group .parts-count').text('| ' + parts + ' SMS ('+charPerSMS+' Char./SMS)');
            });
            <!-- End Group SMS-->

            <!-- Sart Upload SMS-->

            $('#upload .count_me').textareaCount({
                'maxCharacterSize': 765,
                'textAlign': 'right',
                'warningColor': '#CC3300',
                'warningNumber': 160,
                'isCharacterCount': true,
                'isWordCount': false,
                'displayFormat': '#input Characters | #left Characters Left',
                'originalStyle': 'contacts-count',
                'counterCssClass':'#upload .charleft',

            }, function (data) {
                var parts = 1;
                var isUnicode = isDoubleByte($('#upload .count_me').val());
                var typeRadio = $('input:radio[name=uploadsmsRadios]:checked').val();
                var charPerSMS = 160;
                if(isUnicode)
                {     charPerSMS = 70;
                    if (data.input > 70) {
                        parts = Math.ceil(data.input / 67);
                        charPerSMS = 67;
                    }
                    if(typeRadio=="text")
                    {
                        $("#uploadsmsRadiosUnicode").prop('checked', true);
                    }else if(typeRadio=="flash")
                    {
                        $("#uploadsmsRadiosUnicodeFlash").prop('checked', true);
                    }
                }else
                {
                    var isUnicodeNormal = isDoubleByteNormal($('#upload .count_me').val());
                    if(isUnicodeNormal)
                    {   charPerSMS = 140;
                        if (data.input > 140) {
                            parts = Math.ceil(data.input / 134);
                            charPerSMS = 134;
                        }
                    }else{
                        charPerSMS = 160;
                        if (data.input > 160) {
                            parts = Math.ceil(data.input / 153);
                            charPerSMS = 153;
                        }
                    }

                    if(typeRadio=="unicode")
                    {
                        $("#uploadsmsRadiosText").prop('checked', true);
                    }else if(typeRadio=="flashunicode")
                    {
                        $("#uploadsmsRadiosFlash").prop('checked', true);
                    }
                }

                $('#upload .parts-count').text('| ' + parts + ' SMS ('+charPerSMS+' Char./SMS)');
            });
            <!-- End Upload SMS-->
            function isDoubleByte(str) {
                for (var i = 0, n = str.length; i < n; i++) {
                    //if (str.charCodeAt( i ) > 255 && str.charCodeAt( i )!== 8364 )
                    if (str.charCodeAt( i ) > 255)
                    { return true; }
                }
                return false;
            }

            function isDoubleByteNormal(str) {
                for (var i = 0, n = str.length; i < n; i++) {
                    if (str.charCodeAt( i ) ==91
                        || str.charCodeAt( i ) ==92
                        || str.charCodeAt( i ) ==93
                        || str.charCodeAt( i ) ==94
                        || str.charCodeAt( i ) ==123
                        || str.charCodeAt( i ) ==124
                        || str.charCodeAt( i ) ==125
                        || str.charCodeAt( i ) ==126
                    ) { return true; }
                }
                return false;
            }



        });

    </script>
    <script type="text/javascript">

        function yesnoCheck() {
            if (document.getElementById('yesCheck').checked) {
                document.getElementById('ifYes').style.display = 'block';
            }
            else document.getElementById('ifYes').style.display = 'none';

        }
        jQuery('#datetimepicker').datetimepicker();

    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card alert">
                <div class="card-header">
                    <h4>Send Custom SMS</h4>
                </div>
                <div class="card-body">
                    <div class="tab-pane fade active in" id="recipient">
                                    <form data-toggle="validator" id="send-form" role="form" method="POST" enctype="multipart/form-data" action="{{ route('custom-sms.store') }}">
                                        @csrf
                                        <div class="form-group">
                                            <label class="control-label">Select Sender ID
                                                <span class="required"> * </span>
                                            </label>
                                            <select class="form-control select2" name="sender_id" id="sender_id" required>
                                                <option value="non_masking">Non-Masking</option>
                                                @foreach(\Illuminate\Support\Facades\Auth::user()->maskings as $masking)
                                                    <option value="{{ $masking->name }}" >{{ $masking->display_name }}</option>
                                                @endforeach
                                            </select>
                                            <span class="help-block"><a href="#">Request New Sender ID</a></span>
                                        </div>
                                        <div class="form-group">
                                            <label>Enter Mobile Numbers
                                                <span class="required"> * </span>
                                            </label>
                                            <p class="help-block">Input xlsx, xls, txt or CSV File.
                                                Example File: <a href="{{ asset('files/custom_sms.csv') }}">csv</a>,
                                                <a href="{{ asset('files/custom_sms.xls') }}">xlsx, xls</a>,
                                                <a href="{{ asset('files/custom_sms.txt') }}">txt</a>
                                            </p>
                                            <input type="file" class="form-control" name="file">
                                        </div>
                                        <div class="form-group">
                                            <label>Schedule SMS
                                                <span class="required"> * </span>
                                            </label>
                                            <div class="mt-radio-inline">
                                                <label class="mt-radio">
                                                    <input type="radio" onclick="javascript:yesnoCheck();" name="schedul_sms" value="0" id="noCheck" checked> Send Now
                                                </label>

                                                <label class="mt-radio">
                                                    <input type="radio" onclick="javascript:yesnoCheck();" name="schedul_sms" value="1" id="yesCheck"> Send Later
                                                </label>
                                            </div>
                                            <div id="ifYes" style="display:none" class="form-group">
                                                <input id="datetimepicker" type="text" name="time">
                                            </div>
                                        </div>

                                        <div class="form-actions">
                                            <button type="submit" class="btn btn-info">Send And Preview SMS</button>
                                        </div>
                                    </form>
                                </div>
                </div>
            </div>
        </div>

        <div class="col-lg-6">
            @include('send_message.sidebar')
            @include('send_message.sidebar_text')
        </div>
    </div>
@endsection