<div class="row">
    <div class="col-lg-12">
        <div class="card alert">
            <div class="card-header">
                <h4>SMS Content</h4>
            </div>
            <div class="card-body">
                <ul class="list-group">
                    <li class="list-group-item">160 Characters are counted as 1 SMS in case of English language &amp; 70 in other language.</li>
                    <li class="list-group-item">One simple text message containing extended GSM character set (~^{}[]\|\n) is of 140 characters long. Check your SMS count before pushing SMS.</li>
                    <li class="list-group-item">Please type message manually (without copy/paste). Copying text might cause adding special chars from text editors and message will not be delivered properly. [enter] are counted double when sending message without special chars because of GSM specification requirements.</li>
                    <li class="list-group-item">Check your balance before througing sms</li>
                    <li class="list-group-item">Number format must be start with 88, for example 8801727000000</li>
                    <li class="list-group-item">You may send up to 3 sms size in a single try.</li>
                    <li class="list-group-item">Type one number in one line, for example <br> 8801727000000 <br> 8801727000001</li>
                </ul>
            </div>
        </div>
    </div>
</div>