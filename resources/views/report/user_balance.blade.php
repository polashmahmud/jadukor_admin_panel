@extends('layouts.app')

@section('title', 'User Balance')

@section('breadcrumb')
    <li class="active">User Balance</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
    <link href={{ asset("assets/css/lib/calendar2/pignose.calendar.min.css") }} rel="stylesheet">
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
    <script src={{ asset("assets/js/lib/calendar-2/moment.latest.min.js") }}></script>
    <script src={{ asset("assets/js/lib/calendar-2/semantic.ui.min.js") }}></script>
    <script src={{ asset("assets/js/lib/calendar-2/prism.min.js") }}></script>
    <script src={{ asset("assets/js/lib/calendar-2/pignose.calendar.min.js") }}></script>
    <script src={{ asset("assets/js/lib/calendar-2/pignose.init.js") }}></script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <form action="{{ route('report.userBalance') }}" method="get">
                <div class="card alert">
                    <div class="card-body">
                        <div class="card-header m-b-20">
                            <h4>All Transaction</h4>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="radio" name="date" value="today" @if($value == 'today') checked @endif>
                                        <label class="form-check-label" for="type">
                                            Today
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="radio" name="date" value="week" @if($value == 'week') checked @endif>
                                        <label class="form-check-label" for="type">
                                            7 days
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="radio" name="date" value="this_month" @if($value == 'this_month') checked @endif>
                                        <label class="form-check-label" for="type">
                                            This Month
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="radio" name="date" value="last_month" @if($value == 'last_month') checked @endif>
                                        <label class="form-check-label" for="type">
                                            Last Month
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="radio" name="date" value="this_year" @if($value == 'this_year') checked @endif>
                                        <label class="form-check-label" for="type">
                                            This Year
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group">
                                    <div class="form-check">
                                        <input type="radio" name="date" value="last_year" @if($value == 'last_year') checked @endif>
                                        <label class="form-check-label" for="type">
                                            Last Year
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-3">
                            <div class="basic-form">
                                <div class="form-group">
                                    <label>Start Date</label>
                                    <input type="text" class="form-control calendar bg-ash" placeholder="dd/mm/yyyy" id="text-calendar" name="start_date">
                                    <span class="ti-calendar form-control-feedback booking-system-feedback m-t-30"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="basic-form">
                                <div class="form-group">
                                    <label>End Date</label>
                                    <input type="text" class="form-control calendar bg-ash" placeholder="dd/mm/yyyy" id="text-calendar" name="end_date">
                                    <span class="ti-calendar form-control-feedback booking-system-feedback m-t-30"></span>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-2">
                            <div class="basic-form">
                                <div class="form-group" style="margin-top: 28px;">
                                    <button class="btn btn-default btn-lg m-b-10 bg-info border-none m-r-5 sbmt-btn" type="submit">Search</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card alert">
                        <div class="card-header">
                            <h4>Details</h4>
                            <hr>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                                    <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>State</th>
                                        <th>Payment Method</th>
                                        <th>Description</th>
                                        <th>Amount</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($allData as $value)
                                    <tr>
                                        <td>{{ $value->created_at }}</td>
                                        <td>{{ $value->state ? "Credit" : "Debit" }}</td>
                                        <td>{{ $value->payment_method }}</td>
                                        <td>{!! html_entity_decode($value->description) !!}</td>
                                        <td>{{ $value->amount }}</td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /# column -->
            </div>
        </div>
        <!-- /# column -->
    </div>
@endsection