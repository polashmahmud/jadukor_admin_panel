@extends('layouts.app')

@section('title', 'SMS Details')

@section('breadcrumb')
    <li class="active">SMS Details</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
@endpush


@section('content')
     <div class="row">
        <div class="col-lg-12">
            <div class="card alert">
                <div class="card-header">
                    <h4>Details</h4>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Mobile</th>
                                <th>Send Time</th>
                                <th>Sender</th>
                                <th>Operator</th>
                                <th>Charge (credit/bdt)</th>
                                <th>SMS Text</th>
                                <th>Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allData as $value)
                            <tr>
                                <td>{{ $value->number }}</td>
                                <td>{{ $value->created_at }}</td>
                                <td>{{ $value->sender }}</td>
                                <td>{{ App\Classes\Helper::operatorName($value->number) }}</td>
                                <td>{{ $value->cost }}</td>
                                <td>{{ $value->content }}</td>
                                <td>Delivered</td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <!-- /# column -->
    </div>
@endsection