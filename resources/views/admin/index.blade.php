@extends('layouts.app')

@section('title', 'All Admin')

@section('breadcrumb')
    <li class="active">All Admin</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card alert">
                <div class="card-header">
                    <h4>All Admin </h4>
                </div>
                <hr>
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Full Name</th>
                                <th>User Name</th>
                                <th>Email</th>
                                <th>Phone</th>
                                <th>Pricing Type</th>
                                @if(in_array(38, $user_permissions))
                                <th>Balance</th>
                                @endif
                                @if(in_array(39, $user_permissions))
                                <th>Status</th>
                                @endif
                                <th style="width:120px;">Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($admins as $value)
                                <tr>
                                    <td>{{ $value->first_name }} {{ $value->last_name }}</td>
                                    <td>{{ $value->username }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->phone }}</td>
                                    <td>{{ $value->price_type ? "SMS Pricing" : "Money Pricing" }}</td>
                                    @if(in_array(38, $user_permissions))
                                    <td><a class='btn btn-default btn-xs' href='{{ route('transaction.index') }}?name={{ $value->username }}'>BDT: {{ \App\Classes\Helper::transactionBalance($value->id) }}</a></td>
                                    @endif
                                    @if(in_array(39, $user_permissions))
                                    <td>
                                        <form id="status-change-{{ $value->id }}" method="post" action="{{ route('status-change', $value->id) }}" style="display: none;">
                                        @csrf
                                        <input type="hidden" name="table" value="users">
                                        <input type="hidden" name="id" value="{{ $value->id }}">
                                        <input type="hidden" name="column" value="status">
                                        <input type="hidden" name="value" value="{{ $value->status }}">
                                        <input type="hidden" name="message" value="User Status Change">
                                        </form>
                                        <a class='{{ $value->status ? 'btn btn-info btn-xs' : 'btn btn-warning btn-xs' }}' href="" onclick="
                                           if(confirm('Are you sure, You Want to Change this?'))
                                          {
                                            event.preventDefault();
                                            document.getElementById('status-change-{{ $value->id }}').submit();
                                          }
                                          else{
                                            event.preventDefault();
                                          }" >{{ $value->status ? 'Active' : 'Pending' }}</a>
                                    </td>
                                    @endif
                                    <td>
                                        @if(in_array(40, $user_permissions))
                                        <a href="{{ route('admin.show', $value->username) }}"><i class="ti-user color-dark"></i></a>
                                        @endif
                                        @if(in_array(41, $user_permissions))
                                        <a href="{{ route('admin.edit', $value->id) }}"><i class="ti-pencil-alt color-success"></i></a>
                                        @endif
                                        @if(in_array(42, $user_permissions))
                                        <form id="delete-form-{{ $value->id }}" method="post" action="{{ route('admin.destroy', $value->id) }}" style="display: none">
                                        @csrf
                                        @method('Delete')
                                        </form>
                                        <a href="" onclick="
                                          if(confirm('Are you sure, You Want to delete this?'))
                                          {
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{ $value->id }}').submit();
                                          }
                                          else{
                                            event.preventDefault();
                                          }" ><i class="ti-trash color-danger"></i>
                                        </a>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection
