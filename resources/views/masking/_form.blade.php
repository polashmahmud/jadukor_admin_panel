<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Sender ID </label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="display_name" placeholder="Sender ID" value="{{ old('display_name',$masking->display_name) }}">
    </div>
</div>

@if(\Illuminate\Support\Facades\Auth::user()->role == 1)
    <div class="basic-form m-t-20">
        <div class="form-group">
            <label>Name </label>
            <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Name" value="{{ old('name',$masking->name) }}">
        </div>
    </div>

    @if($masking->exists)
        <div class="basic-form m-t-20">
            <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-lg-6">
                            <input type="radio" name="status" value="1" @if($masking->status == 1) checked @endif > Active
                        </div>
                        <div class="col-lg-6">
                            <input type="radio" name="status" value="0" @if($masking->status == 0) checked @endif> Pending
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @else
        <div class="basic-form m-t-20">
            <div class="form-group">
                <label class="col-sm-2 control-label">Status</label>
                <div class="col-sm-10">
                    <div class="row" style="margin-bottom: 20px;">
                        <div class="col-lg-6">
                            <input type="radio" name="status" value="1" checked> Active
                        </div>
                        <div class="col-lg-6">
                            <input type="radio" name="status" value="0"> Pending
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif

@endif
