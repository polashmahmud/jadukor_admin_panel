@extends('layouts.app')

@section('title', 'All Masking')

@section('breadcrumb')
    <li class="active">All Masking</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
    <link href={{ asset("css/select2.min.css") }} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
    <script src={{ asset("js/select2.min.js") }}></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                allowClear: true,
                placeholder: 'Select an option'
            });
        });
    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card alert">
                <div class="card-header pr">
                    @if($masking->exists)
                        <h4>Edit Masking</h4>
                    @else
                        <h4>Add New Masking</h4>
                    @endif
                </div>

                @if($masking->exists)
                <form method="POST" action="{{ route('masking.update', $masking->id) }}">
                    @method('PATCH')
                    @csrf
                @else
                <form action="{{ route('masking.store') }}" method="POST">
                    @csrf
                @endif

                @include('masking._form')

                    @if($masking->exists)
                        <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                        <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('masking.index') }}">Back</a>
                    @else
                        <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                    @endif
                </form>
            </div>
            <div class="card alert">

                <div class="card-header">
                    <h4>Masking Information</h4>
                </div>
                <div class="card-body">
                    <ul class="list-group">
                        <li class="list-group-item">Sender name can have up to 11 characters. Acceptable characters are: a-z A-Z 0-9 -. [space]. </li>

                        <li class="list-group-item">Each added sender name will be verified by our customer service team.Verification usually takes couple minutes in working hours. </li>



                    </ul>
                </div>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card alert">
                <div class="card-header">
                    <h4>All Masking </h4>
                </div>
                <hr>
                @include('common.datatable')
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection
