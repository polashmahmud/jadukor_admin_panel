@extends('layouts.app')

@section('title', 'Masking Add')

@section('breadcrumb')
    <li class="active">Masking Add</li>
@endsection

@push('header-scripts')

@endpush

@push('footer-scripts')

@endpush


@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card alert">
            <div class="card-header">
                <h4>Add Masking </h4>
                    @if($user->role == 1)
                        <a class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn btn-sm" style="float: right;" href="{{ route('admin.show', $user->username) }}">Back</a>
                    @elseif($user->role == 2)
                        <a class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn btn-sm" style="float: right;" href="{{ route('master-reseller.show', $user->username) }}">Back</a>
                    @elseif($user->role == 3)
                        <a class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn btn-sm" style="float: right;" href="{{ route('reseller.show', $user->username) }}">Back</a>
                    @else
                        <a class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn btn-sm" style="float: right;" href="{{ route('user.show', $user->username) }}">Back</a>
                    @endif
            </div>
            <hr>
            <div class="card-body">
                <form action="{{ route('add-masking.store') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ $user->id }}" name="user_id">
                <div class="table-responsive">
                    <table class="table">
                        <thead>
                        <tr>
                            <th style="width: 50px;">#</th>
                            <th>Masking Name</th>
                            <th>Apply</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($maskings as $masking)
                        <tr>
                            <td>{{ $loop->index + 1 }}</td>
                            <td>{{ $masking->display_name }} <span><code>{{ $masking->name }}</code></span></td>
                            <td><input type="checkbox" value="{{ $masking->id }}" name="masking_id[]" {!! (in_array($masking->id,$approvedMasking)) ? 'checked' : '' !!}></td>
                        </tr>
                        @endforeach
                        </tbody>
                    </table>
                    <button type="submit" class="btn btn-default m-b-10" style="float: right; margin-top: 20px;">Apply</button>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection