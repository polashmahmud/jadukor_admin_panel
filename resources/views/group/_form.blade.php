<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Group Name" value="{{ old('name',$group->name) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Status</label>
        @if($group->exists)
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" @if($group->status ==1) checked @endif >
                                <label class="form-check-label">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0" @if($group->status ==0) checked @endif>
                                <label class="form-check-label">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" checked>
                                <label class="form-check-label">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0">
                                <label class="form-check-label">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>