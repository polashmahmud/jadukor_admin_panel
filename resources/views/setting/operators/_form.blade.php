<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Select Country</label>
        <select class="form-control bg-ash border-none js-example-basic-single" name="country_id">
            <option></option>
            @if($operator->exists)
                @foreach($countries as $country)
                    <option value="{{ $country->id }}" @if($country->id == $operator->country_id) selected @endif >{{ $country->name }}</option>
                @endforeach
            @else
                @foreach($countries as $country)
                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Name" value="{{ old('name',$operator->name) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Prefix</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="prefix" placeholder="Prefix" value="{{ old('prefix',$operator->prefix) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>MNC</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="mnc" placeholder="MNC" value="{{ old('mnc',$operator->mnc) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>TADIG</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="tadig" placeholder="TADIG" value="{{ old('tadig',$operator->tadig) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Status</label>
        @if($operator->exists)
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" @if($operator->status ==1) checked @endif >
                                <label class="form-check-label" for="type">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0" @if($operator->status ==0) checked @endif>
                                <label class="form-check-label" for="type">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" checked>
                                <label class="form-check-label" for="type">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0">
                                <label class="form-check-label" for="type">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>