@extends('layouts.app')

@section('title', 'Setting')

@section('breadcrumb')
    <li class="active">Role And Permission</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
@endpush


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card alert">
                <div class="card-header">
                    <h4>Role and Permission </h4>
                </div>
                <div class="card-body">
                    <form action="{{ route('savePermission') }}" method="post">
                        @csrf
                        <div class="table-responsive">
                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th style="text-align: left;">Permission</th>
                                    <th style="text-align: left;">Admin</th>
                                    <th style="text-align: left;">Master Reseller</th>
                                    <th style="text-align: left;">Reseller</th>
                                    <th style="text-align: left;">User</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($permissions->groupBy('category') as $permission)
                                    <tr style="background: #eef1f6;">
                                        <td style="color: #252424;text-transform:  capitalize;font-weight:  700;" colspan="{!! count($roles)+1 !!} ">{{ $permission[0]->category }}</td>
                                    </tr>
                                    @foreach($permission as $row)
                                        <tr>
                                            <td><code>{{ $row->name }}</code> {{ $row->display_name }} ({{ $row->id }})</td>
                                            @foreach($roles as $role)
                                                <th>
                                                    <input type="checkbox" name="permission[{!!$role->id!!}][{!!$row->id!!}]" value="1" {!! (in_array($role->id.'-'.$row->id,$permission_role)) ? 'checked' : '' !!} >
                                                </th>
                                            @endforeach
                                        </tr>
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                        <br>
                        <button type="submit" class="btn btn-default" style="float: right">{{ __('messages.Submit') }}</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection