<div class="card-header">
    <h4>Permission Form</h4>
</div>
<div class="card-body">
    <div class="basic-form">
        <form action="{{ route('permission.store') }}" method="post">
            @csrf
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                <label>Name</label>
                <input type="type" class="form-control" name="name" placeholder="Name">
                @if ($errors->has('name'))
                    <span class="text-danger"><strong>{{ $errors->first('name') }}</strong></span>
                @endif
            </div>
            <div class="form-group{{ $errors->has('display_name') ? ' has-error' : '' }}">
                <label>Display Name</label>
                <input type="type" class="form-control" name="display_name"
                       placeholder="Display Name">
                @if ($errors->has('display_name'))
                    <span class="text-danger"><strong>{{ $errors->first('display_name') }}</strong></span>
                @endif
            </div>
            <div class="form-group">
                <label>Category</label>
                <select class="form-control bg-ash border-none js-example-basic-single" name="category">
                    <option></option>
                    <option value="Administrator">Administrator</option>
                    <option value="Dashboard">Dashboard</option>
                    <option value="Messaging">Messaging</option>
                    <option value="Phone Book">Phone Book</option>
                    <option value="Coverage and Pricing">Coverage and Pricing</option>
                    <option value="Reports">Reports</option>
                    <option value="Users">Users</option>
                    <option value="All Admin">All Admin</option>
                    <option value="All Master Reseller">All Master Reseller</option>
                    <option value="All Reseller">All Reseller</option>
                    <option value="All User">All User</option>
                    <option value="Api">Api</option>
                </select>
            </div>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>