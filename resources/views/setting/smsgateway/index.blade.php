@extends('layouts.app')

@section('title', 'SMS Gateways')

@section('breadcrumb')
    <li class="active">SMS Gateways</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
    <link href={{ asset("css/select2.min.css") }} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
    <script src={{ asset("js/select2.min.js") }}></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                allowClear: true,
                placeholder: 'Select an option'
            });
        });
    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card alert">
                <div class="card-header pr">
                    @if($smsgateway->exists)
                        <h4>Edit SMS Gateway</h4>
                    @else
                        <h4>Add New SMS Gateway</h4>
                    @endif
                </div>

                @if($smsgateway->exists)
                    <form method="POST" action="{{ route('sms-gateway.update', $smsgateway->id) }}">
                    @method('PATCH')
                @else
                    <form action="{{ route('sms-gateway.store') }}" method="POST">
                @endif
                    @csrf
                        @include('setting.smsgateway._form')
                    @if($smsgateway->exists)
                        <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                        <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('sms-gateway.index') }}">Back</a>
                    @else
                        <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                    @endif
                </form>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card alert">
                <div class="card-header">
                    <h4>All SMS Gateways </h4>
                </div>
                <hr>
                @include('common.datatable')
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection