<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Name" value="{{ old('name',$smsgateway->name) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Display Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="display_name" placeholder="Display Name" value="{{ old('display_name',$smsgateway->display_name) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>User Display Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="user_display_name" placeholder="User Display Name" value="{{ old('user_display_name',$smsgateway->user_display_name) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>URL</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="url" placeholder="URL" value="{{ old('url',$smsgateway->url) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Type</label>
        @if($smsgateway->exists)
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="type" value="0" @if($smsgateway->type ==0) checked @endif >
                                <label class="form-check-label" for="type">
                                    Non Masking
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="type" value="1" @if($smsgateway->type ==1) checked @endif>
                                <label class="form-check-label" for="type">
                                    Masking
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="type" value="0" checked >
                                <label class="form-check-label" for="type">
                                    Non Masking
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="type" value="1">
                                <label class="form-check-label" for="type">
                                    Masking
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        @if($smsgateway->exists)
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" @if($smsgateway->status ==1) checked @endif >
                                <label class="form-check-label" for="type">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0" @if($smsgateway->status ==0) checked @endif >
                                <label class="form-check-label" for="type">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" checked>
                                <label class="form-check-label" for="type">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0">
                                <label class="form-check-label" for="type">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>