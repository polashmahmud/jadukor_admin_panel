<div class="basic-form m-t-20">
    <div class="form-group">
        <label>iso</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="iso" placeholder="iso" value="{{ old('iso', $country->iso) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Name" value="{{ old('name',$country->name) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Nick Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="nickname" placeholder="Nick Name" value="{{ old('nickname',$country->nickname) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>iso3</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="iso3" placeholder="iso3" value="{{ old('iso3',$country->iso3) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Number Code</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="numcode" placeholder="Number Code" value="{{ old('numcode',$country->numcode) }}">
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Phone Code</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="phonecode" placeholder="Phone Code" value="{{ old('phonecode',$country->phonecode) }}">
    </div>
</div>