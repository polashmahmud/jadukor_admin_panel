<div class="bootstrap-data-table-panel">
    <div class="table-responsive">
        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
            <thead>
            <tr>
                @foreach($col_heads as $col_head)
                    @if($col_head == 'Option')
                        <th style="width:120px;">{!! $col_head !!}</th>
                    @elseif($col_head == 'Description')
                        <th style="width:25% ;">{!! $col_head !!}</th>
                    @else
                        <th>{!! $col_head !!}</th>
                    @endif
                @endforeach
            </tr>
            </thead>
            <tbody>
            @foreach($col_data as $data)
                <tr>
                    @foreach($data as $row)
                        <td>{!! $row !!}</td>
                    @endforeach
                </tr>
            @endforeach
            </tbody>
            @if(isset($col_foots))
                <tfoot>
                <tr>
                    @foreach($col_foots as $col_foot)
                        <th>{!! $col_foot !!}</th>
                    @endforeach
                </tr>
                </tfoot>
            @endif
        </table>
    </div>
</div>