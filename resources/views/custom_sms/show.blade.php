@extends('layouts.app')

@section('title', 'Preview SMS')

@section('breadcrumb')
    <li class="active">Preview SMS</li>
@endsection

@push('header-scripts')
   
@endpush

@push('footer-scripts')
    <script>
        function checkAll(ele) {
            var checkboxes = document.getElementsByTagName('input');
            if (ele.checked) {
                for (var i = 0; i < checkboxes.length; i++) {
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = true;
                    }
                }
            } else {
                for (var i = 0; i < checkboxes.length; i++) {
                    console.log(i)
                    if (checkboxes[i].type == 'checkbox') {
                        checkboxes[i].checked = false;
                    }
                }
            }
        }
    </script>
@endpush


@section('content')
    <div class="row">

        <div class="col-lg-6">
            <div class="card alert">
                <div class="card-header">
                    <h4>Preview SMS </h4>
                    <form action="{{ route('custom-sms.sendSMS') }}" method="POST">
                        @csrf
                        <input type="hidden" name="cost" value="{{ $grouped->sum('ttl_cost') }}">
                        <input type="hidden" name="preview_id" value="{{ $id }}">
                        <button type="submit" class="btn btn-success pull-right m-5">Send SMS</button>
                    </form>
                </div>
                <hr>
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Content</th>
                                <th>SMS Size</th>
                                <th>Cost</th>
                                <th style="width:25% ;">Options</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($allSms as $sms)
                            <tr @if($sms->ttl_cost <=0) style="background: #ff00001f;" @endif>
                                <td>{{ $sms->number }}</td>
                                <td>{{ $sms->content }}</td>
                                <td>{{ $sms->ttl_sms }}</td>
                                <td>{{ $sms->ttl_cost }}</td>
                                <td>
                                    {{--<a href="{{ route('user.edit', $value->id) }}"><i class="ti-pencil-alt color-success"></i></a>--}}
                                    <form id="delete-form-{{ $sms->id }}" method="post" action="{{ route('custom-sms.destroy', $sms->id) }}" style="display: none">
                                        @csrf
                                        @method('Delete')
                                    </form>
                                    <a href="" onclick="
                                            if(confirm('Are you sure, You Want to delete this?'))
                                            {
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{ $sms->id }}').submit();
                                            }
                                            else{
                                            event.preventDefault();
                                            }" ><i class="ti-trash color-danger"></i>
                                    </a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>




        <div class="col-lg-6">
            <div class="row">
                @include('send_message.sidebar')
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card alert">

                        <div class="card-body">

                            <div class="col-lg-10"> <center>
                                    Estimated Cost:  <b> {{ $grouped->sum('ttl_cost') }} Credits  </b>
                                </center>
                            </div>


                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Operator</th>
                                    <th>Recipient</th>
                                    <th>SMS</th>
                                    <th>Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($grouped as $value)
                                    <tr>
                                        <td>{{ $value->prefix }}</td>
                                        <td>1</td>
                                        <td>{{ $value->ttl_sms }}</td>
                                        <td>{{ $value->ttl_cost }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection