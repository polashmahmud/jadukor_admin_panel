@extends('layouts.app')

@section('title', 'Contact')

@section('breadcrumb')
    <li class="active">Contact</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
    <link href={{ asset("css/select2.min.css") }} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
    <script src={{ asset("js/select2.min.js") }}></script>
    <script>
        $(document).ready(function() {
            $('.select2').select2();
        });
    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card alert">
                <div class="card-header pr">
                    @if($phone->exists)
                        <h4>Edit Contact</h4>
                    @else
                        <h4>Add New Contact</h4>
                    @endif
                </div>

                @if($phone->exists)
                <form method="POST" action="{{ route('contacts.update', $phone->id) }}">
                        @method('PATCH')
                @else
                <form action="{{ route('contacts.store') }}" method="POST">
                @endif
                @csrf

                @include('phone._form')

                @if($phone->exists)
                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                    <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('contacts.index') }}">Back</a>
                @else
                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                @endif
                </form>
            </div>

            <div class="card alert">
                <div class="card-header pr">
                    <h4>File Upload</h4>
                    <a href="{{ asset('files/phone-example-file.csv') }}">Example File</a>
                </div>
                    <form action="{{ route('file.input.contact') }}" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="basic-form m-t-20">
                            <div class="form-group">
                                <label>File</label>
                                <input type="file" class="form-control border-none input-flat bg-ash" name="file">
                            </div>
                        </div>
                        <div class="basic-form m-t-20">
                            <div class="form-group">
                                <label>Select Groups</label>
                                <select class="form-control bg-ash border-none select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;" name="group_id[]">
                                    <option></option>
                                    @if($phone->exists)
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}" @foreach($phone->groups as $pgroup) @if($pgroup->pivot->group_id == $group->id) selected @endif @endforeach>{{ $group->name }}</option>
                                        @endforeach
                                    @else
                                        @foreach($groups as $group)
                                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                                        @endforeach
                                    @endif
                                </select>
                            </div>
                        </div>
                        <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                    </form>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card alert">
                <div class="card-header">
                    <h4>All Contacts </h4>
                </div>
                <hr>
                <div class="bootstrap-data-table-panel">
                    <div class="table-responsive">
                        <table id="bootstrap-data-table-export" class="table table-striped table-bordered">
                            <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Number</th>
                                <th>Group Name</th>
                                <th style="width:120px;">Option</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($phones as $value)
                                <tr>
                                    <td>{{ $value->name }}</td>
                                    <td>{{ $value->email }}</td>
                                    <td>{{ $value->number }}</td>
                                    <td>
                                        @foreach($value->groups as $group)
                                            <span class="label label-default">{{ $group->name }}</span>
                                        @endforeach
                                    </td>
                                    <td>
                                        <a href="{{ route('contacts.edit', $value->id) }}"><i class="ti-pencil-alt color-success"></i></a>

                                        <form id="delete-form-{{ $value->id }}" method="post" action="{{ route('contacts.destroy', $value->id) }}" style="display: none">
                                        @csrf
                                        @method('DELETE')
                                        </form>
                                        <a href="" onclick="
                                                 if(confirm('Are you sure, You Want to delete this?'))
                                          {
                                            event.preventDefault();
                                            document.getElementById('delete-form-{{ $value->id }}').submit();
                                          }
                                          else{
                                            event.preventDefault();
                                          }" ><i class="ti-trash color-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection
