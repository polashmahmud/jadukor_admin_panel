<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Name" value="{{ old('name',$phone->name) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Number</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="number" placeholder="Number" value="{{ old('number',$phone->number) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Email</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="email" placeholder="Email" value="{{ old('email',$phone->email) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Select Groups</label>
        <select class="form-control bg-ash border-none select2" multiple="multiple" data-placeholder="Select a State" style="width: 100%;" name="group_id[]">
            <option></option>
            @if($phone->exists)
                @foreach($groups as $group)
                    <option value="{{ $group->id }}" @foreach($phone->groups as $pgroup) @if($pgroup->pivot->group_id == $group->id) selected @endif @endforeach>{{ $group->name }}</option>
                @endforeach
            @else
                @foreach($groups as $group)
                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
