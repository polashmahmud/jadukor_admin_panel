@extends('layouts.app')

@section('title', 'Preview SMS')

@section('breadcrumb')
    <li class="active">Preview SMS</li>
@endsection

@push('header-scripts')
    
@endpush

@push('footer-scripts')
    
@endpush


@section('content')
    <div class="row">

        <div class="col-lg-6">
            <div class="card alert">
                <div class="card-header">
                    <h4>Preview SMS</h4>
                </div>
                <div class="card-body">
                    <div class="portlet light ">
                        <div class="portlet-body">
                            <div class="form-horizontal">
                                <form action="{{ route('preview-sms.update', $previewSms->id) }}" method="POST">
                                    @csrf
                                    @method('PUT')
                                <div class="row">
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Sender:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" name="sender" value="{{ $previewSms->sender }}" readonly="readonly" placeholder="{{ $previewSms->sender }}">
                                        </div>
                                    </div>
                                    @if(is_null($previewSms->group_id))
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Recipients:</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" name="recipients">{{ $numbers }}</textarea>
                                            <!-- <select class="form-control js-example-basic-multiple" name="recipients[]" multiple="multiple">
                                                @foreach($previewSms->numbers as $number)
                                                    <option value="{{ $number->number }}" selected>{{ $number->number }}</option>
                                                @endforeach
                                            </select> -->
                                        </div>
                                    </div>
                                    @endif
                                    @if(isset($previewSms->group_id))
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Groups:</label>
                                        <div class="col-sm-10">
                                            <select class="form-control" name="group_id">
                                                @foreach($phoneGroups as $group)
                                                    <option value="{{ $group->id }}" @if($previewSms->group_id == $group->id) selected @endif>{{ $group->name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    @endif
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label">Contents:</label>
                                        <div class="col-sm-10">
                                            <textarea class="form-control" rows="3" name="contents">{{ $previewSms->contents }}</textarea>
                                        </div>
                                        <label class="col-sm-2 control-label"></label>
                                        <div class="col-sm-10">
                                            Characters: <b>4 </b> - SMS count: <b>1 </b>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <button class="btn btn-default btn-lg m-b-10 bg-info border-none m-r-5 sbmt-btn" type="submit">update and preview</button>
                                        <a class="btn btn-default btn-lg m-b-10 bg-warning border-none m-r-5 sbmt-btn" href="{{ route('preview-sms.show', $previewSms->id) }}">Go Back</a>
                                    </div>
                                </form>

                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>




        <div class="col-lg-6">
            <div class="row">
                @include('send_message.sidebar')
            </div>
            <div class="row">
                <div class="col-lg-12">
                    <div class="card alert">

                        <div class="card-body">

                            <div class="col-lg-10"> <center>
                                    Estimated Cost:  <b> {{ $previewSms->smscost->sum('cost') }} Credits  </b>
                                </center>
                            </div>


                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    <th>Operator</th>
                                    <th>Recipient</th>
                                    <th>SMS</th>
                                    <th>Rate</th>
                                    <th>Cost</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($previewSms->smscost as $value)
                                    <tr>
                                        <td>{{ $value->operator->name }} - {{ $value->operator->prefix }}</td>
                                        <td>{{ $value->recipient }}</td>
                                        <td>{{ $value->sms }}</td>
                                        <td>{{ $value->rate }}</td>
                                        <td>{{ $value->cost }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection