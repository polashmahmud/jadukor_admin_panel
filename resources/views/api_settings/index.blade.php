@extends('layouts.app')

@section('title', 'Api Settings')

@section('breadcrumb')
    <li class="active">Api Settings</li>
@endsection

@push('header-scripts')

@endpush

@push('footer-scripts')

@endpush


@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card alert">
                <div class="card-header">
                    <h4>Generate Api</h4>
                </div>
                <div class="card-body">
                    <p>Your API Key : <code>{{ \Illuminate\Support\Facades\Auth::user()->api }}</code></p>
                    <form action="{{ route('api.settings') }}" method="post">
                        @csrf
                        <input type="hidden" name="user_id" value="{{ \Illuminate\Support\Facades\Auth::id() }}">
                        <button class="btn btn-info m-b-10" type="submit">Generate</button>
                    </form>
                    <hr>
                    <h4>API URL Non Masking (GET &amp; POST)</h4>
                    <p><code>http://sms.test/smsapi/non-masking?api_key=$2y$10$dWlSbkhqKz2P4lxPpR7G.Opk25WwURitGae/bflC00zvAW4mbdDT.&amp;smsType=text&amp;mobileNo=(NUMBER)&amp;smsContent=(Message Content)</code></p>
                    <hr>
                    <h4>API URL Masking (GET &amp; POST)</h4>
                    <p><code>http://sms.test/smsapi/non-masking?api_key=$2y$10$dWlSbkhqKz2P4lxPpR7G.Opk25WwURitGae/bflC00zvAW4mbdDT.&amp;smsType=text&amp;mobileNo=(NUMBER)&amp;smsContent=(Message Content)</code></p>
                    <hr>
                    <h4>Credit Balance API</h4>
                    <p><code>http://sms.test/smsapi/non-masking?api_key=$2y$10$dWlSbkhqKz2P4lxPpR7G.Opk25WwURitGae/bflC00zvAW4mbdDT.&amp;smsType=text&amp;mobileNo=(NUMBER)&amp;smsContent=(Message Content)</code></p>

                    <hr>
                    <div class="card alert">
                        <div class="card-header">
                            <h4>Description </h4>
                            <div class="card-header-right-icon">
                                <ul>
                                    <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                    <li class="card-option drop-menu"><i class="ti-settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                        <ul class="card-option-dropdown dropdown-menu">
                                            <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                            <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                            <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                            <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                        </ul>
                                    </li>
                                    <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover ">
                                    <thead>
                                    <tr>
                                        <th>Parameter Name</th>
                                        <th>Meaning/Value</th>
                                        <th>Description</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>api_key</td>
                                        <td>API Key</td>
                                        <td>Your API Key: $2y$10$dWlSbkhqKz2P4lxPpR7G.Opk25WwURitGae/bflC00zvAW4mbdDT.</td>
                                    </tr>
                                    <tr>
                                        <td>type</td>
                                        <td>text/unicode</td>
                                        <td>text for normal SMS/unicode for Bangla SMS</td>
                                    </tr>
                                    <tr>
                                        <td>contacts</td>
                                        <td>mobile number</td>
                                        <td>Exp: 88017XXXXXXXX,88018XXXXXXXX,88019XXXXXXXX...</td>
                                    </tr>
                                    <tr>
                                        <td>msg</td>
                                        <td>SMS body</td>
                                        <td>N.B: Please use url encoding to send some special characters like &amp;, $, @ etc</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="card alert">
                        <div class="card-header">
                            <h4>Error Code &amp; Meaning </h4>
                            <div class="card-header-right-icon">
                                <ul>
                                    <li class="card-close" data-dismiss="alert"><i class="ti-close"></i></li>
                                    <li class="card-option drop-menu"><i class="ti-settings" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" role="link"></i>
                                        <ul class="card-option-dropdown dropdown-menu">
                                            <li><a href="#"><i class="ti-loop"></i> Update data</a></li>
                                            <li><a href="#"><i class="ti-menu-alt"></i> Detail log</a></li>
                                            <li><a href="#"><i class="ti-pulse"></i> Statistics</a></li>
                                            <li><a href="#"><i class="ti-power-off"></i> Clear ist</a></li>
                                        </ul>
                                    </li>
                                    <li class="doc-link"><a href="#"><i class="ti-link"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-hover ">
                                    <thead>
                                    <tr>
                                        <th>Error Code</th>
                                        <th>Meaning</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>1003</td>
                                        <td>API Not Found</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /# card -->
        </div>
    </div>
@endsection