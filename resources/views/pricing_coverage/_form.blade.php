<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Select Country</label>
        <select class="form-control bg-ash border-none js-example-basic-single" name="country_id">
            <option></option>
            @if($pricing_coverage->exists)
                @foreach($countries as $country)
                    <option value="{{ $country->id }}" @if($country->id == $pricing_coverage->country_id) selected @endif >{{ $country->name }}</option>
                @endforeach
            @else
                @foreach($countries as $country)
                    <option value="{{ $country->id }}">{{ $country->name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Select Operator</label>
        <select class="form-control bg-ash border-none js-example-basic-single" name="operator_id">
            <option></option>
            @if($pricing_coverage->exists)
                @foreach($operators as $operator)
                    <option value="{{ $operator->id }}" @if($operator->id == $pricing_coverage->operator_id) selected @endif >{{ $operator->name }}</option>
                @endforeach
            @else
                @foreach($operators as $operator)
                    <option value="{{ $operator->id }}">{{ $operator->name }} - {{ $operator->prefix }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>SMS Gateway Masking</label>
        <select class="form-control bg-ash border-none js-example-basic-single" name="sms_gateway_masking_id">
            <option></option>
            @if($pricing_coverage->exists)
                @foreach($smsGateways as $smsGateway)
                    <option value="{{ $smsGateway->id }}" @if($smsGateway->id == $pricing_coverage->sms_gateway_masking_id) selected @endif >{{ $smsGateway->user_display_name }}</option>
                @endforeach
            @else
                @foreach($smsGateways as $smsGateway)
                    <option value="{{ $smsGateway->id }}">{{ $smsGateway->user_display_name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>
<div class="basic-form m-t-20">
    <div class="form-group">
        <label>SMS Gateway Non-Masking</label>
        <select class="form-control bg-ash border-none js-example-basic-single" name="sms_gateway_non_masking_id">
            <option></option>
            @if($pricing_coverage->exists)
                @foreach($smsGateways as $smsGateway)
                    <option value="{{ $smsGateway->id }}" @if($smsGateway->id == $pricing_coverage->sms_gateway_non_masking_id) selected @endif >{{ $smsGateway->user_display_name }}</option>
                @endforeach
            @else
                @foreach($smsGateways as $smsGateway)
                    <option value="{{ $smsGateway->id }}">{{ $smsGateway->user_display_name }}</option>
                @endforeach
            @endif
        </select>
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Masking Price (default)</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="masking_price" placeholder="Masking Price" value="{{ old('name',$pricing_coverage->masking_price) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Non Masking Price (default)</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="non_masking_price" placeholder="Non Masking Price" value="{{ old('name',$pricing_coverage->non_masking_price) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Sell Masking Price</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="sell_msk_price" placeholder="Sell Masking Price" value="{{ old('name',$pricing_coverage->sell_msk_price) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Sell Non Masking Price</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="sell_non_msk_price" placeholder="Sell Non Masking Price" value="{{ old('name',$pricing_coverage->sell_non_msk_price) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Status</label>
        @if($pricing_coverage->exists)
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" @if($pricing_coverage->status ==1) checked @endif >
                                <label class="form-check-label" for="type">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0" @if($pricing_coverage->status ==0) checked @endif>
                                <label class="form-check-label" for="type">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @else
            <div class="row">
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="1" checked>
                                <label class="form-check-label" for="type">
                                    Active
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="basic-form">
                        <div class="form-group">
                            <div class="form-check">
                                <input type="radio" name="status" value="0">
                                <label class="form-check-label" for="type">
                                    Deactivate
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>