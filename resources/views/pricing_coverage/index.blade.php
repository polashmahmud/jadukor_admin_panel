@extends('layouts.app')

@section('title', 'Pricing Coverage')

@section('breadcrumb')
    <li class="active">Pricing Coverage</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
    <link href={{ asset("css/select2.min.css") }} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
    <script src={{ asset("js/select2.min.js") }}></script>
    <script>
        $(document).ready(function() {
            $('.js-example-basic-single').select2({
                allowClear: true,
                placeholder: 'Select an option'
            });
        });
    </script>
@endpush


@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card alert">
                <div class="card-header pr">
                    @if($pricing_coverage->exists)
                        <h4>Edit Pricing</h4>
                    @else
                        <h4>Add New Pricing</h4>
                    @endif
                </div>

                @if($pricing_coverage->exists)
                <form method="POST" action="{{ route('pricing-coverages.update', $pricing_coverage->id) }}">
                    @method('PATCH')
                    @csrf
                @else
                <form action="{{ route('pricing-coverages.store') }}" method="POST">
                    @csrf
                    <input type="hidden" value="{{ $user->id }}" name="user_id">
                @endif

                @include('pricing_coverage._form')

                @if($pricing_coverage->exists)
                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                    @if($user->role == 1)
                        <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('admin.show', $user->username) }}">Back</a>
                    @elseif($user->role == 2)
                        <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('master-reseller.show', $user->username) }}">Back</a>
                    @elseif($user->role == 3)
                        <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('reseller.show', $user->username) }}">Back</a>
                    @else
                        <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('user.show', $user->username) }}">Back</a>
                    @endif
                @else
                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                @endif
                </form>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card alert">
                <div class="card-header">
                    <h4>All Pricing </h4>
                    <a href="{{ route('pricing-coverages.index') }}?name={{ $user->username }}" class="btn btn-default btn-xs pull-right">Add New</a>
                </div>
                <hr>
                @include('common.datatable')
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection
