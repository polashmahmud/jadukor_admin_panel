<div class="sidebar sidebar-hide-to-small sidebar-shrink sidebar-gestures">
    <div class="nano">
        <div class="nano-content">
            <ul>
                @if(in_array(1, $user_permissions))
                <li><a href="{{ route('dashboard') }}"><i class="ti-home"></i> Dashboard</a></li>
                @endif
                @if(in_array(5, $user_permissions))
                <li><a class="sidebar-sub-toggle"><i class="fa fa-envelope-o"></i>Messaging <span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        @if(in_array(6, $user_permissions))
                        <li><a href="{{ route('send-sms.index') }}">Fast SMS</a></li>
                        @endif
                        @if(in_array(10, $user_permissions))
                        <li><a href="{{ route('send-sms.group') }}">Group SMS</a></li>
                        @endif
                        @if(in_array(7, $user_permissions))
                        <li><a href="{{ route('send-sms.file') }}">File SMS</a></li>
                        @endif
                        <li><a href="{{ route('custom-sms.index') }}">Custom SMS</a></li>
                        <li><a href="{{ route('custom-sms.draft') }}">Custom SMS Draft</a></li>
                        @if(in_array(8, $user_permissions))
                        <li><a href="{{ route('masking.index') }}">Sender ID</a></li>
                        @endif
                        {{--<li><a href="">Campaigns</a></li>--}}
                        @if(in_array(9, $user_permissions))
                        <li><a href="{{ route('preview-sms.index') }}">Draft</a></li>
                        @endif
                        <li><a href="{{ route('template.index') }}">Template</a></li>
                        {{--<li><a href="inbox.html">Inbox</a></li>--}}
                    </ul>
                </li>
                @endif
                @if(in_array(11, $user_permissions))
                <li><a class="sidebar-sub-toggle"><i class="ti-id-badge"></i> Phone Book<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        @if(in_array(12, $user_permissions))
                        <li><a href="{{ route('group.index') }}">List Groups</a></li>
                        @endif
                        @if(in_array(14, $user_permissions))
                        <li><a href="{{ route('contacts.index') }}">List Contacts</a></li>
                        @endif
                        @if(in_array(13, $user_permissions))
                        <li><a href="{{ route('blocked-contacts.index') }}">Blocked Contacts</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @if(in_array(15, $user_permissions))
                <li><a href="{{ route('default-pricing.index') }}"><i class="ti-signal"></i> Coverage & Pricing</a></li>
                @endif
                @if(in_array(16, $user_permissions))
                <li><a class="sidebar-sub-toggle"><i class="ti-stats-up"></i> Reports<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        {{--<li><a href="">Recent Delivery Report</a></li>--}}
                        {{--<li><a href="">Summary Delivery Report</a></li>--}}
                        {{--<li><a href="">Recent Campaign Report</a></li>--}}
                        @if(in_array(17, $user_permissions))
                        <li><a href="{{ route('report.allSms') }}">All SMS</a></li>
                        @endif
                        @if(in_array(18, $user_permissions))
                        <li><a href="{{ route('report.routingSmsTotal') }}">Routing Wise Total SMS</a></li>
                        @endif
                        @if(in_array(19, $user_permissions))
                        <li><a href="{{ route('report.routingSms') }}">Routing Wise SMS Details</a></li>
                        @endif
                        @if(in_array(20, $user_permissions))
                        <li><a href="{{ route('report.userUsage') }}">User Usage</a></li>
                        @endif
                        @if(in_array(21, $user_permissions))
                        <li><a href="{{ route('report.userUsageDetails') }}">User Usage Details</a></li>
                        @endif
                        @if(in_array(22, $user_permissions))
                        <li><a href="{{ route('report.userBalance') }}">User Balance Report</a></li>
                        @endif
                        @if(in_array(23, $user_permissions))
                        <li><a href="{{ route('report.transaction') }}">Transactions (Masking/Non-Masking)</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                @if(in_array(24, $user_permissions))
                <li><a class="sidebar-sub-toggle"><i class="ti-user"></i> Users<span class="sidebar-collapse-icon ti-angle-down"></span></a>
                    <ul>
                        @if(in_array(25, $user_permissions))
                        <li><a href="{{ route('admin.index') }}">List Admin</a></li>
                        @endif
                        @if(in_array(26, $user_permissions))
                        <li><a href="{{ route('master-reseller.index') }}">List Master Reseller</a></li>
                        @endif
                        @if(in_array(27, $user_permissions))
                        <li><a href="{{ route('reseller.index') }}">List Reseller</a></li>
                        @endif
                        @if(in_array(28, $user_permissions))
                        <li><a href="{{ route('user.index') }}">List User</a></li>
                        @endif
                        @if(in_array(29, $user_permissions))
                        <li><a href="{{ route('customer.create') }}">Add Customer</a></li>
                        @endif
                    </ul>
                </li>
                @endif
                <li><a href="{{ route('api.settings') }}"><i class="ti-file"></i> Api Settings</a></li>
            </ul>
        </div>
    </div>
</div>
