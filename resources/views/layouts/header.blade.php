<div class="header">
    <div class="pull-left">
        <div class="logo"><a href="{{ route('dashboard') }}"><span>Adminity</span></a></div>
        <div class="hamburger sidebar-toggle">
            <span class="line"></span>
            <span class="line"></span>
            <span class="line"></span>
        </div>
    </div>
    <div class="pull-right p-r-15">
        <ul>
            <li class="header-icon dib"><img style="width: 20px;" @if(LaravelLocalization::getCurrentLocale() == 'en') src="{{ url('image/uk-flug.png') }}" @else src="{{ url('image/bn-flug.png') }}" @endif alt="" /> <span class="user-avatar">{{ LaravelLocalization::getCurrentLocale() == 'en' ? 'English' : 'বাংলা' }} <i class="ti-angle-down f-s-10"></i></span>
                <div class="drop-down dropdown-profile">
                    <div class="dropdown-content-body">
                        <ul>
                            @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                    <a rel="alternate" hreflang="{{ $localeCode }}" href="{{ LaravelLocalization::getLocalizedURL($localeCode, null, [], true) }}">
                                        {{ $properties['native'] }}
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </li>
            @if(\Illuminate\Support\Facades\Auth::user()->role==1)
            <li class="header-icon dib"><span class="user-avatar">Setting <i class="ti-angle-down f-s-10"></i></span>
                <div class="drop-down dropdown-profile">
                    <div class="dropdown-content-body">
                        <ul>
                            @if(in_array(32, $user_permissions))
                            <li><a href="{{ route('role-permission') }}"><i class="ti-settings"></i> <span>Permission Save</span></a></li>
                            @endif
                            @if(in_array(33, $user_permissions))
                            <li><a href="{{ route('permission.index') }}"><i class="ti-settings"></i> <span>Permission Create</span></a></li>
                             @endif
                             @if(in_array(34, $user_permissions))
                            <li><a href="{{ route('countries.index') }}"><i class="ti-settings"></i> <span>Countries</span></a></li>
                            @endif
                            @if(in_array(37, $user_permissions))
                            <li><a href="{{ route('operators.index') }}"><i class="ti-settings"></i> <span>Operators</span></a></li>
                            @endif
                            @if(in_array(35, $user_permissions))
                            <li><a href="{{ route('sms-gateway.index') }}"><i class="ti-settings"></i> <span>SMS Gateway</span></a></li>
                            @endif
                            @if(in_array(36, $user_permissions))
                            <li><a href="{{ route('payment-method.index') }}"><i class="ti-settings"></i> <span>Payment Method</span></a></li>
                            @endif
                        </ul>
                    </div>
                </div>
            </li>
            @endif
            <li class="header-icon dib">
                @if( ! \Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar', 'small'))
                    <img class="avatar-img" src="{{ asset("assets/images/user-profile.jpg") }}" alt="" />
                @else
                    <img class="avatar-img" src="{{ asset(\Illuminate\Support\Facades\Auth::user()->getFirstMediaUrl('avatar', 'small')) }}" alt=""/>
                @endif
                <span class="user-avatar">{{ \Illuminate\Support\Facades\Auth::user()->name }} <i class="ti-angle-down f-s-10"></i></span>
                <div class="drop-down dropdown-profile">
                    <div class="dropdown-content-body">
                        <ul>
                            @if(in_array(30, $user_permissions))
                            <li><a href="{{ route('profile', \Illuminate\Support\Facades\Auth::user()->username ) }}"><i class="ti-user"></i> <span>Profile</span></a></li>
                            @endif
                            @if(in_array(31, $user_permissions))
                            <li><a href="#"><i class="ti-settings"></i> <span>Setting</span></a></li>
                            @endif
                            <li>
                                <a href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="ti-power-off"></i> <span>Logout</span>
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </li>
                        </ul>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>