<!DOCTYPE html>
<html lang="en">

@include('layouts.head')

<body>

@include('layouts.sidebar')
<!-- /# sidebar -->

@include('layouts.header')


<div class="content-wrap">
    <div class="main">
        <div class="container-fluid">
        @include('layouts.breadcrumb')
        @include('error')
        <!-- /# row -->
            <section id="main-content">
            @yield('content')
            <!-- /# row -->
            @include('layouts.footer')
            </section>
        </div>
    </div>
</div>

@include('layouts.footer-script')
</body>

</html>