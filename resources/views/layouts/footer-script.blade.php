<script src={{ asset("assets/js/lib/jquery.min.js") }}></script>
<script src={{ asset("assets/js/lib/jquery.nanoscroller.min.js") }}></script>
<script src={{ asset("assets/js/lib/menubar/sidebar.js") }}></script>
<script src={{ asset("assets/js/lib/preloader/pace.min.js") }}></script>
<script src={{ asset("assets/js/lib/bootstrap.min.js") }}></script>
<script src={{ asset("assets/js/lib/sweetalert/sweetalert.min.js") }}></script>
@stack('footer-scripts')
@include('notifaction')
<script src={{ asset("assets/js/scripts.js") }}></script>

