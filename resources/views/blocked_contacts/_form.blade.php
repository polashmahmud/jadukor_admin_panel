<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Name</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="name" placeholder="Name" value="{{ old('name',$blockedContact->name) }}">
    </div>
</div>

<div class="basic-form m-t-20">
    <div class="form-group">
        <label>Number</label>
        <input type="text" class="form-control border-none input-flat bg-ash" name="number" placeholder="Number" value="{{ old('number',$blockedContact->number) }}">
    </div>
</div>