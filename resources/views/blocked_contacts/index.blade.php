@extends('layouts.app')

@section('title', 'Blocked Contracts')

@section('breadcrumb')
    <li class="active">Blocked Contracts</li>
@endsection

@push('header-scripts')
    <link href={{asset("assets/css/lib/data-table/buttons.bootstrap.min.css")}} rel="stylesheet" />
@endpush

@push('footer-scripts')
    <script src={{asset("assets/js/lib/data-table/datatables.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/dataTables.buttons.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.flash.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/jszip.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/pdfmake.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/vfs_fonts.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.html5.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/buttons.print.min.js")}}></script>
    <script src={{asset("assets/js/lib/data-table/datatables-init.js")}}></script>
@endpush


@section('content')
    <div class="row">
        <div class="col-md-3">
            <div class="card alert">
                <div class="card-header pr">
                    @if($blockedContact->exists)
                        <h4>Edit Block Number</h4>
                    @else
                        <h4>Add New Block Number</h4>
                    @endif
                </div>

                @if($blockedContact->exists)
                <form method="POST" action="{{ route('blocked-contacts.update', $blockedContact->id) }}">
                @method('PATCH')
                @else
                <form action="{{ route('blocked-contacts.store') }}" method="POST">
                @endif
                @csrf

                @include('blocked_contacts._form')

                @if($blockedContact->exists)
                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Edit Enter</button>
                    <a class="btn btn-info btn-lg m-b-10 border-none m-r-5 sbmt-btn" href="{{ route('blocked-contacts.index') }}">Back</a>
                @else
                    <button class="btn btn-default btn-lg m-b-10 border-none m-r-5 sbmt-btn" type="submit">Submit Enter</button>
                @endif
                </form>
            </div>
        </div>
        <div class="col-lg-9">
            <div class="card alert">
                <div class="card-header">
                    <h4>All Blocked Contacts </h4>
                </div>
                <hr>
                @include('common.datatable')
            </div>
            <!-- /# card -->
        </div>
        <!-- /# column -->
    </div>
@endsection
