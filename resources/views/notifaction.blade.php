<script>
    @if(session()->has('success'))
    $(function() {
        swal("Good job!", "{!! Session::get('success') !!}", "success")
    });
    @endif

    @if(session()->has('info'))
    $(function() {
        swal("Information!", "{!! Session::get('info') !!}", "info")
    });
    @endif

    @if(session()->has('warning'))
    $(function() {
        swal("Warning!", "{!! Session::get('warning') !!}", "warning")
    });
    @endif

    @if(session()->has('error'))
    $(function() {
        swal("Error!", "{!! Session::get('error') !!}", "error")
    });
    @endif

</script>