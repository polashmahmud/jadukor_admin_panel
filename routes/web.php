<?php


Route::get('/', function () { return redirect('dashboard'); });

Route::get('blank', function () {
    return view('blank');
});

Auth::routes();


Route::group(
    [
        'prefix' => LaravelLocalization::setLocale(),
        'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath', 'auth' ]
    ],
    function()
    {
        Route::get('dashboard', 'DashboardController@index')->name('dashboard');

        // Administrator Setting
        Route::get('role-permission', 'RoleController@index')->name('role-permission');
        Route::post('save-permission', 'RoleController@savePermission')->name('savePermission');
        Route::resource('permission', 'PermissionController');
        Route::resource('countries', 'CountryController');
        Route::resource('operators', 'OperatorController');
        Route::resource('sms-gateway', 'SmsgatewayController');
        Route::resource('payment-method', 'PaymentMethodController');

        // Helper Controller
        Route::post('status-change', 'HelperController@statusChange')->name('status-change');
        Route::post('switch-user', 'HelperController@switchUser')->name('switch-user');


        // Users
        Route::resource('admin', 'AdminController');
        Route::resource('master-reseller', 'MasterResellerController');
        Route::resource('reseller', 'ResellerController');
        Route::resource('user', 'UsersController');
        Route::get('customer/create', 'CustomerController@create')->name('customer.create');
        Route::post('customer', 'CustomerController@store')->name('customer.store');
        Route::get('profile/{name}', 'ProfileController@index')->name('profile');
        Route::put('profile/{id}', 'ProfileController@update')->name('profile.update');
        Route::put('profile/password-change/{id}', 'ProfileController@passwordChange')->name('profile.password.change');

        // Default Pricing
        Route::resource('default-pricing', 'DefaultPricingController');
        Route::resource('pricing-coverages', 'PricingCoveragesController');

        // Transaction
        Route::resource('transaction', 'TransactionController');
        Route::resource('balance', 'BalanceController');

        // Api Settings
        Route::get('api', 'ApiController@index')->name('api.settings');
        Route::post('api', 'ApiController@generate')->name('api.generate');

        // PhoneBook
        Route::resource('group', 'GroupController');
        Route::resource('contacts', 'PhoneController');
        Route::resource('blocked-contacts', 'BlockedContactController');
        Route::post('file-input-contact', 'FileInputContact@store')->name('file.input.contact');

        // Send Message
        Route::get('send-sms', 'SendMessageController@index')->name('send-sms.index');
        Route::get('send-group-sms', 'SendMessageController@group')->name('send-sms.group');
        Route::get('send-file-sms', 'SendMessageController@file')->name('send-sms.file');
        Route::post('send-sms', 'SendMessageController@store')->name('send-sms.store');
        Route::resource('preview-sms', 'PreviewSmsController');
        Route::resource('sms', 'SmsController');
        Route::get('custom-sms', 'CustomSmsController@index')->name('custom-sms.index');
        Route::get('custom-sms/show/{id}', 'CustomSmsController@show')->name('custom-sms.show');
        Route::get('custom-sms/draft', 'CustomSmsController@draft')->name('custom-sms.draft');
        Route::post('custom-sms', 'CustomSmsController@store')->name('custom-sms.store');
        Route::post('custom-sms/sendSMS', 'CustomSmsController@sendSMS')->name('custom-sms.sendSMS');
        Route::DELETE('custom-sms/destroy/{id}', 'CustomSmsController@destroy')->name('custom-sms.destroy');

        // Masking
        Route::resource('masking', 'MaskingController');
        Route::get('add-masking/{name}', 'AddMasking@index')->name('add-masking');
        Route::post('add-masking', 'AddMasking@store')->name('add-masking.store');

        // All Report
        Route::prefix('report')->group(function () {
            Route::get('all-sms', 'ReportController@allSms')->name('report.allSms');
            Route::get('all-sms/view/{id}', 'ReportController@allSmsView')->name('allSmsView');
            Route::get('all-sms/download/{id}', 'ReportController@download');
            Route::get('routing-wise-total-sms', 'ReportController@routingSmsTotal')->name('report.routingSmsTotal');
            Route::get('routing-wise-sms', 'ReportController@routingSms')->name('report.routingSms');
            Route::get('user-usage', 'ReportController@userUsage')->name('report.userUsage');
            Route::get('user-usage-details', 'ReportController@userUsageDetails')->name('report.userUsageDetails');
            Route::get('user-balance', 'ReportController@userBalance')->name('report.userBalance');
            Route::get('transaction', 'ReportController@transaction')->name('report.transaction');
        });

        Route::resource('template', 'TemplateController');



    });

