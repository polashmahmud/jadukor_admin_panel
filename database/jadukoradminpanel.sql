-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: Oct 28, 2018 at 07:00 AM
-- Server version: 5.7.19
-- PHP Version: 7.1.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sms`
--

-- --------------------------------------------------------

--
-- Table structure for table `balances`
--

DROP TABLE IF EXISTS `balances`;
CREATE TABLE IF NOT EXISTS `balances` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Debit, 1=Credit',
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `masking_sms` decimal(10,2) NOT NULL DEFAULT '0.00',
  `non_masking_sms` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `balances_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `balances`
--

INSERT INTO `balances` (`id`, `user_id`, `state`, `payment_method`, `description`, `amount`, `masking_sms`, `non_masking_sms`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Amount Transfer', 'Balance Add', '0.00', '0.00', '2.00', '2018-10-28 00:05:20', '2018-10-28 00:05:20'),
(2, 1, 0, 'Send SMS', 'Send SMS <a href=\"\">Details</a> ', '-1.00', '0.00', '-1.00', '2018-10-28 00:14:03', NULL),
(3, 1, 1, 'Amount Transfer', 'Balance Add', '0.00', '0.00', '10.00', '2018-10-28 00:47:21', '2018-10-28 00:47:21'),
(4, 1, 0, 'Send SMS', 'Send SMS <a href=\"\">Details</a> ', '-2.00', '0.00', '-1.00', '2018-10-28 00:51:05', NULL),
(5, 1, 0, 'Send SMS', 'Send SMS <a href=\"\">Details</a> ', '-2.00', '0.00', '-1.00', '2018-10-28 00:54:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `blocked_contacts`
--

DROP TABLE IF EXISTS `blocked_contacts`;
CREATE TABLE IF NOT EXISTS `blocked_contacts` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
CREATE TABLE IF NOT EXISTS `countries` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `iso` char(2) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nickname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `iso3` char(3) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `numcode` char(6) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phonecode` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`id`, `iso`, `name`, `nickname`, `iso3`, `numcode`, `phonecode`, `created_at`, `updated_at`) VALUES
(1, 'BD', 'BANGLADESH', 'Bangladesh', 'BGD', '50', 880, '2018-10-28 00:03:51', '2018-10-28 00:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `default_pricings`
--

DROP TABLE IF EXISTS `default_pricings`;
CREATE TABLE IF NOT EXISTS `default_pricings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `sms_gateway_masking_id` int(10) UNSIGNED NOT NULL,
  `sms_gateway_non_masking_id` int(10) UNSIGNED NOT NULL,
  `masking_price` double(8,2) NOT NULL DEFAULT '0.00',
  `non_masking_price` double(8,2) NOT NULL DEFAULT '0.00',
  `sell_msk_price` double(8,2) NOT NULL DEFAULT '0.00',
  `sell_non_msk_price` double(8,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Not allowed, 1=Allowed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `default_pricings_user_id_foreign` (`user_id`),
  KEY `default_pricings_country_id_foreign` (`country_id`),
  KEY `default_pricings_operator_id_foreign` (`operator_id`),
  KEY `default_pricings_sms_gateway_masking_id_foreign` (`sms_gateway_masking_id`),
  KEY `default_pricings_sms_gateway_non_masking_id_foreign` (`sms_gateway_non_masking_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `default_pricings`
--

INSERT INTO `default_pricings` (`id`, `user_id`, `country_id`, `operator_id`, `sms_gateway_masking_id`, `sms_gateway_non_masking_id`, `masking_price`, `non_masking_price`, `sell_msk_price`, `sell_non_msk_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 1, 0.30, 0.25, 0.35, 0.30, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(2, 1, 1, 2, 2, 1, 0.30, 0.25, 0.35, 0.30, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(3, 1, 1, 3, 4, 1, 0.30, 0.25, 0.35, 0.30, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(4, 1, 1, 4, 3, 1, 0.30, 0.25, 0.35, 0.30, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(5, 1, 1, 5, 3, 1, 0.30, 0.25, 0.35, 0.30, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(6, 1, 1, 6, 3, 1, 0.30, 0.25, 0.35, 0.30, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `user_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'My Number', 1, '2018-10-28 00:05:44', '2018-10-28 00:05:44');

-- --------------------------------------------------------

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
CREATE TABLE IF NOT EXISTS `logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `logs`
--

INSERT INTO `logs` (`id`, `user_id`, `description`, `created_at`, `updated_at`) VALUES
(1, 1, 'Add Transaction', '2018-10-28 00:04:52', '2018-10-28 00:04:52'),
(2, 1, 'Transfer Amount', '2018-10-28 00:05:20', '2018-10-28 00:05:20'),
(3, 1, 'Phone Group Add', '2018-10-28 00:05:44', '2018-10-28 00:05:44'),
(4, 1, 'Contact Add', '2018-10-28 00:06:01', '2018-10-28 00:06:01'),
(5, 1, 'Contact Add', '2018-10-28 00:06:15', '2018-10-28 00:06:15'),
(6, 1, 'Message Send Successfully', '2018-10-28 00:14:03', '2018-10-28 00:14:03'),
(7, 1, 'Edit Successfully', '2018-10-28 00:45:05', '2018-10-28 00:45:05'),
(8, 1, 'Edit Successfully', '2018-10-28 00:45:21', '2018-10-28 00:45:21'),
(9, 1, 'Delete Draft SMS', '2018-10-28 00:46:12', '2018-10-28 00:46:12'),
(10, 1, 'Edit Successfully', '2018-10-28 00:46:35', '2018-10-28 00:46:35'),
(11, 1, 'Transfer Amount', '2018-10-28 00:47:21', '2018-10-28 00:47:21'),
(12, 1, 'Message Send Successfully', '2018-10-28 00:51:05', '2018-10-28 00:51:05'),
(13, 1, 'Message Send Successfully', '2018-10-28 00:54:41', '2018-10-28 00:54:41'),
(14, 1, 'Edit Successfully', '2018-10-28 00:55:40', '2018-10-28 00:55:40'),
(15, 1, 'Edit Successfully', '2018-10-28 00:56:34', '2018-10-28 00:56:34'),
(16, 1, 'Edit Successfully', '2018-10-28 00:59:29', '2018-10-28 00:59:29'),
(17, 1, 'Edit Successfully', '2018-10-28 00:59:41', '2018-10-28 00:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `maskings`
--

DROP TABLE IF EXISTS `maskings`;
CREATE TABLE IF NOT EXISTS `maskings` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `approved_at` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Pending, 1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maskings_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `masking_user`
--

DROP TABLE IF EXISTS `masking_user`;
CREATE TABLE IF NOT EXISTS `masking_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `masking_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`masking_id`),
  KEY `masking_user_masking_id_foreign` (`masking_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

DROP TABLE IF EXISTS `media`;
CREATE TABLE IF NOT EXISTS `media` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL,
  `collection_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `file_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mime_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `disk` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `size` int(10) UNSIGNED NOT NULL,
  `manipulations` json NOT NULL,
  `custom_properties` json NOT NULL,
  `responsive_images` json NOT NULL,
  `order_column` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `media_model_type_model_id_index` (`model_type`,`model_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2014_10_12_200000_create_countries_table', 1),
(4, '2014_10_12_300000_create_operators_table', 1),
(5, '2014_10_12_400000_create_smsgateways_table', 1),
(6, '2014_10_12_500000_create_default_pricings_table', 1),
(7, '2014_10_12_600000_create_pricing_coverages_table', 1),
(8, '2018_09_02_055251_create_media_table', 1),
(9, '2018_09_03_033900_create_roles_table', 1),
(10, '2018_09_03_033935_create_permissions_table', 1),
(11, '2018_09_03_063822_create_logs_table', 1),
(12, '2018_09_17_180625_create_transactions_table', 1),
(13, '2018_09_18_190945_create_balances_table', 1),
(14, '2018_09_25_202232_create_payment_methods_table', 1),
(15, '2018_09_26_204651_create_groups_table', 1),
(16, '2018_09_29_200333_create_blocked_contacts_table', 1),
(17, '2018_10_06_211045_create_sms_costs_table', 1),
(18, '2018_10_08_072927_create_sms_table', 1),
(19, '2018_10_10_193141_create_maskings_table', 1),
(20, '2018_10_11_214056_create_send_logs_table', 1),
(21, '2018_10_27_230836_create_numbers_table', 1),
(23, '2018_10_28_062221_create_sendroutes_table', 2);

-- --------------------------------------------------------

--
-- Table structure for table `numbers`
--

DROP TABLE IF EXISTS `numbers`;
CREATE TABLE IF NOT EXISTS `numbers` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `preview_sms_id` int(10) UNSIGNED NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `numbers_preview_sms_id_foreign` (`preview_sms_id`)
) ENGINE=MyISAM AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `numbers`
--

INSERT INTO `numbers` (`id`, `preview_sms_id`, `number`, `created_at`, `updated_at`) VALUES
(1, 1, '8801678126086', NULL, NULL),
(4, 2, '8801678126086', NULL, NULL),
(6, 3, '8801678126086', NULL, NULL),
(7, 4, '8801678126086', NULL, NULL),
(8, 4, '8801887373400', NULL, NULL),
(9, 5, '8801678126086', NULL, NULL),
(10, 5, '8801887373400', NULL, NULL),
(11, 6, '8801678126086', NULL, NULL),
(12, 6, '8801678126086', NULL, NULL),
(13, 7, '8801678126086', NULL, NULL),
(14, 7, '8801887373400', NULL, NULL),
(19, 8, '8801678126086', NULL, NULL),
(20, 8, '8801678126086', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `operators`
--

DROP TABLE IF EXISTS `operators`;
CREATE TABLE IF NOT EXISTS `operators` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `country_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `prefix` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mnc` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tadig` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=Inactive, 1=Active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `operators_country_id_foreign` (`country_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `operators`
--

INSERT INTO `operators` (`id`, `country_id`, `name`, `prefix`, `mnc`, `tadig`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'Robi', '88018', '47002', 'BGDAK', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(2, 1, 'GP', '88017', '47001', 'BGDGP', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(3, 1, 'GP', '88013', '47001', 'BGDGP', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(4, 1, 'BL', '88019', '47003', 'BGDBL', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(5, 1, 'Airtel', '88016', '47007', 'BGDWT', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(6, 1, 'TeleTalk', '88015', '47004', 'BGDTT', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `payment_methods`
--

DROP TABLE IF EXISTS `payment_methods`;
CREATE TABLE IF NOT EXISTS `payment_methods` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Inactive',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `payment_methods`
--

INSERT INTO `payment_methods` (`id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Bank', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(2, 'bKash', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(3, 'Cash', 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

DROP TABLE IF EXISTS `permissions`;
CREATE TABLE IF NOT EXISTS `permissions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `permissions_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `category`, `created_at`, `updated_at`) VALUES
(1, 'dashboard-show', 'Dashboard Show', 'Dashboard', NULL, NULL),
(2, 'dashboard-top-list', 'Dashboard Top List', 'Dashboard', NULL, NULL),
(3, 'pending-masking', 'Dashboard Pending Masking', 'Dashboard', NULL, NULL),
(4, 'chart', 'Dashboard Chart', 'Dashboard', NULL, NULL),
(5, 'show-messaging', 'Sidebar Show Messaging', 'Messaging', NULL, NULL),
(6, 'fast-sms', 'Fast SMS', 'Messaging', NULL, NULL),
(7, 'file-sms', 'File SMS', 'Messaging', NULL, NULL),
(8, 'apply-masking', 'Apply Masking', 'Messaging', NULL, NULL),
(9, 'draft', 'Draft', 'Messaging', NULL, NULL),
(10, 'group-sms', 'Group SMS', 'Messaging', NULL, NULL),
(11, 'show-phone-book', 'Show Phone Book', 'Phone Book', NULL, NULL),
(12, 'list-group', 'List Group', 'Phone Book', NULL, NULL),
(13, 'block-contacts', 'Block Contacts', 'Phone Book', NULL, NULL),
(14, 'list-contacts', 'List Contacts', 'Phone Book', NULL, NULL),
(15, 'coverage-and-pricing', 'Coverage And Pricing', 'Coverage and Pricing', NULL, NULL),
(16, 'show-reports', 'Show Reports', 'Reports', NULL, NULL),
(17, 'all-sms', 'All SMS', 'Reports', NULL, NULL),
(18, 'routing-wise-total-sms', 'Routing Wise Total SMS', 'Reports', NULL, NULL),
(19, 'routing-wise-sms-details', 'Routing Wise SMS Details', 'Reports', NULL, NULL),
(20, 'user-usage', 'User Usage', 'Reports', NULL, NULL),
(21, 'user-usage-details', 'User Usage Details', 'Reports', NULL, NULL),
(22, 'user-balance-report', 'User Balance Report', 'Reports', NULL, NULL),
(23, 'transactions', 'Transactions', 'Reports', NULL, NULL),
(24, 'show-users', 'Show Users', 'Users', NULL, NULL),
(25, 'list-admin', 'List Admin', 'Users', NULL, NULL),
(26, 'list-master-reseller', 'List Master Reseller', 'Users', NULL, NULL),
(27, 'list-reseller', 'List Reseller', 'Users', NULL, NULL),
(28, 'list-user', 'List User', 'Users', NULL, NULL),
(29, 'add-customer', 'Add Customer', 'Users', NULL, NULL),
(30, 'profile', 'Show Profile', 'Users', NULL, NULL),
(31, 'setting', 'General Setting', 'Users', NULL, NULL),
(32, 'permission-save', 'Permission Save', 'Administrator', NULL, NULL),
(33, 'permission-create', 'Permission Create', 'Administrator', NULL, NULL),
(34, 'countries', 'Countries', 'Administrator', NULL, NULL),
(35, 'sms-gateway', 'SMS Gateway', 'Administrator', NULL, NULL),
(36, 'payment-method', 'Payment Method', 'Administrator', NULL, NULL),
(37, 'operators', 'Operatirs', 'Administrator', NULL, NULL),
(38, 'all-admin-balance', 'Add Balance', 'All Admin', NULL, NULL),
(39, 'all-admin-status', 'Change Status', 'All Admin', NULL, NULL),
(40, 'all-admin-show', 'Show Page', 'All Admin', NULL, NULL),
(41, 'all-admin-edit', 'Edit', 'All Admin', NULL, NULL),
(42, 'all-admin-delete', 'Delete', 'All Admin', NULL, NULL),
(43, 'sms-price-status', 'Details Page SMS Price Status Change', 'All Admin', NULL, NULL),
(44, 'sms-price-edit', 'Details Page SMS Price Edit', 'All Admin', NULL, NULL),
(45, 'sms-price-delete', 'Details Page SMS Price Delete', 'All Admin', NULL, NULL),
(46, 'admin-switch-account', 'Admin Switch Account', 'All Admin', NULL, NULL),
(47, 'all-master-reseller-balance', 'Add Balance', 'All Master Reseller', NULL, NULL),
(48, 'all-master-reseller-status', 'Change Status', 'All Master Reseller', NULL, NULL),
(49, 'all-master-reseller-show', 'Show Page', 'All Master Reseller', NULL, NULL),
(50, 'all-master-reseller-edit', 'Edit', 'All Master Reseller', NULL, NULL),
(52, 'all-master-reseller-delete', 'Delete', 'All Master Reseller', NULL, NULL),
(53, 'all-master-reseller-sms-price-status', 'Details Page SMS Price Status Change', 'All Master Reseller', NULL, NULL),
(54, 'all-master-reseller-sms-price-edit', 'Details Page SMS Price Edit', 'All Master Reseller', NULL, NULL),
(55, 'all-master-reseller-sms-price-delete', 'Details Page SMS Price Delete', 'All Master Reseller', NULL, NULL),
(56, 'master-reseller-switch-account', 'Master Reseller Switch Account', 'All Master Reseller', NULL, NULL),
(57, 'all-reseller-balance', 'Add Balance', 'All Reseller', NULL, NULL),
(58, 'all-reseller-status', 'Change Status', 'All Reseller', NULL, NULL),
(59, 'all-reseller-show', 'Show Page', 'All Reseller', NULL, NULL),
(60, 'all-reseller-edit', 'Edit', 'All Reseller', NULL, NULL),
(61, 'all-reseller-delete', 'Delete', 'All Reseller', NULL, NULL),
(63, 'all-reseller-sms-price-status', 'Details Page SMS Price Status Change', 'All Reseller', NULL, NULL),
(64, 'all-reseller-sms-price-edit', 'Details Page SMS Price Edit', 'All Reseller', NULL, NULL),
(65, 'all-reseller-sms-price-delete', 'Details Page SMS Price Delete', 'All Reseller', NULL, NULL),
(66, 'reseller-switch-account', 'Reseller Switch Account', 'All Reseller', NULL, NULL),
(67, 'all-user-balance', 'Add Balance', 'All User', NULL, NULL),
(68, 'all-user-status', 'Change Status', 'All User', NULL, NULL),
(69, 'all-user-show', 'Show Page', 'All User', NULL, NULL),
(70, 'all-user-edit', 'Edit', 'All User', NULL, NULL),
(71, 'all-user-delete', 'Delete', 'All User', NULL, NULL),
(72, 'all-user-sms-price-status', 'Details Page SMS Price Status Change', 'All User', NULL, NULL),
(73, 'all-user-sms-price-edit', 'Details Page SMS Price Edit', 'All User', NULL, NULL),
(74, 'all-user-sms-price-delete', 'Details Page SMS Price Delete', 'All User', NULL, NULL),
(75, 'user-switch-account', 'User Switch Account', 'All User', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

DROP TABLE IF EXISTS `permission_role`;
CREATE TABLE IF NOT EXISTS `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`permission_id`,`role_id`),
  KEY `permission_role_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(19, 1),
(20, 1),
(21, 1),
(22, 1),
(23, 1),
(24, 1),
(25, 1),
(26, 1),
(27, 1),
(28, 1),
(29, 1),
(30, 1),
(31, 1),
(32, 1),
(33, 1),
(34, 1),
(35, 1),
(36, 1),
(37, 1),
(38, 1),
(39, 1),
(40, 1),
(41, 1),
(42, 1),
(43, 1),
(44, 1),
(45, 1),
(46, 1),
(47, 1),
(48, 1),
(49, 1),
(50, 1),
(52, 1),
(53, 1),
(54, 1),
(55, 1),
(56, 1),
(57, 1),
(58, 1),
(59, 1),
(60, 1),
(61, 1),
(63, 1),
(64, 1),
(65, 1),
(66, 1),
(67, 1),
(68, 1),
(69, 1),
(70, 1),
(71, 1),
(72, 1),
(73, 1),
(74, 1),
(75, 1);

-- --------------------------------------------------------

--
-- Table structure for table `phones`
--

DROP TABLE IF EXISTS `phones`;
CREATE TABLE IF NOT EXISTS `phones` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `number` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phones`
--

INSERT INTO `phones` (`id`, `user_id`, `number`, `name`, `email`, `created_at`, `updated_at`) VALUES
(1, 1, '01678126086', 'Polash Mahmud', NULL, '2018-10-28 00:06:01', '2018-10-28 00:06:01'),
(2, 1, '01887373400', 'Polash Mahmud', NULL, '2018-10-28 00:06:15', '2018-10-28 00:06:15');

-- --------------------------------------------------------

--
-- Table structure for table `phone_group`
--

DROP TABLE IF EXISTS `phone_group`;
CREATE TABLE IF NOT EXISTS `phone_group` (
  `phone_id` int(10) UNSIGNED NOT NULL,
  `group_id` int(10) UNSIGNED NOT NULL,
  KEY `phone_group_phone_id_index` (`phone_id`),
  KEY `phone_group_group_id_index` (`group_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `phone_group`
--

INSERT INTO `phone_group` (`phone_id`, `group_id`) VALUES
(1, 1),
(2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `pricing_coverages`
--

DROP TABLE IF EXISTS `pricing_coverages`;
CREATE TABLE IF NOT EXISTS `pricing_coverages` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `country_id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `sms_gateway_masking_id` int(10) UNSIGNED NOT NULL,
  `sms_gateway_non_masking_id` int(10) UNSIGNED NOT NULL,
  `masking_price` double(8,2) NOT NULL DEFAULT '0.00',
  `non_masking_price` double(8,2) NOT NULL DEFAULT '0.00',
  `sell_msk_price` double(8,2) NOT NULL DEFAULT '0.00',
  `sell_non_msk_price` double(8,2) NOT NULL DEFAULT '0.00',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Not allowed, 1=Allowed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `pricing_coverages_user_id_foreign` (`user_id`),
  KEY `pricing_coverages_country_id_foreign` (`country_id`),
  KEY `pricing_coverages_operator_id_foreign` (`operator_id`),
  KEY `pricing_coverages_sms_gateway_masking_id_foreign` (`sms_gateway_masking_id`),
  KEY `pricing_coverages_sms_gateway_non_masking_id_foreign` (`sms_gateway_non_masking_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `pricing_coverages`
--

INSERT INTO `pricing_coverages` (`id`, `user_id`, `country_id`, `operator_id`, `sms_gateway_masking_id`, `sms_gateway_non_masking_id`, `masking_price`, `non_masking_price`, `sell_msk_price`, `sell_non_msk_price`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 1, 2, 1, 0.35, 0.30, 0.45, 0.40, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(2, 1, 1, 2, 3, 1, 0.35, 0.30, 0.45, 0.40, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(3, 1, 1, 3, 4, 1, 0.35, 0.30, 0.45, 0.40, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(4, 1, 1, 4, 3, 1, 0.35, 0.30, 0.45, 0.40, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(5, 1, 1, 5, 3, 1, 0.35, 0.30, 0.45, 0.40, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(6, 1, 1, 6, 3, 1, 0.35, 0.30, 0.45, 0.40, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_name_unique` (`name`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', NULL, NULL),
(2, 'master_reseller', 'Master Reseller', NULL, NULL),
(3, 'reseller', 'Reseller', NULL, NULL),
(4, 'user', 'User', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

DROP TABLE IF EXISTS `role_user`;
CREATE TABLE IF NOT EXISTS `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `role_user_role_id_foreign` (`role_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `sendroutes`
--

DROP TABLE IF EXISTS `sendroutes`;
CREATE TABLE IF NOT EXISTS `sendroutes` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `sms_id` int(10) UNSIGNED NOT NULL,
  `route_id` int(11) NOT NULL,
  `count` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sendroutes_sms_id_foreign` (`sms_id`),
  KEY `sendroutes_route_id_foreign` (`route_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sendroutes`
--

INSERT INTO `sendroutes` (`id`, `sms_id`, `route_id`, `count`, `created_at`, `updated_at`) VALUES
(1, 6, 1, '2.00', NULL, NULL),
(2, 7, 1, '1.00', '2018-10-28 00:54:41', '2018-10-28 00:54:41'),
(3, 7, 1, '1.00', '2018-10-28 00:54:41', '2018-10-28 00:54:41');

-- --------------------------------------------------------

--
-- Table structure for table `send_logs`
--

DROP TABLE IF EXISTS `send_logs`;
CREATE TABLE IF NOT EXISTS `send_logs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `preview_sms_id` int(10) UNSIGNED NOT NULL,
  `report` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `send_logs_preview_sms_id_foreign` (`preview_sms_id`),
  KEY `send_logs_user_id_foreign` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sms`
--

DROP TABLE IF EXISTS `sms`;
CREATE TABLE IF NOT EXISTS `sms` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sender` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `contents` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `send_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=quick, 1=group, 2=file',
  `group_id` int(11) DEFAULT NULL,
  `sms_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=text, 1=unicode',
  `masking` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=non-masking, 1=masking',
  `sms` decimal(10,2) NOT NULL DEFAULT '1.00' COMMENT 'Count SMS',
  `send_route` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `schedule` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=now, 1=schedule',
  `schedule_time` datetime DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=send, 0=save',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms`
--

INSERT INTO `sms` (`id`, `user_id`, `sender`, `contents`, `send_type`, `group_id`, `sms_type`, `masking`, `sms`, `send_route`, `schedule`, `schedule_time`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'non_masking', 'test', 0, NULL, 0, 0, '1.00', '1', 0, NULL, 1, '2018-10-28 00:13:59', '2018-10-28 00:14:03'),
(3, 1, 'non_masking', 'test', 0, NULL, 0, 0, '1.00', '', 0, NULL, 0, '2018-10-28 00:46:24', '2018-10-28 00:46:24'),
(4, 1, 'non_masking', 'test', 1, 1, 0, 0, '1.00', '', 0, NULL, 0, '2018-10-28 00:47:01', '2018-10-28 00:47:01'),
(5, 1, 'non_masking', 'test', 1, 1, 0, 0, '1.00', '', 0, NULL, 0, '2018-10-28 00:47:39', '2018-10-28 00:47:39'),
(6, 1, 'non_masking', 'test', 0, NULL, 0, 0, '1.00', '', 0, NULL, 1, '2018-10-28 00:48:10', '2018-10-28 00:51:05'),
(7, 1, 'non_masking', 'ts', 1, 1, 0, 0, '1.00', '', 0, NULL, 1, '2018-10-28 00:54:39', '2018-10-28 00:54:41'),
(8, 1, 'non_masking', 'test', 0, NULL, 0, 0, '1.00', '', 0, NULL, 0, '2018-10-28 00:55:26', '2018-10-28 00:55:26');

-- --------------------------------------------------------

--
-- Table structure for table `smsgateways`
--

DROP TABLE IF EXISTS `smsgateways`;
CREATE TABLE IF NOT EXISTS `smsgateways` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=non_masking, 1=masking',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '0=Not allowed, 1=Allowed',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `smsgateways_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `smsgateways`
--

INSERT INTO `smsgateways` (`id`, `user_id`, `name`, `display_name`, `user_display_name`, `url`, `type`, `status`, `created_at`, `updated_at`) VALUES
(1, 1, 'rankstel', 'RANKSTEL', 'Route 1', 'http://api.rankstelecom.com/api/v3/sendsms/plain?user=jadusms&password=ft654qwE&sender=8804445601100&SMSText=%MessageContent%&GSM=%number%&TYPE=longSMS&datacoding=0', 0, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(2, 1, 'robi', 'Robi', 'Route 2', 'https://api.mobireach.com.bd/SendTextMessage?Username=jadukor&Password=Dhaka@123&From=%maskingID%&To=%number%&Message=%Message%', 1, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(3, 1, 'banglalink', 'Banglalink', 'Route 3', 'https://vas.banglalinkgsm.com/sendSMS/sendSMS?msisdn=%number%&message=%Message%&userID=JADUKORIT&passwd=c4abb12821f4e446d67a1ec9ea48fe33&sender=%maskingID%', 1, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51'),
(4, 1, 'elitbuzz', 'Elitbuzz', 'Route 4', 'http://bangladeshsms.com/smsapi?api_key=R60001355ae445a37c58e0.76317646&type=%text%&contacts=%number%&senderid=%maskingID%&msg=%Message%&reply=Y', 1, 1, '2018-10-28 00:03:51', '2018-10-28 00:03:51');

-- --------------------------------------------------------

--
-- Table structure for table `sms_costs`
--

DROP TABLE IF EXISTS `sms_costs`;
CREATE TABLE IF NOT EXISTS `sms_costs` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `preview_sms_id` int(10) UNSIGNED NOT NULL,
  `operator_id` int(10) UNSIGNED NOT NULL,
  `recipient` decimal(10,2) NOT NULL DEFAULT '0.00',
  `sms` decimal(10,2) NOT NULL DEFAULT '0.00',
  `rate` decimal(10,2) NOT NULL DEFAULT '0.00',
  `cost` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `sms_costs_preview_sms_id_foreign` (`preview_sms_id`),
  KEY `sms_costs_operator_id_foreign` (`operator_id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sms_costs`
--

INSERT INTO `sms_costs` (`id`, `preview_sms_id`, `operator_id`, `recipient`, `sms`, `rate`, `cost`, `created_at`, `updated_at`) VALUES
(1, 1, 5, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:13:59', '2018-10-28 00:13:59'),
(4, 2, 5, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:45:21', '2018-10-28 00:45:21'),
(6, 3, 5, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:46:35', '2018-10-28 00:46:35'),
(7, 4, 5, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:47:01', '2018-10-28 00:47:01'),
(8, 4, 1, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:47:01', '2018-10-28 00:47:01'),
(9, 5, 5, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:47:39', '2018-10-28 00:47:39'),
(10, 5, 1, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:47:39', '2018-10-28 00:47:39'),
(11, 6, 5, '2.00', '1.00', '1.00', '2.00', '2018-10-28 00:48:10', '2018-10-28 00:48:10'),
(12, 7, 5, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:54:39', '2018-10-28 00:54:39'),
(13, 7, 1, '1.00', '1.00', '1.00', '1.00', '2018-10-28 00:54:39', '2018-10-28 00:54:39'),
(18, 8, 5, '2.00', '1.00', '1.00', '2.00', '2018-10-28 00:59:41', '2018-10-28 00:59:41');

-- --------------------------------------------------------

--
-- Table structure for table `transactions`
--

DROP TABLE IF EXISTS `transactions`;
CREATE TABLE IF NOT EXISTS `transactions` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(10) UNSIGNED NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0=Debit, 1=Credit',
  `payment_method` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `amount` decimal(10,2) NOT NULL DEFAULT '0.00',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `transactions_user_id_foreign` (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `transactions`
--

INSERT INTO `transactions` (`id`, `user_id`, `state`, `payment_method`, `description`, `amount`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Bank', NULL, '20.00', '2018-10-28 00:04:52', '2018-10-28 00:04:52'),
(2, 1, 0, 'Transfer', 'Transfer Amount to SMS Balance', '-0.50', '2018-10-28 00:05:20', '2018-10-28 00:05:20'),
(3, 1, 0, 'Send SMS', 'Send SMS <a href=\"\">Details</a> ', '-1.00', '2018-10-28 00:14:03', NULL),
(4, 1, 0, 'Transfer', 'Transfer Amount to SMS Balance', '-2.50', '2018-10-28 00:47:21', '2018-10-28 00:47:21'),
(5, 1, 0, 'Send SMS', 'Send SMS <a href=\"\">Details</a> ', '-2.00', '2018-10-28 00:51:05', NULL),
(6, 1, 0, 'Send SMS', 'Send SMS <a href=\"\">Details</a> ', '-2.00', '2018-10-28 00:54:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `first_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `member_id` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `price_type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1=SMS Pricing, 0=Money Pricing',
  `average_masking_price` double(8,2) NOT NULL DEFAULT '0.00' COMMENT 'for SMS Pricing A/C',
  `average_non_masking_price` double(8,2) NOT NULL DEFAULT '0.00' COMMENT 'for SMS Pricing A/C',
  `role` tinyint(1) NOT NULL DEFAULT '4' COMMENT '1=Admin, 2=Master Reseller, 3=Reseller, 4=User',
  `api` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1=Active, 0=Inactive',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_username_unique` (`username`),
  UNIQUE KEY `users_email_unique` (`email`),
  UNIQUE KEY `users_phone_unique` (`phone`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `last_name`, `username`, `email`, `member_id`, `phone`, `address`, `price_type`, `average_masking_price`, `average_non_masking_price`, `role`, `api`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Super', 'Admin', 'admin', 'admin@admin.com', 1, '01678126086', 'Vogra, Bangladesh', 1, 0.25, 0.25, 1, NULL, '$2y$10$PnZR9x5IgtXuiDDAuv18qeyT5TApL65C8tf8Lwjl8F88ew/hb.k2q', 1, NULL, '2018-10-28 00:03:51', '2018-10-28 00:03:51');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
