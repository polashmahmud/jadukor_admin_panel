<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMaskingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('maskings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('display_name');
            $table->string('name')->nullable();
            $table->dateTime('approved_at')->nullable();
            $table->boolean('status')->default(0)->comment('0=Pending, 1=Active');
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('masking_user', function (Blueprint $table) {
            $table->integer('user_id')->unsigned();
            $table->integer('masking_id')->unsigned();

            $table->foreign('user_id')->references('id')->on('users')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('masking_id')->references('id')->on('maskings')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->primary(['user_id', 'masking_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('masking_user');
        Schema::dropIfExists('maskings');
    }
}
