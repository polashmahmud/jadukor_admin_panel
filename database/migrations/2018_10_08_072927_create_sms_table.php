<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('sender');
            $table->mediumText('contents');
            $table->boolean('send_type')->default(0)->comment('0=quick, 1=group, 2=file');
            $table->integer('group_id')->nullable();
            $table->boolean('sms_type')->default(0)->comment('0=text, 1=unicode');
            $table->boolean('masking')->default(0)->comment('0=non-masking, 1=masking');
            $table->decimal('ttl_number', 8, 2)->default(0)->comment('Total Number');
            $table->decimal('ttl_sms', 8, 2)->default(0)->comment('Total SMS');
            $table->decimal('ttl_cost', 8, 2)->default(0)->comment('Total Cost');
            $table->boolean('schedule')->default(0)->comment('0=now, 1=schedule');
            $table->dateTime('schedule_time')->nullable();
            $table->boolean('status')->default(0)->comment('1=send, 0=save');
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms');
    }
}
