<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSmsCostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sms_costs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preview_sms_id')->unsigned();
            $table->integer('operator_id')->unsigned();
            $table->decimal('recipient', 10,2)->default(0.00);
            $table->decimal('sms', 10,2)->default(0.00);
            $table->decimal('rate', 10,2)->default(0.00);
            $table->decimal('cost', 10,2)->default(0.00);
            $table->timestamps();

            $table->foreign('preview_sms_id')->references('id')->on('sms')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('operator_id')->references('id')->on('operators')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sms_costs');
    }
}
