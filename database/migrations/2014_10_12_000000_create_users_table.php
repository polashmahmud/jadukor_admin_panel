<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('username')->unique();
            $table->string('email')->unique();
            $table->integer('member_id');
            $table->string('phone')->unique();
            $table->text('address')->nullable();
            $table->boolean('price_type')->default(0)->comment('1=SMS Pricing, 0=Money Pricing'); // SMS Pricing is Fixed Pricing, Per SMS 1 TK and Money Pricing is calculation on pricing table amount.
            $table->double('average_masking_price', 8,2)->default(0)->comment('for SMS Pricing A/C'); // Only for use when SMS Pricing A/C Create
            $table->double('average_non_masking_price', 8,2)->default(0)->comment('for SMS Pricing A/C'); // Only for use when SMS Pricing A/C Create
            $table->boolean('role')->default(4)->comment('1=Admin, 2=Master Reseller, 3=Reseller, 4=User');
            $table->string('api')->nullable();
            $table->string('password');
            $table->boolean('status')->default(1)->comment('1=Active, 0=Inactive');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
