<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNumbersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('numbers', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('preview_sms_id')->unsigned();
            $table->string('content')->nullable();
            $table->string('number');
            $table->string('sender');
            $table->decimal('sms', 8, 2)->default(0.00);
            $table->decimal('rate', 8, 2)->default(0.00);
            $table->decimal('cost', 8, 2)->default(0.00);
            $table->timestamps();

            $table->foreign('preview_sms_id')->references('id')->on('sms')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('numbers');
    }
}
