<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDefaultPricingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('default_pricings', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('operator_id')->unsigned();
            $table->integer('sms_gateway_masking_id')->unsigned();
            $table->integer('sms_gateway_non_masking_id')->unsigned();
            $table->double('masking_price',8,2)->default(0);
            $table->double('non_masking_price',8,2)->default(0);
            $table->double('sell_msk_price',8,2)->default(0);
            $table->double('sell_non_msk_price',8,2)->default(0);
            $table->boolean('status')->default(0)->comment('0=Not allowed, 1=Allowed');
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('country_id')->references('id')
                ->on('countries')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('operator_id')->references('id')
                ->on('operators')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sms_gateway_masking_id')->references('id')
                ->on('smsgateways')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('sms_gateway_non_masking_id')->references('id')
                ->on('smsgateways')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('default_pricings');
    }
}
