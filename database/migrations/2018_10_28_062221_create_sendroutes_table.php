<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSendroutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sendroutes', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sms_id')->unsigned();
            $table->integer('route_id');
            $table->decimal('ttl_number', 8, 2)->default(0)->comment('Total Number');
            $table->decimal('ttl_sms', 8, 2)->default(0)->comment('Total SMS');

            $table->timestamps();

            $table->foreign('sms_id')->references('id')->on('sms')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('route_id')->references('id')->on('smsgateways')
                ->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sendroutes');
    }
}
