<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustomSmsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('preview_custom_sms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('description')->nullable()->default('Custom SMS');
            $table->boolean('schedule')->default(0)->comment('0=now, 1=schedule');
            $table->dateTime('schedule_time')->nullable();
            $table->boolean('status')->default(0)->comment('1=send, 0=save');
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
        });

        Schema::create('custom_sms', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->integer('preview_custom_sms_id')->unsigned();
            $table->string('number')->nullable();
            $table->string('content')->nullable();
            $table->string('sender')->nullable();
            $table->string('prefix')->nullable();
            $table->boolean('sms_type')->default(0)->comment('0=text, 1=unicode');
            $table->boolean('masking')->default(0)->comment('0=non-masking, 1=masking');
            $table->decimal('ttl_sms', 8, 2)->default(0)->comment('Total SMS');
            $table->decimal('sms_cost', 8, 2)->default(0)->comment('Per SMS Cost');
            $table->decimal('ttl_cost', 8, 2)->default(0)->comment('Total Cost');
            $table->boolean('schedule')->default(0)->comment('0=now, 1=schedule');
            $table->dateTime('schedule_time')->nullable();
            $table->boolean('status')->default(0)->comment('1=send, 0=save');
            $table->longText('log')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')
                ->on('users')->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('preview_custom_sms_id')->references('id')
                ->on('preview_custom_sms')->onUpdate('cascade')->onDelete('cascade');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('preview_custom_sms');
        Schema::dropIfExists('custom_sms');
    }
}
